// TestThread.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include <windows.h>
#include <string.h>
#include <iostream>

HANDLE m_hThread,m_hThread_1;

int increment = 0;

DWORD WINAPI GetStateThread(LPVOID lpParam)
{
	char result;
	DWORD dwRet;

	for(;;){
		increment++;
		printf("Hola %u\n",increment);
	}

	return 0;
}

DWORD WINAPI GetStateThread_2(LPVOID lpParam)
{
	char result;
	DWORD dwRet;

	for(;;){
		increment++;
		printf("Adios %u\n",increment);
	}

	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{

	m_hThread=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)GetStateThread,NULL,0,NULL);
	m_hThread_1=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)GetStateThread_2,NULL,0,NULL);

	while(1);
	return 0;

}

