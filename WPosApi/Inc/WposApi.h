#ifndef INTERFAZWPOSAPI_H
#define INTERFAZWPOSAPI_H

//Codigos de error
#define SA_EVENTLOG_OK			0
#define SA_EVENTLOG_NO_OPENED	10
#define SA_EVENTLOG_NO_FIELD	20
#define SA_ERR_GENERICO			50
#define SA_ERR_EVENTLOG_SOURCE	51
#define SA_ERR_EVENTLOG_OPENING	52
#define SA_ERR_EVENTLOG_NO_MEM	53

#define MAX_IDEVENTO_LEN   20
#define MAX_MENSAJE_LEN    1024
#define MAX_FECHA_LEN      20
#define MAX_HORA_LEN       20
#define MAX_DETALLE_LEN    5*1024
#define MAX_DATOS_LEN      5*1024

typedef unsigned int APIRET;

#ifndef EXPORT_DLL
#define EXPORT_DLL __declspec(dllexport)
#endif


EXPORT_DLL APIRET OpenWpos(char *source,int buffersize);

EXPORT_DLL APIRET SaleWpos(char *outBuffer, char* importe);

EXPORT_DLL APIRET Importe(char *source);

EXPORT_DLL APIRET SendMensage(char *source);

EXPORT_DLL APIRET CloseWpos();

EXPORT_DLL APIRET WposSocket();



#endif // #ifndef INTERFAZWPOSAPI_H