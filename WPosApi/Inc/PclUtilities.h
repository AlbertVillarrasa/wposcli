/*! \file PclUtilities.h
    \brief Interface functions of PCLUtilities.dll
	\version 2.0
*/

#ifndef __PCL_UTILITIES_H__
#define __PCL_UTILITIES_H__

#ifdef PCLUTILITIES_EXPORTS
#define PCL_UTILITIES_API __declspec(dllexport)
#else
#define PCL_UTILITIES_API __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C"{
#endif

#if (_WIN32_WINNT >= _WIN32_WINNT_WIN7) && (_MSC_VER >= 1700)
#include "minwindef.h"
#include "winerror.h"
#include "HeapApi.h"
#else
#include "winbase.h"
#endif

/// Map old API name to new API name
#define	apiActivateCompanion		activateBTCompanion
#define	apiGetPairedCompanions		getPairedDevices
#define	apiActivateCompanionCom		activateUSBDevice	
#define	apiGetUsbCompanions			getUSBDevices
#define	apiGetRs232Companions		getRS232Devices
#define	apiActivateRs232Companion	activateRS232Device
#define apiGetActivatedCompanion	getActivatedDevice

#define ERROR_PCL_UTIL_BASE					61000
/*! No COM port is associated with  terminal. This may be due to issue 
during pairing.*/
#define ERROR_PCL_UTIL_NO_COM_PORT			(ERROR_PCL_UTIL_BASE+0)
/*! COM port associated with terminal is invalid. This may be due to issue 
during pairing.*/
#define ERROR_PCL_UTIL_INVALID_COM_PORT		(ERROR_PCL_UTIL_BASE+1)
/*! Error during RAS entry creation */
#define ERROR_PCL_UTIL_NO_RAS_ENTRY			(ERROR_PCL_UTIL_BASE+2)
/*! Error during finding IP companions*/
#define ERROR_PCL_UTIL_IP_RECV_ERROR		(ERROR_PCL_UTIL_BASE+3)

/** Max name size*/
#define MAX_NAME_SIZE	248
/** Max bluetooth address size*/
#define ADDRESS_SIZE	13
/** Max ip address size*/
#define ADDRESS_IP_SIZE	20
/** Max port size*/
#define PORT_SIZE		7

/*! \def TYPE_USB */
#define TYPE_USB		0
/*! \def TYPE_BLUETOOTH */
#define TYPE_BLUETOOTH	1
/*! \def TYPE_RS232 */
#define TYPE_RS232		2
/*! \def TYPE_IP */
#define TYPE_IP			3

/** The BLUETOOTH_COMPANION_INFO structure provides information about a Bluetooth terminal. */
typedef struct _BLUETOOTH_COMPANION_INFO {
    
    TCHAR	Address[ADDRESS_SIZE];	/**< Bluetooth address of the terminal, eg. "547F54ABCDEF" */
    BOOL    fActivated;				/**< Specifies whether the terminal is activated. */
    TCHAR   szName[MAX_NAME_SIZE ];	/**< Bluetooth name of the terminal */

} BLUETOOTH_COMPANION_INFO;

typedef BLUETOOTH_COMPANION_INFO * PBLUETOOTH_COMPANION_INFO;

/** The USB_COMPANION_INFO structure provides information about a USB terminal. */
typedef struct _USB_COMPANION_INFO {
    
    TCHAR	szPort[PORT_SIZE];		/**< COM port used by the terminal */
    BOOL    fActivated;				/**< Specifies whether the terminal is activated. */
    TCHAR   szName[MAX_NAME_SIZE ];	/**< Name of the terminal (terminal type-serial number, eg. "iCT250-12345678") */

} USB_COMPANION_INFO;

typedef USB_COMPANION_INFO * PUSB_COMPANION_INFO;

/** The IP_COMPANION_INFO structure provides information about a IP terminal. */
typedef struct _IP_COMPANION_INFO {

	TCHAR	AdressIP[ADDRESS_IP_SIZE];		/**< IP adresse used by the terminal */
	TCHAR	AdressMAC[ADDRESS_IP_SIZE];		/**< MAC adresse of the terminal */
	BOOL    fActivated;						/**< Specifies whether the terminal is activated. */
	TCHAR   szName[MAX_NAME_SIZE];			/**< Name of the terminal (terminal type-serial number, eg. "iCT250-12345678") */
	BOOL	UseSsl;							/**< Boolean to use ssl connection*/
} IP_COMPANION_INFO;

typedef IP_COMPANION_INFO * PIP_COMPANION_INFO;

/*!
\fn BOOL activateBTCompanion(TCHAR* pAddr)
\brief Activate Bluetooth terminal paired with the Windows device.

@param	[in] pAddr	A pointer to a buffer containing the Bluetooth address of the terminal (eg "547F54ABCDEF")
					The Bluetooth address can be retrieved with @ref getPairedDevices
 @return TRUE in case of success FALSE in case of failure. Call GetLastError to get the error code.
		  Possible error codes are Windows system error codes or the following specific PCL error codes:
		  ERROR_PCL_UTIL_NO_COM_PORT  No COM port is associated with terminal. This may be due to issue during pairing.
		  ERROR_PCL_UTIL_INVALID_COM_PORT	COM port associated with terminal is invalid. This may be due to issue during pairing.
	      ERROR_PCL_UTIL_NO_RAS_ENTRY		Error during RAS entry creation */
PCL_UTILITIES_API BOOL activateBTCompanion(TCHAR* pAddr);

/** Get Bluetooth paired companions.

 @param [out]		pCompanions	A pointer to a buffer that, on output, receives an array of BLUETOOTH_COMPANION_INFO structures,
								one for each Bluetooth paired terminal.
 @param [in,out]	pdwSize		Pointer to a variable that, on input, contains the size, in bytes, of the buffer specified by pCompanions.
								On output, contains the size, in bytes, of the array of BLUETOOTH_COMPANION_INFO structures required for Bluetooth paired companions.
 @param [out]		pdwEntries	Pointer to a variable that receives the number of Bluetooth paired companions written to the buffer specified by pCompanions.

 @return ERROR_SUCCESS is returned in case of success and pCompanions contains the list of plugged companions. The number of companions in this list is indicated by *pdwEntries.
ERROR_NOT_ENOUGH_MEMORY is returned if pCompanions is NULL or if its size is not large enough to contain all the plugged companions,  *pdwSize contains the size of the buffer to be allocated. 
If *pdwSize equals zero it means that no terminal is plugged. */
PCL_UTILITIES_API DWORD getPairedDevices(PBLUETOOTH_COMPANION_INFO pCompanions, DWORD* pdwSize, DWORD* pdwEntries);

/** API activate terminal com on USB.

 @param [in,out] pCom Null-terminated string containing the COM port of the 
					  terminal to be paired
 @return TRUE in case of success FALSE in case of failure. Call GetLastError to get the error code.
		  Possible error codes are Windows system error codes or the following specific PCL error codes:
		  ERROR_PCL_UTIL_NO_COM_PORT  No COM port is associated with terminal. This may be due to issue during pairing.
		  ERROR_PCL_UTIL_INVALID_COM_PORT	COM port associated with terminal is invalid. This may be due to issue during pairing.
	      ERROR_PCL_UTIL_NO_RAS_ENTRY		Error during RAS entry creation */
PCL_UTILITIES_API BOOL activateUSBDevice(TCHAR* pCom);

/** Retrieves the list of Usb Ingenico companions plugged in the Windows device.

 @param pCompanions		    Buffer containing the list of companions plugged in the Windows device
 @param [in,out] pdwSize    Size of pCompanions buffer as input. Required size as output.
 @param [in,out] pdwEntries Number of USB_COMPANION_INFO entries returned in pCompanions

 @return ERROR_SUCCESS is returned in case of success and pCompanions contains the list of plugged companions. The number of companions in this list is indicated by *pdwEntries.
	     ERROR_NOT_ENOUGH_MEMORY is returned if pCompanions is NULL or if its size is not large enough to contain all the plugged companions,  *pdwSize contains the size of the buffer to be allocated. 
		 If *pdwSize equals zero it means that no terminal is plugged.. */
PCL_UTILITIES_API DWORD getUSBDevices(PUSB_COMPANION_INFO pCompanions, DWORD* pdwSize, DWORD* pdwEntries);

/** Retrieves the list of Rs232 Ingenico companions plugged in the Windows device.

 @param pCompanions		    Buffer containing the list of companions plugged in the Windows device
 @param [in,out] pdwSize    Size of pCompanions buffer as input. Required size as output.
 @param [in,out] pdwEntries Number of USB_COMPANION_INFO entries returned in pCompanions

@return ERROR_SUCCESS is returned in case of success and pCompanions contains the list of plugged companions. The number of companions in this list is indicated by *pdwEntries.
ERROR_NOT_ENOUGH_MEMORY is returned if pCompanions is NULL or if its size is not large enough to contain all the plugged companions,  *pdwSize contains the size of the buffer to be allocated. 
If *pdwSize equals zero it means that no terminal is plugged. */
PCL_UTILITIES_API DWORD getRS232Devices(PUSB_COMPANION_INFO pCompanions, DWORD* pdwSize, DWORD* pdwEntries);

/** Activate a Rs232 terminal plugged on the Windows device.
 @param [in,out] pCom Null-terminated string containing the COM port of the 
					  terminal to be paired.
 @return TRUE in case of success FALSE in case of failure. Call GetLastError to get the error code.
	Possible error codes are Windows system error codes or the following specific PCL error codes:
	ERROR_PCL_UTIL_NO_COM_PORT	No COM port is associated with terminal. This may be due to issue during pairing.
	ERROR_PCL_UTIL_INVALID_COM_PORT	COM port associated with terminal is invalid. This may be due to issue during pairing.
	ERROR_PCL_UTIL_NO_RAS_ENTRY		Error during RAS entry creation*/
PCL_UTILITIES_API BOOL activateRS232Device(TCHAR* pCom);

/** Retrieves the list of IP Ingenico companions on the same network of the Windows device.
@param pCompanions		   Buffer containing the list of companions on the same network of the Windows device (can not be NULL).
@param [in,out] pdwSize    Size of pCompanions buffer as input. Required size as output (can not be NULL).
@param [in,out] pdwEntries Number of IP_COMPANION_INFO entries returned in pCompanions
@return ERROR_SUCCESS is returned in case of success and pCompanions contains the list of plugged companions. The number of companions in this list is indicated by *pdwEntries.
If *pdwEntries equals zero it means that no terminal is founded.
ERROR_NOT_ENOUGH_MEMORY is returned if pCompanions size is not large enough to contain all the plugged companions,  *pdwSize contains the size of the buffer to be allocated.
ERROR_PCL_UTIL_IP_RECV_ERROR is returned in others error case. */
PCL_UTILITIES_API DWORD getIPDevices(PIP_COMPANION_INFO pCompanions, DWORD* pdwSize, DWORD* pdwEntries);

/** Activate a IP terminal plugged on the Windows device.
@param [in] pIPDevice Structure containing the IP terminal to be paired.
@return TRUE in case of success FALSE in case of failure. Call GetLastError to get the error code.
Possible error codes are Windows system error codes.*/
PCL_UTILITIES_API BOOL activateIPDevice(PIP_COMPANION_INFO pIPDevice);

/**
Get activated terminal name and connection type.
@param	szName		A pointer to a buffer that receives the terminal name.
@param	dwNameSize	Size of the buffer pointed to by the szName parameter, in bytes.
@param	pdwType		A pointer to a DWORD to receive connection type (@ref ConnectionType)
@return TRUE in case of success, else FALSE*/
PCL_UTILITIES_API BOOL getActivatedDevice(TCHAR* szName, DWORD dwNameSize, DWORD* pdwType);

/** Add Authorized Bluetooth Address 
* @param  pAddr Bluetooth Address format "xx:xx:xx:" 
* @return TRUE in case of success, else FALSE 
* @note by default authorized :
* "00:03:81:", "00:7F:54:", "54:7F:54:", "54:E1:40:", "00:1D:FA:", "B4:00:16:", "38:EF:E3:"*/
PCL_UTILITIES_API BOOL addAuthorizedBTAddr(char * pAddr);

/** Add Authorized USB Vid/Pid 
* @param  pVid USB Vid 
* @param  pPid USB Pid 
* @return TRUE in case of success, else FALSE
* @note by default authorized :
* (0x079B, 0x0028), (0x0B00, 0x0052), (0x0B00, 0x0053), (0x0B00, 0x0054), (0x0B00, 0x0055), (0x0B00, 0x0056), (0x0B00, 0x0057),  (0x0B00, 0x0060), (0x0B00, 0x0061), 
* (0x0B00, 0x0062), (0x0B00, 0x0063), (0x0B00, 0x0064), (0x0B00, 0x0066), (0x0B00, 0x0080), (0x0B00, 0x0081), (0x0B00, 0x0082), (0x0B00, 0x0083), (0x0B00, 0x0084)*/
PCL_UTILITIES_API BOOL addAuthorizedVidPid(unsigned short pVid, unsigned short pPid);

#ifdef __cplusplus
}
#endif
#endif