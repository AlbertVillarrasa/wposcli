// PrinterFontDialog.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "PclTestApp.h"
#include "PrinterFontDialog.h"
#include "afxdialogex.h"
#include "pdautil.h"

eFonts g_font;

// Bo�te de dialogue PrinterFontDialog

IMPLEMENT_DYNAMIC(PrinterFontDialog, CDialogEx)

PrinterFontDialog::PrinterFontDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(PrinterFontDialog::IDD, pParent)
	, m_font(0)
{

}

PrinterFontDialog::~PrinterFontDialog()
{
}

void PrinterFontDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_ISO1, m_font);
}


BEGIN_MESSAGE_MAP(PrinterFontDialog, CDialogEx)
	ON_BN_CLICKED(IDOK, &PrinterFontDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &PrinterFontDialog::OnBnClickedCancel)
END_MESSAGE_MAP()


// Gestionnaires de messages de PrinterFontDialog


void PrinterFontDialog::OnBnClickedOk()
{
	UpdateData(TRUE);
	switch (m_font)
	{
	case 0:
		g_font = ISO8859_1;
		break;
	case 1:
		g_font = ISO8859_2;
		break;
	case 2:
		g_font = ISO8859_3;
		break;
	case 3:
		g_font = ISO8859_5;
		break;
	case 4:
		g_font = ISO8859_6;
		break;
	case 5:
		g_font = ISO8859_7;
		break;
	case 6:
		g_font = ISO8859_15;
		break;
	}
	CDialogEx::OnOK();
}


void PrinterFontDialog::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}
