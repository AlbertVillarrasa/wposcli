#pragma once


// Bo�te de dialogue PrinterFontDialog

class PrinterFontDialog : public CDialogEx
{
	DECLARE_DYNAMIC(PrinterFontDialog)

public:
	PrinterFontDialog(CWnd* pParent = NULL);   // constructeur standard
	virtual ~PrinterFontDialog();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_FONT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	int m_font;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
