#pragma once
#include "afxwin.h"


// Bo�te de dialogue LOGONAME

class LOGONAME : public CDialog
{
	DECLARE_DYNAMIC(LOGONAME)

public:
	LOGONAME(CWnd* pParent = NULL);   // constructeur standard
	virtual ~LOGONAME();
	virtual BOOL OnInitDialog();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_LOGONAME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	CEdit var_logoName;
	afx_msg void OnBnClickedOk();
};
