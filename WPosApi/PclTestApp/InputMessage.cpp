// InputMessage.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "PclTestApp.h"
#include "InputMessage.h"
#include "afxdialogex.h"

#define BUFFER_SIZE 1024
char gSendMsg[BUFFER_SIZE];
extern BOOL isKeyboardPresent;

// Bo�te de dialogue InputMessage

IMPLEMENT_DYNAMIC(InputMessage, CDialogEx)

InputMessage::InputMessage(CWnd* pParent /*=NULL*/)
	: CDialogEx(InputMessage::IDD, pParent)
{

}

InputMessage::~InputMessage()
{
}

void InputMessage::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_MSG_SEND, m_input_msg);
}


BEGIN_MESSAGE_MAP(InputMessage, CDialogEx)
	ON_BN_CLICKED(IDOK, &InputMessage::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &InputMessage::OnBnClickedCancel)
END_MESSAGE_MAP()

// Gestionnaires de messages de InputMessage
BOOL InputMessage::OnInitDialog()
{
	CDialog::OnInitDialog();
	//init message
	m_input_msg.SetWindowTextW(L"Msg send from WINDOWS");

    if ( !isKeyboardPresent )
	{
		TCHAR  infoBuf[1024];
		infoBuf[0]='\0';
		if ( SHGetSpecialFolderPath(0,infoBuf,CSIDL_PROGRAM_FILES,FALSE) )
		{
			infoBuf[2]='\0';
			PathAppend(infoBuf, TEXT("\\Program Files\\Common Files\\microsoft shared\\ink\\tabtip.exe"));
			ShellExecute(NULL, NULL,infoBuf, NULL, NULL, SW_SHOWNORMAL);
		}
	}
	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}


void InputMessage::OnBnClickedOk()
{
	wchar_t buffer[BUFFER_SIZE];
	size_t i;

	m_input_msg.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, gSendMsg, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	if ( !isKeyboardPresent )
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	CDialogEx::OnOK();
}


void InputMessage::OnBnClickedCancel()
{
	if (!isKeyboardPresent)
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	CDialogEx::OnCancel();
}
