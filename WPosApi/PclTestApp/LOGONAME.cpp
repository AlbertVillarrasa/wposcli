// LOGONAME.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "PclTestApp.h"
#include "LOGONAME.h"
#include "afxdialogex.h"

//Global variable
#define BUFFER_SIZE 256
char globLogoName[BUFFER_SIZE];
extern BOOL isKeyboardPresent;

// Bo�te de dialogue LOGONAME

IMPLEMENT_DYNAMIC(LOGONAME, CDialog)

LOGONAME::LOGONAME(CWnd* pParent /*=NULL*/)
	: CDialog(LOGONAME::IDD, pParent)
{

}

LOGONAME::~LOGONAME()
{
}

void LOGONAME::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOGONAME, var_logoName);
}


BEGIN_MESSAGE_MAP(LOGONAME, CDialog)
	ON_BN_CLICKED(IDOK, &LOGONAME::OnBnClickedOk)
END_MESSAGE_MAP()


// Gestionnaires de messages de LOGONAME
BOOL LOGONAME::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: ajoutez ici une initialisation suppl�mentaire
	var_logoName.LimitText(8);

    if ( !isKeyboardPresent )
	{
		TCHAR  infoBuf[1024];
		infoBuf[0]='\0';
		if ( SHGetSpecialFolderPath(0,infoBuf,CSIDL_PROGRAM_FILES,FALSE) )
		{
			infoBuf[2]='\0';
			PathAppend(infoBuf, TEXT("\\Program Files\\Common Files\\microsoft shared\\ink\\tabtip.exe"));
			ShellExecute(NULL, NULL,infoBuf, NULL, NULL, SW_SHOWNORMAL);
		}
	}
	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}


void LOGONAME::OnBnClickedOk()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	wchar_t buffer[BUFFER_SIZE];
	size_t i;

	var_logoName.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, globLogoName, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	if ( !isKeyboardPresent )
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	CDialog::OnOK();
}
