#pragma once


// Bo�te de dialogue BacklightDialog

class BacklightDialog : public CDialogEx
{
	DECLARE_DYNAMIC(BacklightDialog)

public:
	BacklightDialog(CWnd* pParent = NULL);   // constructeur standard
	virtual ~BacklightDialog();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_BACKLIGHT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	int m_radioLock;
};
