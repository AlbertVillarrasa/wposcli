
// PclTestDlg.cpp : fichier d'impl�mentation
//

#include "stdafx.h"
#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#include "PclTestApp.h"
#include "PclTestDlg.h"
#include "afxdialogex.h"
#include "pdautil.h"
#include "UNIT_TEST.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// bo�te de dialogue CPclTestDlg

CPclTestDlg::CPclTestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPclTestDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_PCLTESTAPP);
}

void CPclTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPclTestDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CPclTestDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_UNIT_TESTS, &CPclTestDlg::OnBnClickedButtonUnitTests)
	ON_BN_CLICKED(IDC_BUTTON_LOOP_TESTS, &CPclTestDlg::OnBnClickedButtonLoopTests)
END_MESSAGE_MAP()


// gestionnaires de messages pour CPclTestDlg

BOOL CPclTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// D�finir l'ic�ne de cette bo�te de dialogue. L'infrastructure effectue cela automatiquement
	//  lorsque la fen�tre principale de l'application n'est pas une bo�te de dialogue
	SetIcon(m_hIcon, TRUE);			// D�finir une grande ic�ne
	SetIcon(m_hIcon, FALSE);		// D�finir une petite ic�ne

	// initialise new VID/PID 
	addAuthorizedVidPidSrv(0x01,0x02);

	//init ssl files
	setClientCertificate("./cert_files/pclTestApp.pem",strlen("./cert_files/pclTestApp.pem"));
	setClientKey("./cert_files/pclTestApp.key", strlen("./cert_files/pclTestApp.key"), "sdkpcl", strlen("sdkpcl"));
	setDHFile("./cert_files/dh1024.pem", strlen("./cert_files/dh1024.pem"));

	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}

// Si vous ajoutez un bouton R�duire � votre bo�te de dialogue, vous devez utiliser le code ci-dessous
//  pour dessiner l'ic�ne. Pour les applications MFC utilisant le mod�le Document/Vue,
//  cela est fait automatiquement par l'infrastructure.

void CPclTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // contexte de p�riph�rique pour la peinture

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrer l'ic�ne dans le rectangle client
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dessiner l'ic�ne
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Le syst�me appelle cette fonction pour obtenir le curseur � afficher lorsque l'utilisateur fait glisser
//  la fen�tre r�duite.
HCURSOR CPclTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//CALLBACK BUTTON

void CPclTestDlg::OnBnClickedOk()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn = stopPclService();
	CDialog::OnOK();
}



void CPclTestDlg::OnBnClickedButtonUnitTests()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	UNIT_TEST Dlg_UNIT_TEST;
	Dlg_UNIT_TEST.DoModal();

}


void CPclTestDlg::OnBnClickedButtonLoopTests()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
}
