#ifndef __EPASR_H__
#define __EPASR_H__

#ifdef PCLSERVICE_EXPORTS
#define PCL_SERVICE_API __declspec(dllexport)
#else
#define PCL_SERVICE_API __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C"{
#endif

#include <windows.h>
#include "winbase.h"

#ifdef ENABLE_EPAS
class IEpasLoginRequest
{
	public:		
		virtual bool setOperatorId(int id) = 0;
};

class IEpasLoginResponse
{
	public:		
		virtual char* getResult() = 0;
		
};

PCL_SERVICE_API BOOL apiEpasConfigure(char* pHostName, unsigned short port, char* pWorkstationId, char* pPOIId, char* pManufacturerId, char* pApplicationName, char* pSoftwareVersion, char* pCertificationCode);
PCL_SERVICE_API BOOL apiEpasConnect();
PCL_SERVICE_API BOOL apiEpasDisconnect();
PCL_SERVICE_API BOOL apiEpasLogin(DWORD timeout, DWORD *pdwError);
PCL_SERVICE_API BOOL apiEpasLogout(DWORD timeout, DWORD *pdwError);
PCL_SERVICE_API BOOL apiEpasPaymentRequest(double amount, DWORD timeout, DWORD *pdwError);

#define EPASR_ERROR_ERTR01				1001
#define EPASR_ERROR_ERTR02				1002
#define EPASR_ERROR_ERTR03				1003
#define EPASR_ERROR_ERTR04				1004
#define EPASR_ERROR_ERTR05				1005
#define EPASR_ERROR_ERTR06				1006
#define EPASR_ERROR_ERTR07				1007

#define EPASR_ERROR_INVALID_REQUEST		2000
#define EPASR_ERROR_INVALID_RESPONSE	2001
#define EPASR_ERROR_NOT_ENOUGH_MEMORY	2002
#define EPASR_ERROR_TIMEOUT				2003
#define EPASR_ERROR_INSUFFICIENT_BUFFER	2004
#define EPASR_ERROR_INTERNAL_ERROR		2005

#endif // ENABLE_EPAS

#ifdef __cplusplus
}
#endif

#endif