// OpenBcrDialog.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "PclTestApp.h"
#include "OpenBcrDialog.h"
#include "afxdialogex.h"

extern BOOL isKeyboardPresent;


// Bo�te de dialogue OpenBcrDialog

IMPLEMENT_DYNAMIC(OpenBcrDialog, CDialogEx)

OpenBcrDialog::OpenBcrDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(OpenBcrDialog::IDD, pParent)
	, m_nInactivityTo(600)
{
	
}

OpenBcrDialog::~OpenBcrDialog()
{
}

void OpenBcrDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_BCR_INACTIVITY_TO, m_editInactivityTo);
	DDX_Text(pDX, IDC_EDIT_BCR_INACTIVITY_TO, m_nInactivityTo);
	DDV_MinMaxUInt(pDX, m_nInactivityTo, 0, 3600);
}


BEGIN_MESSAGE_MAP(OpenBcrDialog, CDialogEx)
	ON_BN_CLICKED(IDOK, &OpenBcrDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// Gestionnaires de messages de OpenBcrDialog
BOOL OpenBcrDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	if ( !isKeyboardPresent )
	{
		TCHAR  infoBuf[1024];
		infoBuf[0]='\0';
		if ( SHGetSpecialFolderPath(0,infoBuf,CSIDL_PROGRAM_FILES,FALSE) )
		{
			infoBuf[2]='\0';
			PathAppend(infoBuf, TEXT("\\Program Files\\Common Files\\microsoft shared\\ink\\tabtip.exe"));
			ShellExecute(NULL, NULL,infoBuf, NULL, NULL, SW_SHOWNORMAL);
		}
	}
	return TRUE;
}



void OpenBcrDialog::OnBnClickedOk()
{
	if (!isKeyboardPresent)
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	UpdateData(TRUE);
	CDialogEx::OnOK();
}
