#pragma once
#include "afxwin.h"


// Bo�te de dialogue TRANSACTION

class TRANSACTION : public CDialog
{
	DECLARE_DYNAMIC(TRANSACTION)

public:
	TRANSACTION(CWnd* pParent = NULL);   // constructeur standard
	virtual ~TRANSACTION();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_DOTRANSACTION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtDotransaction();
private:
	CComboBox var_list_TransType;
	CComboBox var_list_AuthType;
	CComboBox var_list_AuthRequired;

public:
	CEdit var_pos_number;
	CEdit var_currency_code;
	CEdit var_amount;
	CEdit var_userData;
	afx_msg void OnBnClickedOk();
	CEdit var_extended_data;
	CEdit var_appli_nb;
	CStatic var_text_extended;
	CStatic var_text_appliNb;
	afx_msg void OnEnSetfocusTransPosnb();
	afx_msg void OnEnSetfocusTransCurrency();
	CButton var_radio_extented_data_file;
	CButton var_radio_extented_data_text;
	afx_msg void OnBnClickedButtDotransactionStruct();
};
