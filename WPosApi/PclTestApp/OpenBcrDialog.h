#pragma once
#include "afxwin.h"


// Bo�te de dialogue OpenBcrDialog

class OpenBcrDialog : public CDialogEx
{
	DECLARE_DYNAMIC(OpenBcrDialog)

public:
	OpenBcrDialog(CWnd* pParent = NULL);   // constructeur standard
	virtual ~OpenBcrDialog();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_OPENBCR_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_editInactivityTo;
	UINT m_nInactivityTo;
	afx_msg void OnBnClickedOk();
};
