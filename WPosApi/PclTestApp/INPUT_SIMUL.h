#pragma once
#include "afxwin.h"


// Bo�te de dialogue INPUT_SIMUL

class INPUT_SIMUL : public CDialog
{
	DECLARE_DYNAMIC(INPUT_SIMUL)

public:
	INPUT_SIMUL(CWnd* pParent = NULL);   // constructeur standard
	virtual ~INPUT_SIMUL();
	virtual BOOL OnInitDialog();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_INPUTSIMUL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtSimul1();
	afx_msg void OnBnClickedButtSimul2();
	afx_msg void OnBnClickedButtSimul3();
	afx_msg void OnBnClickedButtSimul4();
	afx_msg void OnBnClickedButtSimul5();
	afx_msg void OnBnClickedButtSimul6();
	afx_msg void OnBnClickedButtSimul7();
	afx_msg void OnBnClickedButtSimul8();
	afx_msg void OnBnClickedButtSimul9();
	afx_msg void OnBnClickedButtSimulf();
	afx_msg void OnBnClickedButtSimul0();
	afx_msg void OnBnClickedButtSimuldot();
	afx_msg void OnBnClickedButtSimulred();
	afx_msg void OnBnClickedButtSimulyellow();
	afx_msg void OnBnClickedButtSimulgreen();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtInputsimulskhaut();
	afx_msg void OnBnClickedButtInputsimulskbas();
	afx_msg void OnBnClickedButtInputsimulsk2();
	afx_msg void OnBnClickedButtInputsimulsk1();
	afx_msg void OnBnClickedButtInputsimulsk3();
	afx_msg void OnBnClickedButtInputsimulsk4();
	afx_msg void OnBnClickedButtInputsimulsk1val();
	CEdit var_inputSimul_status;
};
