// BacklightDialog.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "PclTestApp.h"
#include "BacklightDialog.h"
#include "afxdialogex.h"
#include "pdautil.h"

int g_lock;



// Bo�te de dialogue BacklightDialog

IMPLEMENT_DYNAMIC(BacklightDialog, CDialogEx)

BacklightDialog::BacklightDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(BacklightDialog::IDD, pParent)
	, m_radioLock(0)
{

}

BacklightDialog::~BacklightDialog()
{
}

void BacklightDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO_LOCK, m_radioLock);
}


BEGIN_MESSAGE_MAP(BacklightDialog, CDialogEx)
	ON_BN_CLICKED(IDOK, &BacklightDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &BacklightDialog::OnBnClickedCancel)
END_MESSAGE_MAP()


// Gestionnaires de messages de BacklightDialog


void BacklightDialog::OnBnClickedOk()
{
	UpdateData(TRUE);
	if (m_radioLock == 0)
	{
		g_lock = 3;
	}
	else
	{
		g_lock = 0;
	}
	CDialogEx::OnOK();
}


void BacklightDialog::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}
