// TestThread.cpp: define el punto de entrada de la aplicación de consola.
//

#include "pugixml.hpp"
#include "Wpos.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <windows.h>
#include <string.h>

#define BUFFERSIZE 16*1024

using namespace std;


struct xml_string_writer: pugi::xml_writer
{
    std::string result;

    virtual void write(const void* data, size_t size)
    {
        result.append(static_cast<const char*>(data), size);
    }
};

struct xml_memory_writer: pugi::xml_writer
{
    char* buffer;
    size_t capacity;

    size_t result;

    xml_memory_writer(): buffer(0), capacity(0), result(0)
    {
    }

    xml_memory_writer(char* buffer, size_t capacity): buffer(buffer), capacity(capacity), result(0)
    {
    }

    size_t written_size() const
    {
        return result < capacity ? result : capacity;
    }

    virtual void write(const void* data, size_t size)
    {
        if (result < capacity)
        {
            size_t chunk = (capacity - result < size) ? capacity - result : size;

            memcpy(buffer + result, data, chunk);
        }

        result += size;
    }
};

BOOL response_Message(char* outbuffer,char* amount, char* currentcode, char *tip, char* AdditBussines);

char* xmlresponse=
	"<response_message>\
		<header type='sale' message_id='1' version='01'/>\
			<data>\
				<field name='AcceptanceCode'>91</field>\
				<field name='TransactionData'> <![CDATA[DATOS A DISPLAY EN FORMATO JSON]]>  </field>\
				<field name='CashoutData'>1</field>\
			</data>\
	</response_message>";

char* oriol =
	"<?xml version=\"1.0\" encoding=\"utf-8\"?>\
	<request_message>\
		<header type=\"sale\" message_id=\"0\" version=\"01\" />\
			<data>\
			<field name=\"Tip\">0</field>\
			<field name=\"Amount\">000000001299</field>\
			<field name=\"CurrencyCode\">978</field>\
			<field name=\"AdditionalBusinessData\"></field>\
			</data>\
	</request_message>";


BOOL Sale_Message(char* buffer, char* amount, char* currentcode, char *tip, char* AdditBussines , int *xmllen, char *timestamp)
{

    const char source[] = "<request_message> </request_message>";
	char ctime[128];
    size_t size = sizeof(source);

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_buffer(source, size);

	//<header type='sale' message_id='1' version='01'/>  

	pugi::xml_node decl = doc.prepend_child(pugi::node_declaration);
    decl.append_attribute("version") = "1.0";
    decl.append_attribute("encoding") = "UTF-8";

	pugi::xml_node header = doc.child("request_message").append_child("header");
	header.append_attribute("type") = "sale";
	header.append_attribute("message_id") = "1";
	header.append_attribute("version") = "01";
	
	sprintf_s(ctime,"%llu",TimeStamp());
	header.append_attribute("timestamp") = ctime;

    // add description node with text child
    pugi::xml_node data = doc.child("request_message");
    // add param node before the description

	pugi::xml_node field= data.append_child("data");

	pugi::xml_node param = field.append_child("field");
    param.append_attribute("name") = "Amount";
	param.text()=amount;

	param = field.append_child("field");
    param.append_attribute("name") = "CurrencyCode";
	param.text()=currentcode;

	param = field.append_child("field");
    param.append_attribute("name") = "Tip";
	param.text()=tip;
    
	param = field.append_child("field");
	param.append_attribute("name") = "AdditionalBusinessData";
	param = param.append_child(pugi::node_cdata); 
	param.set_value(AdditBussines);

	doc.save_file("SendSale.xml");

    xml_memory_writer counter;
    doc.print(counter);

    // allocate necessary size (+1 for null termination)
    //char* buffer = new char[counter.result + 1];

    // second pass: actual printing

	char * xmlbuffer = (char*) malloc(BUFFERSIZE);
//	char * tmp = (char*) malloc(BUFFERSIZE*4);

    xml_memory_writer writer(xmlbuffer, counter.result);
    doc.print(writer);

    // null terminate

	memcpy_s(buffer,(int)counter.result,xmlbuffer,(int)counter.result);
	//memcpy_s(buffer,(int)strlen(sample),sample,(int)strlen(sample));
	//memcpy_s(buffer,(int)strlen(oriol),oriol,(int)strlen(oriol));

	int xmlc= (int)counter.result; //strlen(oriol); //
	*xmllen=(int)xmlc;

	free(xmlbuffer);
	return true;

}

BOOL Refund_Message(char* buffer, char* amount, char* currentcode, char *OriginalTransactionNumber, char* AdditBussines , int *xmllen, char *timestamp)
{

    const char source[] = "<request_message> </request_message>";
	char ctime[128];
    size_t size = sizeof(source);

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_buffer(source, size);

	//<header type='sale' message_id='1' version='01'/>  

	pugi::xml_node decl = doc.prepend_child(pugi::node_declaration);
    decl.append_attribute("version") = "1.0";
    decl.append_attribute("encoding") = "UTF-8";

	pugi::xml_node header = doc.child("request_message").append_child("header");
	header.append_attribute("type") = "refund";
	header.append_attribute("message_id") = "1";
	header.append_attribute("version") = "01";
	
	sprintf_s(ctime,"%llu",TimeStamp());
	header.append_attribute("timestamp") = ctime;

    // add description node with text child
    pugi::xml_node data = doc.child("request_message");
    // add param node before the description

	pugi::xml_node field= data.append_child("data");

	pugi::xml_node param = field.append_child("field");
    param.append_attribute("name") = "Amount";
	param.text()=amount;

	param = field.append_child("field");
    param.append_attribute("name") = "CurrencyCode";
	param.text()=currentcode;

	param = field.append_child("field");
    param.append_attribute("name") = "OriginalTransactionNumber";
	param.text()=OriginalTransactionNumber;
    
	param = field.append_child("field");
	param.append_attribute("name") = "AdditionalBusinessData";
	param.text()=AdditBussines;

	doc.save_file("SendRefund.xml");

     xml_memory_writer counter;
    doc.print(counter);

    // allocate necessary size (+1 for null termination)
    //char* buffer = new char[counter.result + 1];

    // second pass: actual printing

	char * xmlbuffer = (char*) malloc(BUFFERSIZE);
//	char * tmp = (char*) malloc(BUFFERSIZE*4);

    xml_memory_writer writer(xmlbuffer, counter.result);
    doc.print(writer);

    // null terminate

	int len = strlen(xmlbuffer);
	memcpy_s(buffer,len,xmlbuffer,len);

	int xmlc= (int)counter.result;
	*xmllen=(int)xmlc;

	free(xmlbuffer);
	return true;
}


BOOL End_Message(char* buffer, char *end, int *xmllen, char *timestamp)
{

    const char source[] = "<request_message> </request_message>";
	char ctime[128];
    size_t size = sizeof(source);

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_buffer(source, size);

	//<header type='sale' message_id='1' version='01'/>  

	pugi::xml_node decl = doc.prepend_child(pugi::node_declaration);
    decl.append_attribute("version") = "1.0";
    decl.append_attribute("encoding") = "UTF-8";

	pugi::xml_node header = doc.child("request_message").append_child("header");
	header.append_attribute("type") = "end";
	header.append_attribute("message_id") = "1";
	header.append_attribute("version") = "01";
	
	sprintf_s(ctime,"%llu",TimeStamp());
	//header.append_attribute("timestamp") = ctime;

    // add description node with text child
    pugi::xml_node data = doc.child("request_message");
    // add param node before the description

	pugi::xml_node field= data.append_child("data");

	pugi::xml_node param = field.append_child("field");
    param.append_attribute("name") = "EndCode";
	param.text()=end;

	doc.save_file("SendEnd.xml");

    xml_memory_writer counter;
    doc.print(counter);

    // allocate necessary size (+1 for null termination)
    //char* buffer = new char[counter.result + 1];

    // second pass: actual printing

	char * xmlbuffer = (char*) malloc(BUFFERSIZE);
//	char * tmp = (char*) malloc(BUFFERSIZE*4);

    xml_memory_writer writer(xmlbuffer, counter.result);
    doc.print(writer);

    // null terminate

	int len = strlen(xmlbuffer);
	memcpy_s(buffer,len,xmlbuffer,len);

	int xmlc= (int)counter.result;
	*xmllen=(int)xmlc;

	free(xmlbuffer);
	return true;

}


BOOL Request_Sale_Message(char* AcceptanceCode, char* TransactionData, char* CashoutData){

	pugi::xml_document docresp;
	pugi::xml_parse_result result = docresp.load_string(xmlresponse);
	//pugi::xml_parse_result result = docresp.load_file("RenponseMessage.xml");
	pugi::xml_node header = docresp.child("response_message").child("header");
	pugi::xml_node datas = docresp.child("response_message").child("data");

/*
    for (pugi::xml_node field = datas.child("field"); field; field = field.next_sibling("field"))
    {
		std::cout << field.attribute("name").value()<< " -->"<< field.child_value() <<std::endl;
    }

*/
	pugi::xml_node da= datas.first_child();
	//std::cout << da.attribute("name").value()<< " -->"<< da.child_value() <<std::endl;
	memcpy(AcceptanceCode,da.child_value(),16);
    pugi::xml_node de= da.next_sibling("field");
	//std::cout << de.attribute("name").value()<< " -->"<< de.child_value() <<std::endl;
	memcpy(TransactionData,de.child_value(),512);
    pugi::xml_node di= de.next_sibling("field");
	//std::cout << di.attribute("name").value()<< " -->"<< di.child_value() <<std::endl;
	memcpy(CashoutData,di.child_value(),16);


	return 0;
}


BOOL Request_Acceptance(char *buffer, char *AcceptanceCode){

	pugi::xml_document docresp;
	pugi::xml_parse_result result = docresp.load_string(buffer);
	//pugi::xml_parse_result result = docresp.load_file("RenponseMessage.xml");
	pugi::xml_node header = docresp.child("response_message").child("header");
	pugi::xml_node datas = docresp.child("response_message").child("data");

	pugi::xml_node da= datas.first_child();
	//std::cout << da.attribute("name").value()<< " -->"<< da.child_value() <<std::endl;
	memcpy(AcceptanceCode,da.child_value(),16);

	return 0;
}

