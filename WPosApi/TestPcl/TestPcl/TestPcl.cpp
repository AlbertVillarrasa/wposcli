// TestPcl.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include "pdautil.h"
#include "WposApi.h"
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds


int _tmain(int argc, _TCHAR* argv[])
{
	char *buffer;
	buffer = (char *) malloc(100);
	int r = strcpy_s(buffer, sizeof "WPOS", "WPOS");
	printf("\nNumero %u\nStatus: %s \n",OpenPcl(buffer),buffer);

	exit(0);

	if(startPclService())
	{
		printf("El servicio esta funcionando\n");
		char result;
		//std::this_thread::sleep_for (std::chrono::seconds(1));

		// Get Telium terminal information
		BOOL bReturn = FALSE;
		spmInfo_t iwlInfo;
		char buffer[256];
		int time=0;

		while(bReturn==FALSE && time==0){
			
			if (serverStatus(&result))	{
				printf("Server Status ?\n");
				//std::this_thread::sleep_for (std::chrono::seconds(1));
				if (result == 1)
					time=1;
				else
					time=0;

			}
		}

		printf("Server Status OK\n");

		while(1)
		{
			bReturn=getTerminalInfo(&iwlInfo);
			if (bReturn == FALSE)
			{
				printf("Get Info KO\n");
				if(stopPclService())
					printf("PCL Service stopped");
				else
					printf("PCL Service not stopped");

				return -1;
			}
			else
				{
					printf("Get Info OK\n");
					sprintf_s(buffer,"Serial Number: %x \r\nProduct Number: %x\r\n",iwlInfo.dwSerialNumber,iwlInfo.dwProductNumber);
					printf("%s",buffer);
			}
		}

	}


	return -1;
}

