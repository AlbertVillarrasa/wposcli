#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#define BUFFER_SIZE_MESSAGE 256
#define LENSTATUSMESSAGE 256

using namespace std::chrono;
char* textStatus;

void DoEvents()
{
    MSG msg;
    BOOL result;

    while ( ::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE ) )
    {
        result = ::GetMessage(&msg, NULL, 0, 0);
        if (result == 0) // WM_QUIT
        {                
            ::PostQuitMessage(msg.wParam);
            break;
        }
        else if (result == -1)
        {
             // Handle errors/exit application, etc.
        }
        else 
        {
            ::TranslateMessage(&msg);
            :: DispatchMessage(&msg);
        }
    }
}

__int64 tiempoepoch=0;

__int64 TimeStamp(){
		system_clock::time_point tp = system_clock::now();
		system_clock::duration dtn = tp.time_since_epoch();
		return dtn.count();
}

void Message(char* info, char *text, int val){
	textStatus = (char*)malloc(BUFFER_SIZE_MESSAGE);
	sprintf_s(textStatus,LENSTATUSMESSAGE,text,val);
	memcpy_s(info,LENSTATUSMESSAGE,textStatus,LENSTATUSMESSAGE);
	free(textStatus);
}


void Message(char* info, char *text, char* string, __int64 val){
	textStatus = (char*)malloc(BUFFER_SIZE_MESSAGE);
	sprintf_s(textStatus,LENSTATUSMESSAGE,text,string,val);
	memcpy_s(info,LENSTATUSMESSAGE,textStatus,LENSTATUSMESSAGE);
	free(textStatus);
}

void Message(char* info, char *text, char*val){
	textStatus = (char*)malloc(BUFFER_SIZE_MESSAGE);
	sprintf_s(textStatus,LENSTATUSMESSAGE,text);
	memcpy_s(info,LENSTATUSMESSAGE,textStatus,LENSTATUSMESSAGE);
	free(textStatus);
}

void Message(char* info, char *text){
	textStatus = (char*)malloc(BUFFER_SIZE_MESSAGE);
	sprintf_s(textStatus,LENSTATUSMESSAGE,text);
	memcpy_s(info,LENSTATUSMESSAGE,textStatus,LENSTATUSMESSAGE);
	free(textStatus);
}