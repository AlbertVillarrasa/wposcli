#include <winsock.h>

int OpenSocket(char *hostname, int port, UINT &sock,  int sockeytimeout, char *info);
int SaleFullDuplex(char *hostname, int port, char *sendbuf, char *recvbuffer, char *info, int xmllen , int sockeytimeout);
int Send(UINT &sock, char *sendbuf, char *info, int xmllen);
int Received(UINT &sock, char *recvbuffer, char *info);
int ReceivedBytes(UINT &sock, char *recvbuffer, char *info, int timeout) ;
int ReceivedByteToByte(UINT &sock, char *recvbuffer); 


__int64 TimeStamp();