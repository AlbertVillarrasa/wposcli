#define WIN32_LEAN_AND_MEAN
//#include "stdafx.h"

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "pdautil.h"
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#include "Utils.hpp"

#include "PclUtilities.h"


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 64*1024
#define DEFAULT_PORT 4666

#define LOOPS 2

#define RECV_TIMEOUT 500


BYTE* bufferReceived;

//#define REPEAT


int OpenSocket(char *hostname, int port, UINT &sock,  int sockeytimeout, char *info) 
{

	int pkt_sent = 0, pkt_recvd = 0;
	int bytes_sent= 0, bytes_recvd = 0;
	int error = 0;
	
	WSADATA wsa;
	//BYTE bufferToSend [2048];
    if (WSAStartup(MAKEWORD(2, 2), &wsa))
	{
		return -1;
	}
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock == INVALID_SOCKET)
	{
		Message(info,"socket failed",NULL);
		WSACleanup();
		return -1;
	}

	struct hostent *hostinfo = NULL;
	SOCKADDR_IN sin = { 0 }; /* initialise la structure avec des 0 */
	//const char *hostname = "127.0.0.1";
	
	sin.sin_port = htons(port); /* on utilise htons pour le port */
	sin.sin_family = AF_INET;
	setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,(char *)&sockeytimeout,sizeof(int)); //setting the receive timeout

	int a = 32768;
	if (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char *)&a, sizeof(int)) == -1) {
		Message(info,"Error setting socket opts: %s\n",(char*)strerror(errno));
		return -1;
	}

	//SEND PACKETS FROM WINDOWS TO TELIUM
	int ret = addDynamicBridge(port, BRIDGE_TOWARDS_TELIUM);
	if (ret == 0 || ret == -2)
	{
		sin.sin_addr.s_addr = inet_addr(hostname);
		printf("Connection in progress...");
		if(connect(sock,(SOCKADDR *) &sin, sizeof(sin)) == SOCKET_ERROR)
		{
			int error = WSAGetLastError();
			shutdown(sock, SD_BOTH);
			closesocket(sock);
			WSACleanup();
			Message(info,"Connection failed with error %d",error);
			return -1;
		}

	}else
		{
			shutdown(sock, SD_BOTH);
			closesocket(sock);
			Message(info,"Error on addDynamicBridge: %d",ret);
			return -2;
		}
	Message(info, "Connected");
	return 1;

	
}

int Send(UINT &sock, char *sendbuf, char *info, int xmllen) 
{
	int error = 0;
	int n=0;
	if((n = send(sock, (const char*)sendbuf, xmllen, 0)) < 0)
	{
		error = WSAGetLastError();
		shutdown(sock, SD_BOTH);
		closesocket(sock);
		Message(info,"send failed with error %d",(char*)strerror(error));
		return -1;
	}

	Message(info,"Enviados %d",n);

	return n;

}

int ReceivedBytes(UINT &sock, char *recvbuffer, char *info, int timeout) 
{
	BYTE *bufferReceived;
	bufferReceived= (BYTE*)malloc(BUFFER_SIZE*sizeof(BYTE));
	BYTE buffer[1];
	int n;
	int error = 0;
	int rx = 0;
	int recvbytes=0;
	int waitloops=0;
	int timeoutrecv= RECV_TIMEOUT;
	
	setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,(char *)&timeoutrecv,sizeof(int));

	do{
		n = recv(sock,  (char*)&buffer[0], 1, 0);
		if(n==1){
			bufferReceived[recvbytes]=buffer[0];
			recvbytes++;			
			}
		else if (n==0){
			break;
		}else{
			DoEvents();	// Timepo de espera 100mS
			waitloops++;
		}
	} while ((n!=-1 || recvbytes<=1) && waitloops<(timeout/100));

	if(n==0){
		Message(info,"Se ha producido un error en la conexion...: %d",n);
		free(bufferReceived);
		return -1;
	}else if (waitloops>=(timeout/100)){
		Message(info,"Se ha producido un error en el time out de la conexion...: %d",n);
		free(bufferReceived);
		return -1;
	}else if(recvbytes<BUFFER_SIZE){
		setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,(char *)&timeout,sizeof(int));
		memcpy_s(recvbuffer,recvbytes-1,&bufferReceived[0],recvbytes-1);
		free(bufferReceived);
	}else
	{
		Message(info,"Se ha producido un error en la conexion...: %d",n);
		free(bufferReceived);
		return -1;
	}

	Message(info,"Recvd: %d",recvbytes);
return recvbytes;

}

int ReceivedByteToByte(UINT &sock, char *recvbuffer) 
{
	int timeoutrecv= RECV_TIMEOUT;
	setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,(char *)&timeoutrecv,sizeof(int));
	int n = recv(sock,  (char*)recvbuffer, 1, 0);

	return n;
}


int Received(UINT &sock, char *recvbuffer, char *info, int timeout) 
{
	BYTE buffer[1];
	int n;
	int error = 0;
	int rx = 0;
	int recvbytes=0;
	int waitloops=0;
	int timeoutrecv= RECV_TIMEOUT;

	bufferReceived= new BYTE[BUFFER_SIZE];
	
	setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,(char *)&timeoutrecv,sizeof(int));

	do{
		n = recv(sock,  (char*)&buffer[0], 1, 0);
		if(n==1){
			bufferReceived[recvbytes]=buffer[0];
			recvbytes++;			
			}
		else if (n==0){
			break;
		}else{
			DoEvents();	// Timepo de espera 100mS
			waitloops++;
		}
	} while ((n!=-1 || recvbytes<=1) && waitloops<(timeout/100));

	if(n==0){
		Message(info,"Se ha producido un error en la conexion...: %d",n);
		free(bufferReceived);
		return -1;
	}else if (waitloops>=(timeout/100)){
		Message(info,"Se ha producido un error en el time out de la conexion...: %d",n);
		free(bufferReceived);
		return -1;
	}else if(recvbytes<BUFFER_SIZE){
		setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,(char *)&timeout,sizeof(int));
		memcpy_s(recvbuffer,recvbytes-1,&bufferReceived[0],recvbytes-1);
		free(bufferReceived);
	}else
	{
		Message(info,"Se ha producido un error en la conexion...: %d",n);
		return -1;
	}

	Message(info,"Recvd: %d",recvbytes);
	free(bufferReceived);

return recvbytes;

}


