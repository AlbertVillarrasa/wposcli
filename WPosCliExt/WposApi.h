#ifndef INTERFAZWPOSAPI_H
#define INTERFAZWPOSAPI_H

//Codigos de error


typedef unsigned int APIRET;
typedef unsigned int UINT;

#ifndef EXPORT_DLL
#define EXPORT_DLL __declspec(dllexport)
#endif


EXPORT_DLL	APIRET Importe(char* source);

EXPORT_DLL	APIRET WposSocket(char *host,int port , UINT timestocheck,char *info);

EXPORT_DLL	APIRET SendMensage(char* source);

EXPORT_DLL	APIRET ServerStatus(char* result);

EXPORT_DLL	APIRET OpenPclService(char* info, UINT time );

EXPORT_DLL  APIRET StopPclService();

EXPORT_DLL	APIRET OpenWpos(char *host,int port ,char* info,int buffersize, UINT scoketimeout, UINT  timestries );

EXPORT_DLL	APIRET SaleWpos(char *buffer, char *info, char *importe , char *currentCode, char *tip, char *additionalBusinessData);

EXPORT_DLL	APIRET RefundWpos(char *outBuffer, char *info, char *importe , char *currentCode, char *OriginalTransactionNumber, char *additionalBusinessData);

EXPORT_DLL  APIRET FinalWpos(char *outBuffer, char *info, char *acceptance,char *code);

EXPORT_DLL	APIRET Sale(char *buffer, char *info, char *importe , char *currentCode, char *tip, char *additionalBusinessData);

EXPORT_DLL	APIRET Recv(char *buffer);


#endif // #ifndef INTERFAZWPOSAPI_H