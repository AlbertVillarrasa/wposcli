#pragma once

#include "WposApi.h"
#include "Windows.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <chrono>         // std::chrono::seconds


namespace WposCli {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace std::chrono;
	using namespace	System::Xml;

	#define BUFFERSIZE 128*1024
	#define MAX_CHAR	100
	#define SALETIME	20

	#define TRIESCONNECT	10

	#define HOST	"127.0.0.1"
	#define PORT	4666
	#define TIMEOUT	5*60*1000;

	/// <summary>
	/// Resumen de WposForm
	/// </summary>

	public ref class WposForm : public System::Windows::Forms::Form
	{
	public:
	public: unsigned int saletimer;
	public:	unsigned int initImporte;
	public:	unsigned int ticks;
	public: char*  cImport;
	public: FileStream ^ fs;
	private: System::Windows::Forms::TabControl^  tabControl1;
	public: 
	private: System::Windows::Forms::TabPage^  tabPage1;

	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::TextBox^  XmlViewer;
	private: System::Windows::Forms::WebBrowser^  WebViewer;
	private: System::Windows::Forms::Button^  StartByCalls;

	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::TextBox^  FinalizacioMessage;
	private: System::Windows::Forms::Button^  RefundBoton;
	private: System::Windows::Forms::TextBox^  NumDevolucion;




	public: 

	public: 
	public:	StreamWriter ^  sw;
		WposForm(void)
		{
			InitializeComponent();
			initImporte=200;
			ticks=0;
			saletimer=0;

			fs = File::Create("salelog.txt");
			sw = gcnew StreamWriter(fs);
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}

		protected:
		/// <summary>
		/// Limpiar los recursos que se est�n utilizando.
		/// </summary>
		~WposForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  BotonStartService;
	protected: 

	private: System::Windows::Forms::Button^  BotonStopService;
	private: System::Windows::Forms::Button^  SaleBoton;

	private: System::Windows::Forms::TextBox^  TextImport;


	private: System::Windows::Forms::TextBox^  Pantalla;


	private: System::Windows::Forms::TextBox^  TextMessaje;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::Button^  LoopSale;
	private: System::ComponentModel::IContainer^  components;


	protected: 


	protected: 

	private:
		/// <summary>
		/// Variable del dise�ador requerida.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido del m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->BotonStartService = (gcnew System::Windows::Forms::Button());
			this->Pantalla = (gcnew System::Windows::Forms::TextBox());
			this->BotonStopService = (gcnew System::Windows::Forms::Button());
			this->SaleBoton = (gcnew System::Windows::Forms::Button());
			this->TextImport = (gcnew System::Windows::Forms::TextBox());
			this->TextMessaje = (gcnew System::Windows::Forms::TextBox());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->LoopSale = (gcnew System::Windows::Forms::Button());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->XmlViewer = (gcnew System::Windows::Forms::TextBox());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->WebViewer = (gcnew System::Windows::Forms::WebBrowser());
			this->StartByCalls = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->FinalizacioMessage = (gcnew System::Windows::Forms::TextBox());
			this->RefundBoton = (gcnew System::Windows::Forms::Button());
			this->NumDevolucion = (gcnew System::Windows::Forms::TextBox());
			this->tabControl1->SuspendLayout();
			this->tabPage2->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->SuspendLayout();
			// 
			// BotonStartService
			// 
			this->BotonStartService->Enabled = false;
			this->BotonStartService->Location = System::Drawing::Point(144, 2);
			this->BotonStartService->Name = L"BotonStartService";
			this->BotonStartService->Size = System::Drawing::Size(110, 28);
			this->BotonStartService->TabIndex = 0;
			this->BotonStartService->Text = L"Start Service";
			this->BotonStartService->UseVisualStyleBackColor = true;
			this->BotonStartService->Click += gcnew System::EventHandler(this, &WposForm::Boton_StartService_Click);
			// 
			// Pantalla
			// 
			this->Pantalla->Location = System::Drawing::Point(544, 10);
			this->Pantalla->Name = L"Pantalla";
			this->Pantalla->Size = System::Drawing::Size(309, 20);
			this->Pantalla->TabIndex = 0;
			// 
			// BotonStopService
			// 
			this->BotonStopService->Location = System::Drawing::Point(260, 2);
			this->BotonStopService->Name = L"BotonStopService";
			this->BotonStopService->Size = System::Drawing::Size(106, 28);
			this->BotonStopService->TabIndex = 2;
			this->BotonStopService->Text = L"StopService";
			this->BotonStopService->UseVisualStyleBackColor = true;
			this->BotonStopService->Click += gcnew System::EventHandler(this, &WposForm::BotonStopService_Click);
			// 
			// SaleBoton
			// 
			this->SaleBoton->Enabled = false;
			this->SaleBoton->Location = System::Drawing::Point(28, 36);
			this->SaleBoton->Name = L"SaleBoton";
			this->SaleBoton->Size = System::Drawing::Size(110, 23);
			this->SaleBoton->TabIndex = 3;
			this->SaleBoton->Text = L"Sale";
			this->SaleBoton->UseVisualStyleBackColor = true;
			this->SaleBoton->Click += gcnew System::EventHandler(this, &WposForm::SendBoton_Click);
			// 
			// TextImport
			// 
			this->TextImport->Location = System::Drawing::Point(260, 39);
			this->TextImport->Name = L"TextImport";
			this->TextImport->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->TextImport->Size = System::Drawing::Size(117, 20);
			this->TextImport->TabIndex = 4;
			this->TextImport->Text = L"00000000020";
			this->TextImport->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->TextImport->Leave += gcnew System::EventHandler(this, &WposForm::TextImport_Leave);
			// 
			// TextMessaje
			// 
			this->TextMessaje->Location = System::Drawing::Point(24, 536);
			this->TextMessaje->Name = L"TextMessaje";
			this->TextMessaje->Size = System::Drawing::Size(829, 20);
			this->TextMessaje->TabIndex = 9;
			// 
			// timer1
			// 
			this->timer1->Interval = 1000;
			this->timer1->Tick += gcnew System::EventHandler(this, &WposForm::timer1_Tick);
			// 
			// LoopSale
			// 
			this->LoopSale->Location = System::Drawing::Point(716, 60);
			this->LoopSale->Name = L"LoopSale";
			this->LoopSale->Size = System::Drawing::Size(137, 38);
			this->LoopSale->TabIndex = 10;
			this->LoopSale->Text = L"Loop";
			this->LoopSale->UseVisualStyleBackColor = true;
			this->LoopSale->Click += gcnew System::EventHandler(this, &WposForm::LoopSale_Click);
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Location = System::Drawing::Point(24, 104);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(829, 426);
			this->tabControl1->TabIndex = 11;
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->XmlViewer);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(821, 400);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Xml";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// XmlViewer
			// 
			this->XmlViewer->AcceptsReturn = true;
			this->XmlViewer->AcceptsTab = true;
			this->XmlViewer->Location = System::Drawing::Point(6, 6);
			this->XmlViewer->Multiline = true;
			this->XmlViewer->Name = L"XmlViewer";
			this->XmlViewer->ScrollBars = System::Windows::Forms::ScrollBars::Both;
			this->XmlViewer->Size = System::Drawing::Size(809, 388);
			this->XmlViewer->TabIndex = 0;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->WebViewer);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(821, 400);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Html";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// WebViewer
			// 
			this->WebViewer->Location = System::Drawing::Point(3, 3);
			this->WebViewer->MinimumSize = System::Drawing::Size(20, 20);
			this->WebViewer->Name = L"WebViewer";
			this->WebViewer->Size = System::Drawing::Size(812, 394);
			this->WebViewer->TabIndex = 0;
			// 
			// StartByCalls
			// 
			this->StartByCalls->Location = System::Drawing::Point(28, 2);
			this->StartByCalls->Name = L"StartByCalls";
			this->StartByCalls->Size = System::Drawing::Size(110, 28);
			this->StartByCalls->TabIndex = 12;
			this->StartByCalls->Text = L"New Start Service";
			this->StartByCalls->UseVisualStyleBackColor = true;
			this->StartByCalls->Click += gcnew System::EventHandler(this, &WposForm::StartByCalls_Click);
			// 
			// button2
			// 
			this->button2->Enabled = false;
			this->button2->Location = System::Drawing::Point(144, 36);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(110, 23);
			this->button2->TabIndex = 13;
			this->button2->Text = L"SaleByCalls";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &WposForm::button2_Click);
			// 
			// FinalizacioMessage
			// 
			this->FinalizacioMessage->Location = System::Drawing::Point(383, 39);
			this->FinalizacioMessage->Name = L"FinalizacioMessage";
			this->FinalizacioMessage->Size = System::Drawing::Size(123, 20);
			this->FinalizacioMessage->TabIndex = 14;
			this->FinalizacioMessage->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// RefundBoton
			// 
			this->RefundBoton->Location = System::Drawing::Point(28, 65);
			this->RefundBoton->Name = L"RefundBoton";
			this->RefundBoton->Size = System::Drawing::Size(110, 23);
			this->RefundBoton->TabIndex = 15;
			this->RefundBoton->Text = L"Refund";
			this->RefundBoton->UseVisualStyleBackColor = true;
			this->RefundBoton->Click += gcnew System::EventHandler(this, &WposForm::Refund_Click);
			// 
			// NumDevolucion
			// 
			this->NumDevolucion->Location = System::Drawing::Point(144, 65);
			this->NumDevolucion->Name = L"NumDevolucion";
			this->NumDevolucion->Size = System::Drawing::Size(110, 20);
			this->NumDevolucion->TabIndex = 1;
			this->NumDevolucion->Text = L"000000000000";
			this->NumDevolucion->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// WposForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(881, 568);
			this->Controls->Add(this->NumDevolucion);
			this->Controls->Add(this->RefundBoton);
			this->Controls->Add(this->FinalizacioMessage);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->StartByCalls);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->LoopSale);
			this->Controls->Add(this->TextMessaje);
			this->Controls->Add(this->Pantalla);
			this->Controls->Add(this->TextImport);
			this->Controls->Add(this->SaleBoton);
			this->Controls->Add(this->BotonStopService);
			this->Controls->Add(this->BotonStartService);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Location = System::Drawing::Point(42, 93);
			this->Name = L"WposForm";
			this->Text = L"WPos";
			this->Load += gcnew System::EventHandler(this, &WposForm::WposForm_Load);
			this->tabControl1->ResumeLayout(false);
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			this->tabPage1->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Boton_StartService_Click(System::Object^  sender, System::EventArgs^  e) {

			fs = File::Create("salelog.txt");
			sw = gcnew StreamWriter(fs);

			Application::DoEvents();

			char buffer[BUFFERSIZE*sizeof(char)];
			char apiname[]="WPOS";
			char result=0;

			String^ name = gcnew String(buffer);
			int timeout =TIMEOUT;
			if(int n=OpenWpos(HOST,PORT,buffer,(int)BUFFERSIZE,timeout,(UINT)10)==1)
			{
				String^ info = gcnew String(buffer);
				this->Pantalla->Text=L""+info;
				this->button2->Enabled=true;
				this->SaleBoton->Enabled=true;
				ticks=0;
			}else
				{
					String^ info = gcnew String(buffer);
					this->Pantalla->Text=L""+info;
					this->BotonStartService->Enabled=false;
					this->button2->Enabled=false;
				}  
		 }
private: System::Void BotonStopService_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			 sw->Close();
			 StopPclService();
			 this->timer1->Enabled= false;
			 this->XmlViewer->Text=L"";
			 this->TextMessaje->Text=L"";
			 this->SaleBoton->Enabled=false;
			 this->button2->Enabled=false;
			 this->BotonStartService->Enabled=true;
			 this->Pantalla->Text=L"Puerto Wpos Cerrado";
		 }
private: System::Void WposForm_Load(System::Object^  sender, System::EventArgs^  e) {

			STARTUPINFO info={sizeof(info)};
			PROCESS_INFORMATION processInfo;

			WCHAR szCmdLine[] = L"\"C:\\Python27\\python.exe\" C:\\Users\\Albert\\Desktop\\Comercia_Server_Host\\ComerciaServer.py 10.5.38.9 10010";
			if (CreateProcess(NULL,szCmdLine, NULL, NULL, TRUE, 0, NULL, NULL, &info, &processInfo)){
				WaitForSingleObject(processInfo.hProcess, INFINITE);
				CloseHandle(processInfo.hProcess);
				CloseHandle(processInfo.hThread);
			}

			 char  cImport[BUFFERSIZE*sizeof(char)];
			 StopPclService();
			 sw->Close();
			 this->Pantalla->Text=L"Pulse Start Service para abrir la comunicacion...";
			 this->XmlViewer->Text=L"";
			 this->TextMessaje->Text=L"";
			 this->SaleBoton->Enabled=false;	// false
			 this->button2->Enabled=false;	// false
			 sprintf_s(cImport, "%012d", initImporte,64);
			 String^ importe = gcnew String(cImport);
			 this->TextImport->Text= importe;


		 }
private: System::Void SendBoton_Click(System::Object^  sender, System::EventArgs^  e) {

			 char  outBuffer[BUFFERSIZE*sizeof(char)];
			 char  info[BUFFERSIZE*sizeof(char)];
			 char  cImport[BUFFERSIZE*sizeof(char)];
			 memset(outBuffer,'\x00',BUFFERSIZE*sizeof(char));
			 memset(cImport,'\x00',BUFFERSIZE*sizeof(char));
			 unsigned long len =(unsigned long)strlen(outBuffer);

			 this->SaleBoton->Enabled=false;
			 this->TextMessaje->Text=L"";
			 this->XmlViewer->Text=L"";
			 this->FinalizacioMessage->Text=L"";
			 Application::DoEvents();

			 sprintf_s(cImport, "%012d", initImporte);
			 int n =SaleWpos(outBuffer,info,cImport,"0978","100","TestAdditBussines");
			 String^ inf = gcnew String(info);
			 this->TextMessaje->Text=L""+inf;

			 if(n!=-1){
	 			 String^ ot = gcnew String(outBuffer);
				 this->XmlViewer->Text=ot;

				char  cHora[BUFFERSIZE*sizeof(char)];
				System::DateTime^ now = System::DateTime::Now;
				sprintf_s(cHora, "%02d:%02d:%02d ", now->Hour,now->Minute,now->Second,64);
				String^ hora = gcnew System::String(cHora);
				sw->AutoFlush;
				sw->WriteLine(hora+ "--> "+inf);

				if(ot->Length>1){
					Xml::XmlDataDocument^ data = gcnew System::Xml::XmlDataDocument();
					data->LoadXml(ot);
					XmlNodeList^ fields= data->GetElementsByTagName("field");
					if(fields->Count>1){
							char code[16*sizeof(char)];
							String^ acept = fields->Item(0)->InnerText;

							if(String::Compare(acept,gcnew String("90"))==0 || String::Compare(acept,gcnew String("91"))==0 || String::Compare(acept,gcnew String("92"))==0) {
								FinalWpos(outBuffer,info,"00",code);
								String^c= gcnew System::String(code);
								this->FinalizacioMessage->Text=L""+c;
							}else
							{
								FinalWpos(outBuffer,info,"00",code);
							}
							this->WebViewer->DocumentText  = fields->Item(1)->InnerText;
						}else{
							String^ acept = fields->Item(0)->InnerText;
							char code[16*sizeof(char)];
							FinalWpos(outBuffer,info,"00",code);
							String^c= gcnew System::String(code);
							this->FinalizacioMessage->Text=L""+c;
						}
					}
				}
				else{
				 //CloseWpos();
				this->TextMessaje->Text=L""+inf;
				 this->Pantalla->Text=L"Se ha producido un error. Cierre el PCL...";
				 this->SaleBoton->Enabled=false;
			 }

			 this->SaleBoton->Enabled=true;

			 initImporte++;

			 sprintf_s(cImport, "%012d", initImporte,64);
			 String^ importe = gcnew String(cImport);
			 this->TextImport->Text= importe;


		 }

private: System::Void TextImport_Leave(System::Object^  sender, System::EventArgs^  e) {
			char  cImport[BUFFERSIZE*sizeof(char)];
			initImporte =int::Parse(this->TextImport->Text);
			sprintf_s(cImport, "%012d", initImporte,64);
			String^ importe = gcnew String(cImport);
			this->TextImport->Text= importe;

		 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 if(ticks==saletimer){
				 if(this->SaleBoton->Enabled==true){
					this->button2_Click(sender,e);
					saletimer=ticks+SALETIME;
				 }
			 }
			 ticks++;
		 }
private: System::Void LoopSale_Click(System::Object^  sender, System::EventArgs^  e) {
			this->timer1->Enabled=true;
			saletimer=2;
		 }

private: System::Void StartByCalls_Click(System::Object^  sender, System::EventArgs^  e) {

			fs = File::Create("salelog.txt");
			sw = gcnew StreamWriter(fs);

			char buffer[BUFFERSIZE*sizeof(char)];
			char info[BUFFERSIZE*sizeof(char)];
			char apiname[]="WPOS";
			char result=0;
			unsigned char tries=0;

			this->Pantalla->Text=L"Estableciendo Conexion... 0";
			Application::DoEvents();
			StopPclService();
			_sleep(3*1000);

			if(OpenPclService(buffer,1)==1)
			{
				do{
					ServerStatus(&result);
					Application::DoEvents();
					_sleep(1*1000);
					tries++;
					this->Pantalla->Text=L"Estableciendo Conexion... "+tries;
				}while (result!=1 && tries <TRIESCONNECT);

				if(tries<TRIESCONNECT){
					UINT timeout=TIMEOUT;
					WposSocket(HOST,PORT,timeout,info);

					this->Pantalla->Text=L"Comunicacion Establecida en " + tries+" Segundo/s";
					this->SaleBoton->Enabled=true;
				}else
				{
					this->Pantalla->Text=L"Fallo al establecer la comunicacion. Time Out ServerStatus";
					this->BotonStartService->Enabled=false;
					this->SaleBoton->Enabled=false;
				}
			}
			else{
				this->Pantalla->Text=L"Fallo al establecer la comunicacion. (OpenPclService)";
				this->BotonStartService->Enabled=false;
				this->SaleBoton->Enabled=false;
				ticks=0;
			}
		}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

			 char  outBuffer[BUFFERSIZE*sizeof(char)];
			 char  info[BUFFERSIZE*sizeof(char)];
			 char  xmlbuffer[BUFFERSIZE*sizeof(char)];
			 char  cImport[BUFFERSIZE*sizeof(char)];
			 memset(outBuffer,'\x00',BUFFERSIZE*sizeof(char));
			 memset(cImport,'\x00',BUFFERSIZE*sizeof(char));
			 unsigned long len =(unsigned long)strlen(outBuffer);
			 this->SaleBoton->Enabled=false;
			 this->TextMessaje->Text=L"";
			 this->XmlViewer->Text=L"";
//			 int n=0;

			 int timeout=TIMEOUT;

			 this->button2->Enabled=false;
			 this->SaleBoton->Enabled=false;

			 sprintf_s(cImport, "%012d", initImporte);
			 int n =Sale(outBuffer,info,cImport,"0978","100","TestAdditBussines");

			 int caraters=0;
			 char b[1];
			 n=0;
			 char  output[BUFFERSIZE*sizeof(char)];
			 memset(output,'\x00',BUFFERSIZE*sizeof(char));
			  memset(xmlbuffer,'\x00',BUFFERSIZE*sizeof(char));
			 do{
				n = Recv(b);
				if(n==1){
					output[caraters]=b[0];
					caraters++;
				}
				Application::DoEvents();
			 }while(n>=0 || caraters<1);
			 //memcpy_s(xmlbuffer,caraters,output,caraters);

	 		 String^ ot = gcnew String(output);
			 this->XmlViewer->Text=ot;
			 String^ inf = gcnew String(info);
			 this->TextMessaje->Text=L"Recv: "+caraters;

			char  cHora[BUFFERSIZE*sizeof(char)];
			System::DateTime^ now = System::DateTime::Now;
			sprintf_s(cHora, "%02d:%02d:%02d ", now->Hour,now->Minute,now->Second);
			String^ hora = gcnew System::String(cHora);
			sw->AutoFlush;
			sw->WriteLine(hora+ "--> "+inf);

			if(ot->Length>1){
				Xml::XmlDataDocument^ data = gcnew System::Xml::XmlDataDocument();
				data->LoadXml(ot);
				XmlNodeList^ fields= data->GetElementsByTagName("field");
				if(fields->Count>1){
					this->WebViewer->DocumentText  = fields->Item(1)->InnerText;
				}
			}

			 if( n==-1) {
				 //CloseWpos();
				 //this->Pantalla->Text=L"Error port timeout: "+inf;
				 this->SaleBoton->Enabled=false;
			 }

			 this->SaleBoton->Enabled=true;
			 this->button2->Enabled=true;
			 initImporte++;

			 sprintf_s(cImport, "%012d", initImporte,64);
			 String^ importe = gcnew String(cImport);
			 this->TextImport->Text= importe;


		 }
private: System::Void Refund_Click(System::Object^  sender, System::EventArgs^  e) {
			 char  outBuffer[BUFFERSIZE*sizeof(char)];
			 char  info[BUFFERSIZE*sizeof(char)];
			 char  cImport[BUFFERSIZE*sizeof(char)];
			 char  cDevolucion[BUFFERSIZE*sizeof(char)];
			 memset(outBuffer,'\x00',BUFFERSIZE*sizeof(char));
			 memset(cImport,'\x00',BUFFERSIZE*sizeof(char));
			 unsigned long len =(unsigned long)strlen(outBuffer);

			 this->SaleBoton->Enabled=false;
			 this->TextMessaje->Text=L"";
			 this->XmlViewer->Text=L"";
			 this->FinalizacioMessage->Text=L"";
			 Application::DoEvents();

			 sprintf_s(cImport, "%012d", initImporte);
			 sprintf_s(cDevolucion, "%012s", this->NumDevolucion->Text);
			 int n =RefundWpos(outBuffer,info,cImport,"0978",cDevolucion,"TestAdditBussines");
			 String^ inf = gcnew String(info);
			 this->TextMessaje->Text=L""+inf;

			 if(n!=-1){
	 			 String^ ot = gcnew String(outBuffer);
				 this->XmlViewer->Text=ot;

				char  cHora[BUFFERSIZE*sizeof(char)];
				System::DateTime^ now = System::DateTime::Now;
				sprintf_s(cHora, "%02d:%02d:%02d ", now->Hour,now->Minute,now->Second,64);
				String^ hora = gcnew System::String(cHora);
				sw->AutoFlush;
				sw->WriteLine(hora+ "--> "+inf);

				if(ot->Length>1){
					Xml::XmlDataDocument^ data = gcnew System::Xml::XmlDataDocument();
					data->LoadXml(ot);
					XmlNodeList^ fields= data->GetElementsByTagName("field");
					if(fields->Count>1){
							char code[16*sizeof(char)];
							String^ acept = fields->Item(0)->InnerText;

							if(String::Compare(acept,gcnew String("90"))==0 || String::Compare(acept,gcnew String("91"))==0 || String::Compare(acept,gcnew String("92"))==0) {
							}else
							{
							}
							FinalWpos(outBuffer,info,"00",code);
							String^c= gcnew System::String(code);
							this->FinalizacioMessage->Text=L""+c;
							this->WebViewer->DocumentText  = fields->Item(1)->InnerText;
						}else{
							String^ acept = fields->Item(0)->InnerText;
							char code[16*sizeof(char)];
							FinalWpos(outBuffer,info,"00",code);
							String^c= gcnew System::String(code);
							this->FinalizacioMessage->Text=L""+c;
						}
					}
				}
				else{
				 //CloseWpos();
				char code[16*sizeof(char)];
				FinalWpos(outBuffer,info,"00",code);
				this->TextMessaje->Text=L""+inf;
				this->Pantalla->Text=L"Se ha producido un error. Cierre el PCL...";
				this->SaleBoton->Enabled=false;
			 }

			 this->SaleBoton->Enabled=true;

			 initImporte++;

			 sprintf_s(cImport, "%012d", initImporte,64);
			 String^ importe = gcnew String(cImport);
			 this->TextImport->Text= importe;




		 }
};
}