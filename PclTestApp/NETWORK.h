#pragma once
#include "afxwin.h"


// Bo�te de dialogue NETWORK

class NETWORK : public CDialog
{
	DECLARE_DYNAMIC(NETWORK)

public:
	NETWORK(CWnd* pParent = NULL);   // constructeur standard
	virtual ~NETWORK();
	virtual BOOL OnInitDialog();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_NETWORK };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	CEdit var_networkPort;
	CEdit var_networkPacketSize;
	CEdit var_networkPacketNumber;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtNetworktest();
	CComboBox var_list_direction;
	afx_msg void OnEnKillfocusNetworkpacketsize();
	afx_msg void OnEnKillfocusNetworkpacketnumber();
	afx_msg void OnEnKillfocusNetworkport();
	CStatic m_networkStatus;
	afx_msg void OnBnClickedButtonBridge();
};
