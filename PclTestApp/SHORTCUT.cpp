// SHORTCUT.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "PclTestApp.h"
#include "SHORTCUT.h"
#include "afxdialogex.h"
#include "pdautil.h"

//Global Variables
#define BUFFER_SIZE 256
extern BOOL isKeyboardPresent;

STARTUPINFO si;
PROCESS_INFORMATION pi;
BOOL bReturnShortCutApi;

// Bo�te de dialogue SHORTCUT

IMPLEMENT_DYNAMIC(SHORTCUT, CDialog)

SHORTCUT::SHORTCUT(CWnd* pParent /*=NULL*/)
	: CDialog(SHORTCUT::IDD, pParent)
{

}

SHORTCUT::~SHORTCUT()
{
}

void SHORTCUT::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, var_shortcut_nb);
}


BEGIN_MESSAGE_MAP(SHORTCUT, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_SHORTCUTSEND, &SHORTCUT::OnBnClickedButtonShortcutsend)
	ON_BN_CLICKED(IDOK, &SHORTCUT::OnBnClickedOk)
END_MESSAGE_MAP()


// Gestionnaires de messages de SHORTCUT
BOOL SHORTCUT::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: ajoutez ici une initialisation suppl�mentaire
    if ( !isKeyboardPresent )
	{
		TCHAR  infoBuf[1024];
		infoBuf[0]='\0';
		if ( SHGetSpecialFolderPath(0,infoBuf,CSIDL_PROGRAM_FILES,FALSE) )
		{
			infoBuf[2]='\0';
			PathAppend(infoBuf, TEXT("\\Program Files\\Common Files\\microsoft shared\\ink\\tabtip.exe"));
			ShellExecute(NULL, NULL,infoBuf, NULL, NULL, SW_SHOWNORMAL);
		}
	}
	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}

void SHORTCUT::OnBnClickedButtonShortcutsend()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	char charShorcut[BUFFER_SIZE];
	wchar_t Wshortcut_nb[BUFFER_SIZE];
	size_t i;
	var_shortcut_nb.GetWindowTextW(Wshortcut_nb,256);
	wcstombs_s(&i, charShorcut, (size_t)BUFFER_SIZE, Wshortcut_nb, (size_t)BUFFER_SIZE);
	if (!isKeyboardPresent)
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	bReturnShortCutApi = launchM2OSShortcut(charShorcut);
	EndDialog(0);
}


void SHORTCUT::OnBnClickedOk()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	if (!isKeyboardPresent)
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	CDialog::OnOK();
}
