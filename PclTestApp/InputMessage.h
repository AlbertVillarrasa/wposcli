#pragma once
#include "afxwin.h"


// Bo�te de dialogue InputMessage

class InputMessage : public CDialogEx
{
	DECLARE_DYNAMIC(InputMessage)

public:
	InputMessage(CWnd* pParent = NULL);   // constructeur standard
	virtual ~InputMessage();
	virtual BOOL OnInitDialog();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_INPUT_MSG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CEdit m_input_msg;
};
