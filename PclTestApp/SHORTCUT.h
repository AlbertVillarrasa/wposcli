#pragma once
#include "afxwin.h"


// Bo�te de dialogue SHORTCUT

class SHORTCUT : public CDialog
{
	DECLARE_DYNAMIC(SHORTCUT)

public:
	SHORTCUT(CWnd* pParent = NULL);   // constructeur standard
	virtual ~SHORTCUT();
	virtual BOOL OnInitDialog();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_SHORTCUT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButtonShortcutsend();
	CEdit var_shortcut_nb;
	afx_msg void OnBnClickedOk();
};
