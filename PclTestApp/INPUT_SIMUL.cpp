// INPUT_SIMUL.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "PclTestApp.h"
#include "INPUT_SIMUL.h"
#include "afxdialogex.h"
#include "pdautil.h"

// Bo�te de dialogue INPUT_SIMUL

IMPLEMENT_DYNAMIC(INPUT_SIMUL, CDialog)

INPUT_SIMUL::INPUT_SIMUL(CWnd* pParent /*=NULL*/)
	: CDialog(INPUT_SIMUL::IDD, pParent)
{

}

INPUT_SIMUL::~INPUT_SIMUL()
{
}

void INPUT_SIMUL::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_INPUTSIMUL_STATUS, var_inputSimul_status);
}


BEGIN_MESSAGE_MAP(INPUT_SIMUL, CDialog)
	ON_BN_CLICKED(IDC_BUTT_SIMUL1, &INPUT_SIMUL::OnBnClickedButtSimul1)
	ON_BN_CLICKED(IDC_BUTT_SIMUL2, &INPUT_SIMUL::OnBnClickedButtSimul2)
	ON_BN_CLICKED(IDC_BUTT_SIMUL3, &INPUT_SIMUL::OnBnClickedButtSimul3)
	ON_BN_CLICKED(IDC_BUTT_SIMUL4, &INPUT_SIMUL::OnBnClickedButtSimul4)
	ON_BN_CLICKED(IDC_BUTT_SIMUL5, &INPUT_SIMUL::OnBnClickedButtSimul5)
	ON_BN_CLICKED(IDC_BUTT_SIMUL6, &INPUT_SIMUL::OnBnClickedButtSimul6)
	ON_BN_CLICKED(IDC_BUTT_SIMUL7, &INPUT_SIMUL::OnBnClickedButtSimul7)
	ON_BN_CLICKED(IDC_BUTT_SIMUL8, &INPUT_SIMUL::OnBnClickedButtSimul8)
	ON_BN_CLICKED(IDC_BUTT_SIMUL9, &INPUT_SIMUL::OnBnClickedButtSimul9)
	ON_BN_CLICKED(IDC_BUTT_SIMULF, &INPUT_SIMUL::OnBnClickedButtSimulf)
	ON_BN_CLICKED(IDC_BUTT_SIMUL0, &INPUT_SIMUL::OnBnClickedButtSimul0)
	ON_BN_CLICKED(IDC_BUTT_SIMULDOT, &INPUT_SIMUL::OnBnClickedButtSimuldot)
	ON_BN_CLICKED(IDC_BUTT_SIMULRED, &INPUT_SIMUL::OnBnClickedButtSimulred)
	ON_BN_CLICKED(IDC_BUTT_SIMULYELLOW, &INPUT_SIMUL::OnBnClickedButtSimulyellow)
	ON_BN_CLICKED(IDC_BUTT_SIMULGREEN, &INPUT_SIMUL::OnBnClickedButtSimulgreen)
	ON_BN_CLICKED(IDOK, &INPUT_SIMUL::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTT_INPUTSIMULSKHAUT, &INPUT_SIMUL::OnBnClickedButtInputsimulskhaut)
	ON_BN_CLICKED(IDC_BUTT_INPUTSIMULSKBAS, &INPUT_SIMUL::OnBnClickedButtInputsimulskbas)
	ON_BN_CLICKED(IDC_BUTT_INPUTSIMULSK2, &INPUT_SIMUL::OnBnClickedButtInputsimulsk2)
	ON_BN_CLICKED(IDC_BUTT_INPUTSIMULSK1, &INPUT_SIMUL::OnBnClickedButtInputsimulsk1)
	ON_BN_CLICKED(IDC_BUTT_INPUTSIMULSK3, &INPUT_SIMUL::OnBnClickedButtInputsimulsk3)
	ON_BN_CLICKED(IDC_BUTT_INPUTSIMULSK4, &INPUT_SIMUL::OnBnClickedButtInputsimulsk4)
	ON_BN_CLICKED(IDC_BUTT_INPUTSIMULSK1VAL, &INPUT_SIMUL::OnBnClickedButtInputsimulsk1val)
END_MESSAGE_MAP()


// Gestionnaires de messages de INPUT_SIMUL

BOOL INPUT_SIMUL::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: ajoutez ici une initialisation suppl�mentaire
	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}

void INPUT_SIMUL::OnBnClickedButtSimul1()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("1");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimul2()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("2");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimul3()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("3");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimul4()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("4");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimul5()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("5");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimul6()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("6");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimul7()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("7");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimul8()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("8");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimul9()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("9");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimulf()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("(");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimul0()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("0");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimuldot()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul(".");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimulred()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("\x17");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimulyellow()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("\x18");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtSimulgreen()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("\x16");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedOk()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	CDialog::OnOK();
}


void INPUT_SIMUL::OnBnClickedButtInputsimulskhaut()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("\x23");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}

void INPUT_SIMUL::OnBnClickedButtInputsimulskbas()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("\x24");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}

void INPUT_SIMUL::OnBnClickedButtInputsimulsk1()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("\x19");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}

void INPUT_SIMUL::OnBnClickedButtInputsimulsk2()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("\x20");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}

void INPUT_SIMUL::OnBnClickedButtInputsimulsk3()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("\x21");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}

void INPUT_SIMUL::OnBnClickedButtInputsimulsk4()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("\x22");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}


void INPUT_SIMUL::OnBnClickedButtInputsimulsk1val()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	BOOL bReturn;

	bReturn = inputSimul("\x16");
    if (bReturn == FALSE)
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul KO"));
	}
	else
	{
		var_inputSimul_status.SetWindowTextW(_T("Input Simul OK"));
	}
}
