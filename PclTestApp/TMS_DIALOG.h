#pragma once
#include "afxwin.h"


// Bo�te de dialogue TMS_DIALOG

class TMS_DIALOG : public CDialogEx
{
	DECLARE_DYNAMIC(TMS_DIALOG)

public:
	TMS_DIALOG(CWnd* pParent = NULL);   // constructeur standard
	virtual ~TMS_DIALOG();

	// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_TMS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedTmsQuit();
	afx_msg void OnBnClickedWriteTmsParam();
	afx_msg void OnBnClickedReadTmsParam();
	CEdit m_adress_edit;
	CEdit m_port_edit;
	CEdit m_identifier_edit;
	CEdit m_read_current_ssl_profile;
	CListBox m_list_read_ssl_profile;
	CStatic m_number_ssl;
	CStatic m_status;
};
