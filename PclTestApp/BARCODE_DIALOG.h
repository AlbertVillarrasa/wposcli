#pragma once
#include "afxwin.h"
#include <string> 
#include <iostream> 
#include <sstream> 
#include "afxcmn.h"

template<typename T,typename S> 
bool FromString( const S & Str, T & Dest ) 
{ 
#ifdef _UNICODE 
    std::wistringstream iss( Str ); 
#else 
    std::istringstream iss( Str ); 
#endif 
    // tenter la conversion vers Dest 
    return iss >> Dest != 0; 
}

// Bo�te de dialogue BARCODE_DIALOG

class BARCODE_DIALOG : public CDialogEx
{
	DECLARE_DYNAMIC(BARCODE_DIALOG)

public:
	BARCODE_DIALOG(CWnd* pParent = NULL);   // constructeur standard
	virtual ~BARCODE_DIALOG();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_BARCODE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	static void CALLBACK BarcodeEvent(char* data, int len);
	static void CALLBACK BarcodeEventExt(char* data, int len, char* symb, int len_symb);
	static void CALLBACK BarcodeEventClose();
	afx_msg void OnBnClickedOpenBcr();
	afx_msg void OnBnClickedCloseBcr();
	afx_msg void OnBnClickedQuit();
	afx_msg void OnBnClickedReadModeBcr();
	afx_msg void OnBnClickedGoodBeep();
	afx_msg void OnBnClickedTriggerBcr();
	afx_msg void OnBnClickedImagerModeBcr();
	afx_msg void OnBnClickedLightingGoalBcr();
	afx_msg void OnBnClickedLightingModeBcr();
	afx_msg void OnBnClickedIlluminationLevelBcr();
	afx_msg void OnBnClickedIlluminationModeBcr();
	afx_msg void OnBnClickedButtonResetSoftBcr();
	afx_msg void OnBnClickedGetFirmwareVersion();
	afx_msg void OnBnClickedEnableSymbologyBcr();
	afx_msg void OnBnClickedDisableSymbologyBcr();
	afx_msg void OnBnClickedStartScanBcr();
	afx_msg void OnBnClickedStopScanBcr();
	afx_msg void OnBnClickedBeepLengthFreqBcr();
	CStatic textStatus;
	CEdit m_edit_timeout;
	CRichEditCtrl m_comment;
	CListCtrl m_listSymbology;
	CComboBox m_combo_read_mode;
	CComboBox m_combo_illumination_mode;
	CComboBox m_combo_imager_mode;
	CComboBox m_combo_good_scan_beep;
	CComboBox m_combo_lighting_mode;
	CComboBox m_combo_trigger;	
	CEdit m_edit_length;
	CEdit m_edit_freq;
	
	afx_msg void OnBnClickedConvertSymbToString();
	CComboBox m_combo_ean8_as_ean13;
	CComboBox m_combo_upce_as_upca;
	CComboBox m_combo_upca_as_ean13;
	//afx_msg void OnBnClickedUpcaAsEan13();
	//afx_msg void OnBnClickedUpceAsUpca();
	//afx_msg void OnBnClickedEan8AsEan13();
	CEdit m_version_barcode;
	afx_msg void OnBnClickedVolatileParam();
	CButton m_volatile_parameters;
	afx_msg void OnBnClickedApplyVersion();
	afx_msg void OnBnClickedCheckVolatileParam();
};