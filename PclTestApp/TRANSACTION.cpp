// TRANSACTION.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "pdautil.h"
#include "PclTestApp.h"
#include "TRANSACTION.h"
#include "afxdialogex.h"

#define BUFFER_SIZE 16834
#define EXT_BUFFER_SIZE 8000

//GLobal Variables
BOOL breturn_Transaction;
transactionOut_t iTransOutStruct;
ITransactionOut* iTransOut = NULL;
unsigned char globBufferOut[64*1024];
unsigned long globBufferOutSize = 64*1024;

extern BOOL isTransactionEx;
extern BOOL isKeyboardPresent;

// Bo�te de dialogue TRANSACTION

IMPLEMENT_DYNAMIC(TRANSACTION, CDialog)

TRANSACTION::TRANSACTION(CWnd* pParent /*=NULL*/)
	: CDialog(TRANSACTION::IDD, pParent)
{

}

TRANSACTION::~TRANSACTION()
{
}

void TRANSACTION::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_TRANSTYPE, var_list_TransType);
	DDX_Control(pDX, IDC_LIST_AUTHTYPE, var_list_AuthType);
	DDX_Control(pDX, IDC_LIST_AUTHREQUIRED, var_list_AuthRequired);
	DDX_Control(pDX, IDC_TRANS_POSNB, var_pos_number);
	DDX_Control(pDX, IDC_TRANS_CURRENCY, var_currency_code);
	DDX_Control(pDX, IDC_TRANS_AMOUNT, var_amount);
	DDX_Control(pDX, IDC_TRANS_USERDATA, var_userData);
	DDX_Control(pDX, IDC_TRANS_EXTENDEDDATA, var_extended_data);
	DDX_Control(pDX, IDC_TRANS_APPLINB, var_appli_nb);
	DDX_Control(pDX, IDC_STATIC_EXTENDEDDATA, var_text_extended);
	DDX_Control(pDX, IDC_APPLINB, var_text_appliNb);
	DDX_Control(pDX, IDC_RADIO_EXDATA_FILE, var_radio_extented_data_file);
	DDX_Control(pDX, IDC_RADIO_EXDATA_TEXT, var_radio_extented_data_text);
}


BEGIN_MESSAGE_MAP(TRANSACTION, CDialog)
	ON_BN_CLICKED(IDC_BUTT_DOTRANSACTION, &TRANSACTION::OnBnClickedButtDotransaction)
	ON_BN_CLICKED(IDOK, &TRANSACTION::OnBnClickedOk)
	ON_EN_SETFOCUS(IDC_TRANS_POSNB, &TRANSACTION::OnEnSetfocusTransPosnb)
	ON_EN_SETFOCUS(IDC_TRANS_CURRENCY, &TRANSACTION::OnEnSetfocusTransCurrency)
	ON_BN_CLICKED(IDC_BUTT_DOTRANSACTION_STRUCT, &TRANSACTION::OnBnClickedButtDotransactionStruct)
END_MESSAGE_MAP()


BOOL TRANSACTION::OnInitDialog()
{
	BOOL ret = TRUE;

	CDialog::OnInitDialog();

	// TODO: ajoutez ici une initialisation suppl�mentaire

	var_pos_number.LimitText(2);
	var_amount.LimitText(12);
	var_userData.LimitText(10);

	if(isTransactionEx)
	{
		var_text_appliNb.ShowWindow(SW_SHOW);
		var_text_extended.ShowWindow(SW_SHOW);
		var_extended_data.ShowWindow(SW_SHOW);
		var_appli_nb.ShowWindow(SW_SHOW);
		var_radio_extented_data_text.ShowWindow(SW_SHOW);
		var_radio_extented_data_file.ShowWindow(SW_SHOW);
		var_radio_extented_data_text.SetCheck(1);
	}
	else
	{
		var_text_appliNb.ShowWindow(SW_HIDE);
		var_text_extended.ShowWindow(SW_HIDE);
		var_extended_data.ShowWindow(SW_HIDE);
		var_appli_nb.ShowWindow(SW_HIDE);
		var_radio_extented_data_text.ShowWindow(SW_HIDE);
		var_radio_extented_data_file.ShowWindow(SW_HIDE);
	}
	
	var_pos_number.SetWindowText(L"58");
	var_currency_code.SetWindowText(L"978");
	var_amount.SetWindowText(L"12345");
	var_userData.SetWindowText(L"abcdefg");
	
	var_list_TransType.AddString(_T("C Debit"));
	var_list_TransType.AddString(_T("D Credit"));
	var_list_TransType.AddString(_T("I ISO2"));
	var_list_TransType.AddString(_T("J Discard"));
	var_list_TransType.AddString(_T("K Duplicata"));
	var_list_TransType.SetCurSel(0);

	var_list_AuthType.AddString(_T("1"));
	var_list_AuthType.AddString(_T("2"));
	var_list_AuthType.SetCurSel(0);
	var_list_AuthRequired.AddString(_T("1"));
	var_list_AuthRequired.AddString(_T("2"));
	var_list_AuthRequired.SetCurSel(0);

	//  Disable redirection immediately prior to the native API
	//  function call.

    if ( !isKeyboardPresent )
	{
		TCHAR  infoBuf[1024];
		infoBuf[0]='\0';
		if ( SHGetSpecialFolderPath(0,infoBuf,CSIDL_PROGRAM_FILES,FALSE) )
		{
			infoBuf[2]='\0';
			PathAppend(infoBuf, TEXT("\\Program Files\\Common Files\\microsoft shared\\ink\\tabtip.exe"));
			ShellExecute(NULL, NULL,infoBuf, NULL, NULL, SW_SHOWNORMAL);
		}
	}
	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}


// Gestionnaires de messages de TRANSACTION


void TRANSACTION::OnBnClickedButtDotransaction()
{
	breturn_Transaction = FALSE;
	FILE * pFile;
	OPENFILENAME ofn;
	wchar_t fileName[MAX_PATH]=L"";
	
	ITransactionIn* iTransIn = create_itransactionin();
	if (iTransIn)
	{
		iTransOut = create_itransactionout();
		if (iTransOut)
		{
			char charBuff[BUFFER_SIZE];
			wchar_t buffer[BUFFER_SIZE];
			size_t i;

			var_amount.GetWindowText(buffer, BUFFER_SIZE);
			wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
			iTransIn->setAmount(charBuff);

			var_list_AuthType.GetWindowText(buffer, BUFFER_SIZE);
			wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
			iTransIn->setAuthorizationType(charBuff);
	
			var_list_AuthRequired.GetWindowText(buffer, BUFFER_SIZE);
			wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
			iTransIn->setCtrlCheque(charBuff);
	
			var_currency_code.GetWindowText(buffer, BUFFER_SIZE);
			wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
			iTransIn->setCurrencyCode(charBuff);
	
			var_list_TransType.GetWindowText(buffer, BUFFER_SIZE);
			wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
			charBuff[1] = 0; //fin de chaine
			iTransIn->setOperation(charBuff);
	
			var_pos_number.GetWindowText(buffer, BUFFER_SIZE);
			wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
			iTransIn->setTermNum(charBuff);

			var_userData.GetWindowText(buffer, BUFFER_SIZE);
			wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
			iTransIn->setUserData1(charBuff);

	
			if (isTransactionEx)
			{
				unsigned int appNb;
				unsigned char extendedDataReadInFile[BUFFER_SIZE];

				var_appli_nb.GetWindowText(buffer, BUFFER_SIZE);
				wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
				sscanf_s(charBuff, "%u", &appNb);
				unsigned long sizeOfTextFile  = 1;
				unsigned long sizeRead;

				if(var_radio_extented_data_text.GetCheck())
				{
					var_extended_data.GetWindowText(buffer, BUFFER_SIZE);
					wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
					breturn_Transaction = doTransactionEx(iTransIn, iTransOut, appNb, (unsigned char*) charBuff, i, (unsigned char *) globBufferOut, &globBufferOutSize);
				}
				else
				{
					ZeroMemory(&ofn, sizeof(ofn));
					ofn.lStructSize = sizeof(OPENFILENAME);
					ofn.hwndOwner = NULL;
					ofn.lpstrFilter = L"Param Files (*.*)\0*.*\0";
					ofn.lpstrFile = fileName;
					ofn.nMaxFile = MAX_PATH;
					ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
					ofn.lpstrDefExt;
	
					if ( GetOpenFileName(&ofn) )
					{
						//Read of the file to extract the extanded data
						errno_t error = _wfopen_s(&pFile, fileName,L"r");
						// obtain file size:
						//fseek (pFile , 0 , SEEK_END);
						//lSize = ftell (pFile);
						//fseek (pFile , 0 , SEEK_SET);

						// allocate memory to contain the whole file:
						//extendedDataReadInFile = (char*) malloc (lSize * sizeof(char));

						// copy the file into the buffer:
						sizeRead = fread (extendedDataReadInFile,1,BUFFER_SIZE,pFile);
						// terminate
						fclose(pFile);
					}
					breturn_Transaction = doTransactionEx(iTransIn, iTransOut, appNb, (unsigned char*) extendedDataReadInFile, sizeRead, (unsigned char *) globBufferOut, &globBufferOutSize);
					//free(extendedDataReadInFile);
				}
			}
	
			else
			{
				breturn_Transaction = doTransaction(iTransIn, iTransOut);
			}
		}

		delete iTransIn;
	}

	if ( !isKeyboardPresent )
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	CDialog::OnOK();
}


void TRANSACTION::OnBnClickedOk()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
    if ( !isKeyboardPresent )
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	CDialog::OnOK();
}


void TRANSACTION::OnEnSetfocusTransPosnb()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
}


void TRANSACTION::OnEnSetfocusTransCurrency()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
}


void TRANSACTION::OnBnClickedButtDotransactionStruct()
{
breturn_Transaction = FALSE;
	FILE * pFile;
	OPENFILENAME ofn;
	transactionIn_t iTransIn;
	
	wchar_t fileName[MAX_PATH]=L"";

	char charBuff[BUFFER_SIZE];
	wchar_t buffer[BUFFER_SIZE];
	size_t i;

	var_amount.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	memcpy_s(iTransIn.amount,sizeof(iTransIn.amount),charBuff,sizeof(iTransIn.amount));

	var_list_AuthType.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	memcpy_s(iTransIn.authorizationType,sizeof(iTransIn.authorizationType),charBuff,sizeof(iTransIn.authorizationType));

	var_list_AuthRequired.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	memcpy_s(iTransIn.ctrlCheque,sizeof(iTransIn.ctrlCheque),charBuff,sizeof(iTransIn.ctrlCheque));

	var_currency_code.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	memcpy_s(iTransIn.currencyCode,sizeof(iTransIn.currencyCode),charBuff,sizeof(iTransIn.currencyCode));

	var_list_TransType.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	charBuff[1] = 0; //fin de chaine
	memcpy_s(iTransIn.operation,sizeof(iTransIn.operation),charBuff,sizeof(iTransIn.operation));

	var_pos_number.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	memcpy_s(iTransIn.terminalNumber,sizeof(iTransIn.terminalNumber),charBuff,sizeof(iTransIn.terminalNumber));

	var_userData.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	memcpy_s(iTransIn.userData1,sizeof(iTransIn.userData1),charBuff,sizeof(iTransIn.userData1));

	if (isTransactionEx)
	{
		unsigned int appNb;
		unsigned char extendedDataReadInFile[BUFFER_SIZE];

		var_appli_nb.GetWindowText(buffer, BUFFER_SIZE);
		wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
		sscanf_s(charBuff, "%u", &appNb);
		unsigned long sizeOfTextFile  = 1;
		unsigned long sizeRead;

		if(var_radio_extented_data_text.GetCheck())
		{
			var_extended_data.GetWindowText(buffer, BUFFER_SIZE);
			wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
			breturn_Transaction = doTransactionStructEx(&iTransIn, &iTransOutStruct, appNb, (unsigned char*) charBuff, i, (unsigned char *) globBufferOut, &globBufferOutSize);
		}
		else
		{
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(OPENFILENAME);
			ofn.hwndOwner = NULL;
			ofn.lpstrFilter = L"Param Files (*.*)\0*.*\0";
			ofn.lpstrFile = fileName;
			ofn.nMaxFile = MAX_PATH;
			ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
			ofn.lpstrDefExt;

			if ( GetOpenFileName(&ofn) )
			{
				//Read of the file to extract the extanded data
				errno_t error = _wfopen_s(&pFile, fileName,L"r");
				// obtain file size:
				//fseek (pFile , 0 , SEEK_END);
				//lSize = ftell (pFile);
				//fseek (pFile , 0 , SEEK_SET);

				// allocate memory to contain the whole file:
				//extendedDataReadInFile = (char*) malloc (lSize * sizeof(char));

				// copy the file into the buffer:
				sizeRead = fread (extendedDataReadInFile,1,BUFFER_SIZE,pFile);
				// terminate
				fclose(pFile);
			}
			breturn_Transaction = doTransactionStructEx(&iTransIn, &iTransOutStruct, appNb, (unsigned char*) extendedDataReadInFile, sizeRead, (unsigned char *) globBufferOut, &globBufferOutSize);
			//free(extendedDataReadInFile);
		}
	}
	else
	{
		breturn_Transaction = doTransactionStruct(&iTransIn, &iTransOutStruct);
	}


	if ( !isKeyboardPresent )
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	CDialog::OnOK();
}
