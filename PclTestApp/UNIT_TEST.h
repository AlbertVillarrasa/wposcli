#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// Bo�te de dialogue UNIT_TEST

class UNIT_TEST : public CDialog
{
	DECLARE_DYNAMIC(UNIT_TEST)

public:
	UNIT_TEST(CWnd* pParent = NULL);   // constructeur standard
	virtual ~UNIT_TEST();

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_UNIT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Prise en charge de DDX/DDV
	virtual BOOL OnInitDialog();
	LRESULT OnPowerMsgRcvd(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtGoback();
	afx_msg void OnBnClickedButtStart();
	afx_msg void OnBnClickedButtStop();
	afx_msg void OnBnClickedButtCompinfo();
	CStatic textStatus;
	DWORD dwPCLService;
	afx_msg void OnBnClickedButtSettime();
	afx_msg void OnBnClickedButtGettime();
	afx_msg void OnBnClickedButtGetinfos();
	afx_msg void OnBnClickedButtPrinttext();
	afx_msg void OnBnClickedButtUpdate();
	afx_msg void OnBnClickedButtPrintbitmap();
	afx_msg void OnBnClickedButtReset();
	afx_msg void OnBnClickedButtSendmsg();
	afx_msg void OnBnClickedButtReceivemsg();
	afx_msg void OnBnClickedButtFlushmsg();
	afx_msg void OnBnClickedButtShortcut();
	afx_msg void OnBnClickedButtInputsimul();
	afx_msg void OnBnClickedButtDotransaction();
	afx_msg void OnBnClickedButtDotransactionex();
	afx_msg void OnBnClickedButtStorelogo();
	afx_msg void OnBnClickedButtPrintlogo();
	afx_msg void OnBnClickedButtPrinterstatus();
	afx_msg void OnBnClickedButtNetwork();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	CStatic m_staticConnState;
	HANDLE m_hThread;
	afx_msg void OnDestroy();
	//static void CALLBACK BarcodeEvent(char* data, int len);
	//static void CALLBACK BarcodeEventClose();
	static void CALLBACK PrintText(char* szText, unsigned char font, unsigned char justification, unsigned char xFactor, unsigned char yFactor, unsigned char underline, unsigned char bold, unsigned char charset);
	static void CALLBACK PrintImage(unsigned long width, unsigned long height, char* image, unsigned long size, unsigned char justification);
	static void CALLBACK FeedPaper(DWORD dwLines);
	static void CALLBACK CutPaper();
	static int CALLBACK StartReceipt(unsigned char type);
	static int CALLBACK EndReceipt();
	static void CALLBACK AddSignature();
	CRichEditCtrl textComments;
	afx_msg void OnBnClickedButtGetfullsn();
	afx_msg void OnBnClickedCheckLogenabled();
	CButton checkLogEnabled;
	afx_msg void OnClickedButtGetSpmciVersion();
	afx_msg void OnBnClickedButtBcr();
	afx_msg void OnBnClickedInitTmsParam();
#ifdef ENABLE_EPAS
	afx_msg void OnBnClickedButtEpaslogin();
	afx_msg void OnBnClickedButtEpaspayment();
	afx_msg void OnBnClickedButtEpaslogout();
	afx_msg void OnBnClickedButtEpasdisconnect();
	afx_msg void OnBnClickedButtEpasconnect();
#endif
	afx_msg void OnBnClickedButtBacklight();
	afx_msg void OnBnClickedButtPrinterfont();
	HANDLE m_hUpdateThread;

	afx_msg void OnBnClickedButtGetbatlevel();
	afx_msg void OnBnClickedOpenCashDrawerBtn();
};
