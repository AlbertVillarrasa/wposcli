// TMS_DIALOG.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "PclTestApp.h"
#include "TMS_DIALOG.h"
#include "afxdialogex.h"
#include "pdautil.h"

#define IP_SIZE_BUFF	300
#define OTHER_SIZE_BUFF	20

// Bo�te de dialogue TMS_DIALOG

IMPLEMENT_DYNAMIC(TMS_DIALOG, CDialogEx)

TMS_DIALOG::TMS_DIALOG(CWnd* pParent /*=NULL*/)
	: CDialogEx(TMS_DIALOG::IDD, pParent)
{

}

TMS_DIALOG::~TMS_DIALOG()
{
}

void TMS_DIALOG::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ADRESS_TMS, m_adress_edit);
	DDX_Control(pDX, IDC_PORT_TMS, m_port_edit);
	DDX_Control(pDX, IDC_IDENTIFIER_TMS, m_identifier_edit);
	DDX_Control(pDX, IDC_READ_CURRENT_SSL, m_read_current_ssl_profile);
	DDX_Control(pDX, IDC_LIST_SSL_PROFILES, m_list_read_ssl_profile);
	DDX_Control(pDX, IDC_LIST_SSL_NUMBER, m_number_ssl);
	DDX_Control(pDX, IDC_STATUS_TMS, m_status);
}


BEGIN_MESSAGE_MAP(TMS_DIALOG, CDialogEx)
	ON_BN_CLICKED(ID_TMS_QUIT, &TMS_DIALOG::OnBnClickedTmsQuit)
	ON_BN_CLICKED(IDC_WRITE_TMS_PARAM, &TMS_DIALOG::OnBnClickedWriteTmsParam)
	ON_BN_CLICKED(IDC_READ_TMS_PARAM, &TMS_DIALOG::OnBnClickedReadTmsParam)
END_MESSAGE_MAP()


// Gestionnaires de messages de TMS_DIALOG
BOOL TMS_DIALOG::OnInitDialog()
{
	CDialog::OnInitDialog();
	OnBnClickedReadTmsParam();	
	return TRUE;
}

void TMS_DIALOG::OnBnClickedTmsQuit()
{
	EndDialog(0);
}


void TMS_DIALOG::OnBnClickedWriteTmsParam()
{
	char result;
	BOOL bReturn;

	wchar_t ipW[IP_SIZE_BUFF]= {0};
	char ip[IP_SIZE_BUFF]= {0};
	wchar_t portW[OTHER_SIZE_BUFF]= {0};
	char port[OTHER_SIZE_BUFF]= {0};
	wchar_t identifierW[OTHER_SIZE_BUFF]= {0};
	char identifier[OTHER_SIZE_BUFF]= {0};
	wchar_t current_sslW[OTHER_SIZE_BUFF]= {0};
	char current_ssl[OTHER_SIZE_BUFF]= {0};
	size_t s;

	//adress ip or domain name
	m_adress_edit.GetWindowText(ipW,IP_SIZE_BUFF);
	wcstombs_s(&s, ip, ipW, IP_SIZE_BUFF);

	//port
	m_port_edit.GetWindowText(portW,OTHER_SIZE_BUFF);
	wcstombs_s(&s,port, portW, OTHER_SIZE_BUFF);

	//identifier
	m_identifier_edit.GetWindowText(identifierW,OTHER_SIZE_BUFF);
	wcstombs_s(&s,identifier, identifierW, OTHER_SIZE_BUFF);

	m_status.SetWindowText(_T(""));

	//ssl
	int curSel = m_list_read_ssl_profile.GetCurSel();
	if(curSel == LB_ERR)
	{
		m_status.SetWindowText(_T("Error: Select one SSL profile in list!"));
	}
	else if(curSel == 0)
	{
		//cas no SSL current_ssl[0] = '0' 
		bReturn = tmsWriteParam(ip, port, identifier, current_ssl, &result);
	}
	else if(curSel == 1)
	{
		//cas no change
		bReturn = tmsWriteParam(ip, port, identifier, NULL, &result);
	}
	else
	{
		CString str;
		m_list_read_ssl_profile.GetText(curSel, str);

		bReturn = tmsWriteParam(ip, port, identifier,CT2A((LPCTSTR)str), &result);
	}
		
	if (bReturn == FALSE)
	{
		m_status.SetWindowText(_T("Write param TMS KO"));
	}
	else
	{
		if(result == 0)
			m_status.SetWindowText(_T("Write param TMS OK"));
		else
			m_status.SetWindowText(_T("Write param TMS KO"));
	}
}


void TMS_DIALOG::OnBnClickedReadTmsParam()
{
	char result;
	BOOL bReturn;
	tms_ssl_parameter_t ssl_param;
	char ip[IP_SIZE_BUFF]= {0};
	char port[OTHER_SIZE_BUFF]= {0};
	char identifier[OTHER_SIZE_BUFF]= {0};
	char msgNbSSL[50] = {0};
	wchar_t tmp_sslW[12]= {0};
	size_t s;
	int selectedIndex = 0;

	m_status.SetWindowText(_T(""));

	bReturn = tmsReadParam(ip, sizeof(ip), port, sizeof(port), identifier, sizeof(identifier), &ssl_param, &result);
	
	if (bReturn == FALSE)
	{
		m_status.SetWindowTextW(_T("Read param TMS KO"));
	}
	else
	{
		if(result == 0)
		{
			m_status.SetWindowText(_T("Read param TMS OK"));

			m_adress_edit.SetWindowText(CA2W(ip));
			m_port_edit.SetWindowText(CA2W(port));		
			m_identifier_edit.SetWindowText(CA2W(identifier));
			//ssl
			m_read_current_ssl_profile.SetWindowText(CA2W((char*)ssl_param.current_ssl_profile));

			sprintf_s(msgNbSSL,50,"Profile list with %d items:",ssl_param.nb_ssl_profile);
			m_number_ssl.SetWindowText(CA2W(msgNbSSL));

			//delete all profile in list
			m_list_read_ssl_profile.ResetContent();

			m_list_read_ssl_profile.AddString(L"No SSL");
			m_list_read_ssl_profile.AddString(L"No Change");

			//insert new profile
			for(unsigned int i=0;i<ssl_param.nb_ssl_profile ;++i)
			{
				mbstowcs_s(&s,tmp_sslW,(char*)ssl_param.ssl_profiles[i],12);
				m_list_read_ssl_profile.AddString(tmp_sslW);
				if(strncmp((char*)ssl_param.current_ssl_profile,(char*)ssl_param.ssl_profiles[i],12) == 0)
				{
					selectedIndex = i +2;
				}
			}
			//selectionne l'index dans la liste
			m_list_read_ssl_profile.SetCurSel(selectedIndex);
		}
		else
			m_status.SetWindowText(_T("Read param TMS KO"));
	}
}
