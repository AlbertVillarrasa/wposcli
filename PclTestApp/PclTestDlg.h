
// PclTestDlg.h : fichier d'en-t�te
//

#pragma once


// bo�te de dialogue CPclTestDlg
class CPclTestDlg : public CDialogEx
{
// Construction
public:
	CPclTestDlg(CWnd* pParent = NULL);	// constructeur standard

// Donn�es de bo�te de dialogue
	enum { IDD = IDD_PCLTESTAPP_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Prise en charge de DDX/DDV


// Impl�mentation
protected:
	HICON m_hIcon;

	// Fonctions g�n�r�es de la table des messages
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
private:
	
public:

	afx_msg void OnBnClickedButtonUnitTests();
	afx_msg void OnBnClickedButtonLoopTests();
};
