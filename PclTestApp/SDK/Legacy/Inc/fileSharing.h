#ifndef __FILESHARING_PUBLIC_H__
#define __FILESHARING_PUBLIC_H__

/**
 * \file fileSharing.h
 * \brief Interface functions FileSharing of PCLService.dll. 
 *  Working only on Tetra Terminal.
 */

#ifdef __cplusplus
extern "C"{
#endif

#ifdef PCLSERVICE_EXPORTS
	#define FILESHARING_API __declspec(dllexport)
#else 
	#define FILESHARING_API __declspec(dllimport)
#endif

#define FILESHARING_OK                            (     0) //!< Success.
#define FILESHARING_ERROR_INTERNAL_RESOURCE       (-10000) //!< Error while allocating resources.
#define FILESHARING_ERROR_INVALID_PARAMETER       (-10001) //!< Invalid input parameter.
#define FILESHARING_ERROR_INVALID_HANDLE          (-10002) //!< Invalid handle.
#define FILESHARING_ERROR_INVALID_RIGHTS          (-10003) //!< Invalid access rights.
#define FILESHARING_ERROR_ABORTED                 (-10004) //!< Operation was aborted.
#define FILESHARING_ERROR_COMMUNICATION           (-10005) //!< Communication error.
#define FILESHARING_ERROR_TIMEOUT                 (-10006) //!< Timeout error.
#define FILESHARING_ERROR_PROTOCOL                (-10007) //!< Protocol error.
#define FILESHARING_ERROR_INTERRUPTED             (-10008) //!< Operation was interrupted.
#define FILESHARING_ERROR_COMPRESSION             (-10009) //!< Error with compression/decompression.
#define FILESHARING_ERROR_INVALID_PACKAGE         (-10010) //!< Installation error due to an invalid package.
#define FILESHARING_ERROR_IDENTIFICATION          (-10011) //!< Identification error.
#define FILESHARING_ERROR_INJECTION               (-10012) //!< Injection error.
#define FILESHARING_ERROR_NOTHING_TO_INSTALL      (-10013) //!< No file to install.
#define FILESHARING_ERROR_INSTALLATION            (-10014) //!< Installation failure.
#define FILESHARING_ERROR_INVALID_PACKAGE_LIST    (-10015) //!< Error while checking the package list.
#define FILESHARING_ERROR_INVALID_CALL            (-10016) //!< Call not possible.
#define FILESHARING_ERROR_CONNECTION              (-10017) //!< Connection error.
#define FILESHARING_POSTPONED_JOB                 (-10018) //!< Success, installation postponed.
#define FILESHARING_ERROR_SERVER                  (-10019) //!< Error returned by the server.
#define FILESHARING_ERROR_FILE_SYSTEM             (-10020) //!< File system error.
#define FILESHARING_ERROR_ALREADY_CMD_IN_USE      (-10021) //!< File sharing command already in progress

/**
* The telium_file_t structure is a representation of a file get from a Telium device.
*/
typedef struct 
{
	char name[32]; //!< The name of the file.
	char path[512]; //!< The path of the file on Telium.
	int size; //!< The size of the file.
	BOOL isDirectory; //!< The file is a directory.
}telium_file_t;


/** This enum lists return of connection state of file sharing server. */
typedef enum
{
	/// Server disconnected
	FILE_SHARING_STATE_DISCONNECTED,
	/// Server connecting
	FILE_SHARING_STATE_CONNECTING,
	/// Server connected
	FILE_SHARING_STATE_CONNECTED,
	/// Server disconnecting
	FILE_SHARING_STATE_DISCONNECTING
}T_FILE_SHARING_STATE;

/**
* Called when the start task of PCLFileSharing is done.
*
* @param result Status of the operation (0=OK, else=ERROR)
*/
typedef int (CALLBACK *PCLFileSharingOnStart)(int result);
/**
* Called when the stop task of PCLFileSharing is done.
*
* @param result Status of the operation (0=OK, else=ERROR)
*/
typedef int (CALLBACK *PCLFileSharingOnStop)(int result);
/**
* Called when the upload task of PCLFileSharing is done.
*
* @param result Status of the operation (0=OK, else=ERROR)
*/
typedef int (CALLBACK *PCLFileSharingOnUpload)(int result);
/**
* Called when the upload multiple files task of PCLFileSharing has finished to send a file or when its completely done.
*
* @param current The current file sent.
* @param total The total of files to send.
* @param result Status of the operation (0=OK, else=ERROR)
*/
typedef int (CALLBACK *PCLFileSharingOnUploads)(int current, int total, int result);
/**
* Called when the list task of PCLFileSharing is done.
*
* @param files A telium_file_t array of the file in the directory requested from Telium.
* @param number The length of array.
* @param result Status of the operation (0=OK, else=ERROR)
*/
typedef int (CALLBACK *PCLFileSharingOnList)(telium_file_t** files, int number, int result);	        
/**
* Called when the start task of PCLFileSharing is done.
*
* @param result Status of the operation (0=OK, else=ERROR)
*/
typedef int (CALLBACK *PCLFileSharingOnDownload)(int result);


/**
* Start the PCLFileSharing server. It will be executed on an AsyncTask and call the callback with a PCLFileSharingResult param.
*
* @param port The mPort where open the server.
* @param isNotNetwork Specify is the terminal which you want connect is connected on a local network. If true, it means that the terminal is in Bluetooth or USB. If false, it means that the terminal is on a local network.
* @param callback The PCLFileSharingOnStart callback to be notify when the task is done and get the result.
*/
FILESHARING_API void PCLFileSharing_start(int port, BOOL isNotNetwork, PCLFileSharingOnStart callback);


/**
* Start the PCLFileSharing server and launch the remote upgrade of the connected terminal. It will be executed on an AsyncTask and call the callback with a PCLFileSharingResult param.
* The method will take care of saving the TMS settings and start the remove upgrade of the connected terminal.
*
* @param port The mPort where open the server.
* @param callback The PCLFileSharingOnStart callback to be notify when the task is done and get the result.
*/
FILESHARING_API	void PCLFileSharing_startAndLaunchDoUpdate(int port, PCLFileSharingOnStart callback);

/**
* Stop the PCLFileSharing server.
*
* @param callback The PCLFileSharingOnStop callback to be notify when the task is done and get the result.
*/
FILESHARING_API void PCLFileSharing_stop(PCLFileSharingOnStop callback);

/**
* Upload a file from Android to Telium.
*
* @param filepath A path of file that you want to send.
* @param toDirectory The directory path where send the file.
* @param callback The PCLFileSharingOnUploads callback to be notify when a file has been send and get the result for this file.
*/
FILESHARING_API void PCLFileSharing_upload(const char* filepath, const char* toDirectory, PCLFileSharingOnUpload callback);

/**
* Upload a list of files from Android to Telium.
*
* @param files A list of path files that you want to send.
* @param numberFiles number of path files in the list.
* @param toDirectory The directory path where send the file.
* @param callback The PCLFileSharingOnUploads callback to be notify when a file has been send and get the result for this file.
*/
FILESHARING_API void PCLFileSharing_uploadFilesList(char** files, int numberFiles, const char* toDirectory, PCLFileSharingOnUploads callback);

/**
* List the files from Telium.
*
* @param path The directory path that you want to list.
* @param callback The PCLFileSharingOnList callback to be notify when the task is done and get an array of TeliumFile object that describe the files in the requested directory.
*/
FILESHARING_API void PCLFileSharing_list(const char* path, PCLFileSharingOnList callback);

/**
* Download a file from Telium to Android.
*
* @param filepath The path of the file that you want download.
* @param toDirectory The path of the directory where save the file.
* @param callback The PCLFileSharingOnDownload callback to be notify when the task is done and get the result.
*/
FILESHARING_API void PCLFileSharing_download(const char* filepath, const char* toDirectory, PCLFileSharingOnDownload callback);

/**
* This function get if file sharing server is started.
*
* @return boolean true if the server is started
*                 false if the server is not started
*/
FILESHARING_API BOOL PCLFileSharing_isStarted();

/**
* This function get the state of connection of file sharing server. 
*
* @return enum of state file sharing
*/
FILESHARING_API T_FILE_SHARING_STATE PCLFileSharing_currentState();

#ifdef __cplusplus
}
#endif

#endif // ifndef __FILESHARING_FILESHARING_API_H__