#ifndef __PDA_UTIL_H__
#define __PDA_UTIL_H__

/**
 * \file pdautil.h
 * \brief Interface functions of PCLService.dll
 */

#ifdef PCLSERVICE_EXPORTS
#define PCL_SERVICE_API __declspec(dllexport)
#else
#define PCL_SERVICE_API __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C"{
#endif

//#if _WIN32_WINNT >= _WIN32_WINNT_WIN7
//#include "minwinbase.h"
//#include "winerror.h"
//#else
#include <windows.h>
#include "winbase.h"
//#endif

/// PCL service error codes
#define ERROR_PCL_SERVICE_BASE					60000
#define ERROR_PCL_SERVICE_NOT_STARTED			(ERROR_PCL_SERVICE_BASE+0)
#define ERROR_PCL_SERVICE_INIT					(ERROR_PCL_SERVICE_BASE+1)
#define ERROR_PCL_SERVICE_WSA_STARTUP			(ERROR_PCL_SERVICE_BASE+2)
#define ERROR_PCL_SERVICE_NOT_CONNECTED			(ERROR_PCL_SERVICE_BASE+3)
#define ERROR_PCL_SERVICE_ALREADY_STARTED		(ERROR_PCL_SERVICE_BASE+4)
#define ERROR_PCL_SERVICE_NULL_MANAGER			(ERROR_PCL_SERVICE_BASE+5)

/** Interface ITransactionIn */
class ITransactionIn
{
	public:		
		/** Set transaction amount.      
		@param amount  Amount in the smaller currency used for the transaction, only 8 digits are used
		@return true if the method succeeded, false if the method failed. */
		virtual bool setAmount(char* amount) = 0;
		/** Get amount used for transaction.
		@return amount in the smaller currency used for the transaction, limited to 8 digits. */
		virtual char* getAmount() = 0;
		/** Set terminal number.      
		@param termNum  Terminal number, only 2 digits are used
		@return true if the method succeeded, false if the method failed. */
		virtual bool setTermNum(char* termNum) = 0;
		/** Get terminal number.
		@return Terminal number, only 2 digits are used*/
		virtual char* getTermNum() = 0;
		/** Set transaction currency code.      
		@param currency  Currency code for the transaction (see <a href="http://www.iso.org/iso/home/standards/currency_codes.htm">ISO 4217</a>)
		@return true if the method succeeded, false if the method failed. */
		virtual bool setCurrencyCode(char* currencyCode) = 0;
		/** Get currency code used for transaction.     
		@return currency code for the transaction (See <a href="http://www.iso.org/iso/home/standards/currency_codes.htm">ISO 4217</a>) */
		virtual char* getCurrencyCode() = 0;
		/** Set transaction operation. 
		@param operation  Request type for the transaction 
		@return true if the method succeeded, false if the method failed. */
		virtual bool setOperation(char* operation) = 0;
		/** Get transaction operation.
		@return Request type for the transaction */
		virtual char* getOperation() = 0;
		/** Specify whether the payment application must call the
			authorization center:
			- �1� to force the payment application to call the authorization center 
			- �2� to let the payment application decide whether to call the authorization center or not
		@param type  Authorization type for the transaction
		@return true if the method succeeded, false if the method failed. */
		virtual bool setAuthorizationType(char *type) = 0;
		/** Get Authorization type.
		@return Authorization type for the transaction*/
		virtual char* getAuthorizationType() = 0;
		/** Set transaction check control.      
		 @param ctrlCheque  Check control for the transaction
		 @return true if the method succeeded, false if the method failed.  */
		virtual bool setCtrlCheque(char* ctrlCheque) = 0;
		/** Get transaction check control.
		@return  Check control for the transaction*/
		virtual char* getCtrlCheque() = 0;
		/** Set transaction user data.      
		@param data  User data, only 10 characters are used
		@return true if the method succeeded, false if the method failed.   */
		virtual bool setUserData1(char* data) = 0;
		/** Get transaction user data.
		@return User data*/
		virtual char* getUserData1() = 0;
};

/** Interface ITransactionOut*/
class ITransactionOut
{
	public:		
		/** Get amount used for transaction.
		@return amount in the smaller currency used for the transaction, limited to 8 digits. */
		virtual char* getAmount() = 0;
		/** Get error code for transaction.
		@return error code.
		@see ErrorCode */
		virtual char* getC3Error() = 0;
		/** Get currency code used for transaction.     
		@return currency code for the transaction (See <a href="http://www.iso.org/iso/home/standards/currency_codes.htm">ISO 4217</a>) */
		virtual char* getCurrencyCode() = 0;
		/** Get terminal number.
		@return Terminal number, limited to 2 digits. */
		virtual char* getTerminalNumber() = 0;
		/** Get user data.
		@return User data, only 10 bytes should be meaningful. */
		virtual char* getUserData1() = 0;
		/** Get private data.
		@return Private data*/
		virtual char* getPrivateData() = 0;
};

/** Structure transactionIn_t */
typedef struct 
{	
		/** Amount in the smaller currency used for the transaction, limited to 8 digits. */
		char amount[12];
		/** Get terminal number. only 2 digits are used*/
		char terminalNumber[8];
		/** Currency code used for transaction. (See <a href="http://www.iso.org/iso/home/standards/currency_codes.htm">ISO 4217</a>) */
		char currencyCode[3];
		/** Request type for the transaction */ 
		char operation[1];
		/** Authorization type for the transaction */
		char authorizationType[1];
		/** Transaction check control.*/
		char ctrlCheque[1];
		/** Transaction user data.*/
		char userData1[32];
} transactionIn_t;

/** Structure transactionOut_t*/
typedef struct 
{	
		/** Amount used for transaction.*/
		char amount[13];
		/** Error code for transaction.*/
		char c3Error[5];
		/** Currency code used for transaction. (See <a href="http://www.iso.org/iso/home/standards/currency_codes.htm">ISO 4217</a>) */
		char currencyCode[4];
		/** Terminal number. */
		char terminalNumber[9];
		/** User data. */
		char userData1[33];
		/** Private data.*/
		char privateData[74];
} transactionOut_t;

/// Map old API name to new API name
#define apiStartService startPclService
#define apiStopService  stopPclService
#define apiDoTransaction doTransaction
#define apiDoTransactionEx doTransactionEx
#define apiSendMessage sendMessage
#define apiReceiveMessage receiveMessage
#define apiFlushReceivedMessages flushMessages
#define apiLaunchM2OSShortcut launchM2OSShortcut
#define apiInputSim inputSimul
#define apiResetTerminal resetTerminal
#define apiGetInfo getTerminalInfo
#define apiGetFullSerialNumber getFullSerialNumber
#define apiGetSpmciVersion getSPMCIVersion
#define apiSetTeliumTime setTerminalTime
#define apiGetTeliumTime getTerminalTime
#define apiDoUpdate doUpdate
#define apiGetComponentsInfo getTerminalComponents
#define apiOpenPrinter openPrinter
#define apiClosePrinter closePrinter
#define apiGetRemotePrinterStatus getPrinterStatus
#define apiStoreLogo storeLogo
#define apiPrintText printText
#define apiPrintBitmap printBitmap
#define apiPrintLogo printLogo
#define apiSetPrinterFont setPrinterFont
#define apiPPPServerStatus serverStatus
#define apiOpenBarcode openBarcode
#define apiOpenBarcodeWithInactivityTo openBarcodeWithInactivityTo
#define apiCloseBarcode closeBarcode
#define apiStartScan bcrStartScan
#define apiStopScan bcrStopScan
#define apiConfigureBarCodeReaderMode bcrSetReaderMode
#define apiGoodScanBeep bcrSetGoodScanBeep
#define apiEnableTrigger bcrEnableTrigger
#define apiConfigureImagerMode bcrSetImagerMode
#define apiSetBeep bcrSetBeep
#define apiLightingMode bcrSetLightingMode
#define apiBRCSoftReset bcrSoftReset
#define apiEnableSymbologies bcrEnableSymbologies
#define apiDisableSymbologies bcrDisableSymbologies
#define apiSymbologyToText bcrSymbologyToText
#define apiCodeUDSIToSymbology bcrCodeUDSIToSymbology
#define apiGetBCRFirmwareVersion bcrGetFirmwareVersion
#define apiSetBCRNonVolatileMode bcrSetNonVolatileMode
#define apiSetBCRSettingsVersion bcrSetSettingsVersion
#define apiGetBCRSettingsVersion bcrGetSettingsVersion
#define apiReadTMSParam tmsReadParam
#define apiWriteTMSParam tmsWriteParam
#define apiSetBacklightLock setBacklightLock
#define apiAddDynamicBridge addDynamicBridge
#define apiAddDynamicBridgeLocal addDynamicBridgeLocal
#define apiRegisterBarcodeEventCallback registerBarcodeEventCallback
#define apiRegisterBarcodeEventExtCallback registerBarcodeEventExtCallback
#define apiRegisterSignCapCallback registerSignCapCallback
#define apiRegisterCallbacks registerCallbacks
#define apiEnableDebugLog enableDebugLog
#define apiIsDebugLogEnabled isDebugLogEnabled


/** Initiate the connection between the Windows device and the companion.
@return TRUE in case of success, FALSE in case of failure */
PCL_SERVICE_API BOOL startPclService();

/** Close the connection between the Windows device and the companion.
@return TRUE in case of success, FALSE in case of failure */
PCL_SERVICE_API BOOL stopPclService();

/** Create a instance of ITransactionIn 
@return Pointer to a ITransactionIn, you need delete this pointer after use*/
PCL_SERVICE_API ITransactionIn* __cdecl create_itransactionin();

/** Create a instance of ITransactionOut 
@return Pointer to a ITransactionIn, you need delete this pointer after use*/
PCL_SERVICE_API ITransactionOut* __cdecl create_itransactionout();

/** Launches a transaction request, passing the input parameters to the companion payment application.
The output parameters are filled with the response from the companion payment application.
@param transactionReq Transaction request parameters
@param transactionResp Transaction response parameters. Call getC3Error to check transaction result.
@return TRUE in case of success, FALSE in case of failure */
PCL_SERVICE_API BOOL doTransaction(ITransactionIn* transactionReq, ITransactionOut* transactionResp);

/** Launches a transaction request with extended parameters, passing the input parameters to the companion payment application.
The output parameters are filled with the response from the companion payment application.
@param transactionReq Transaction request parameters
@param transactionResp Transaction response parameters. Call getC3Error to check transaction result.
@param nApplicationNumber Specifies explicitely the Telium application to run on the companion (this application must implements PDA_START_TRANSACTION service)
@param inBuffer Pointer to the input extended data
@param inBufferSize Size of the input extended data
@param outBuffer Pointer to the output extended data
@param outBufferSize On input, size of the output buffer. On output, size of the extended data received from the companion.
@return TRUE in case of success, FALSE in case of failure */
PCL_SERVICE_API BOOL doTransactionEx( ITransactionIn* transactionReq, ITransactionOut* transactionResp, unsigned int nApplicationNumber,
						 unsigned char *inBuffer, unsigned long inBufferSize, unsigned char *outBuffer, unsigned long *outBufferSize );

/** Launches a transaction request, passing the input parameters to the companion payment application.
The output parameters are filled with the response from the companion payment application.
@param transactionReq Transaction request parameters
@param transactionResp Transaction response parameters. Read c3Error to check transaction result.
@return TRUE in case of success, FALSE in case of failure */
PCL_SERVICE_API BOOL doTransactionStruct(transactionIn_t* transactionReq, transactionOut_t* transactionResp);

/** Launches a transaction request with extended parameters, passing the input parameters to the companion payment application.
The output parameters are filled with the response from the companion payment application.
@param transactionReq Transaction request parameters
@param transactionResp Transaction response parameters. Read c3Error to check transaction result.
@param nApplicationNumber Specifies explicitely the Telium application to run on the companion (this application must implements PDA_START_TRANSACTION service)
@param inBuffer Pointer to the input extended data
@param inBufferSize Size of the input extended data
@param outBuffer Pointer to the output extended data
@param outBufferSize On input, size of the output buffer. On output, size of the extended data received from the companion.
@return TRUE in case of success, FALSE in case of failure */
PCL_SERVICE_API BOOL doTransactionStructEx( transactionIn_t* transactionReq, transactionOut_t* transactionResp, unsigned int nApplicationNumber,
						 unsigned char *inBuffer, unsigned long inBufferSize, unsigned char *outBuffer, unsigned long *outBufferSize );

/** Send message to the terminal
@param inBuffer pointer to the buffer containing the message to send to the terminal
@param inBufferSize Size of buffer to send
@param bytesSent Number of bytes sent, if the message cannot be sent the result is 0
@return TRUE if the service send the message to the terminal, FALSE if the sending failed */
PCL_SERVICE_API BOOL sendMessage(void * inBuffer, unsigned long inBufferSize, unsigned long * bytesSent);

/** Receive a message from the terminal 
@param outBuffer pointer to the buffer that will receive the message received
@param outBufferSize Size of reception buffer
@param bytesReceived Number of bytes received (If no message received, the value returned in this variable is 0)
@return TRUE if the service received the message from the terminal, FALSE if the received failed */
PCL_SERVICE_API BOOL receiveMessage(void * outBuffer, unsigned long outBufferSize, unsigned long * bytesReceived);

/** Flush messages received from the terminal
@return TRUE if the service flushed the messages, FALSE otherwise */
PCL_SERVICE_API BOOL flushMessages(void);

/** Launch M2OS Shortcut
@param pbShortcut Null-terminated string containing shortcut
@return TRUE if the shortcut was launched, FALSE otherwise */
PCL_SERVICE_API BOOL launchM2OSShortcut(char *pbShortcut);

/** Telium Input Simulation
@param pbString Null-terminated string containing keys to be simulated
@return TRUE if the keys are simulated, FALSE otherwise */
PCL_SERVICE_API BOOL inputSimul(char *pbString);

/** Telium Reset
@param nResetInfo code to return when resetting the terminal (see OEM_exit() definition in Telium SDK Help for more information on return code).
@return TRUE if the terminal resets, FALSE otherwise*/
PCL_SERVICE_API BOOL resetTerminal(int nResetInfo);

/** This structure is use by function getTerminalInfo to return serial and product number. */
typedef struct
{
	/// Serial number of the remote device.
    DWORD   dwSerialNumber;
	/// Product number of the remote device.
    DWORD   dwProductNumber;
}spmInfo_t;

/** Get Telium terminal information
@param spmInfo: information structure 
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL getTerminalInfo(spmInfo_t * spmInfo);

/** Get Telium terminal full serial number
@param fullSN: buffer to receive full serial number
@param fullSNSize: buffer size
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL getFullSerialNumber(char* fullSN, unsigned long fullSNSize);

/** Get battery level
@param batLevel: Pointer on int to receive battery level. Battery level is 0 if the terminal is not portable or has no battery.
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL getBatteryLevel(int* batLevel);

/** Get SPMCI version
@param spmciVersion: buffer to receive pcl version
@param fspmciVersionSize: buffer size
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL getSPMCIVersion(char* spmciVersion, unsigned long fspmciVersionSize);

/** Set Telium Time
@param result: status of the operation 
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL setTerminalTime(unsigned long * result);

/** Get Telium Time
@param lpSystemTime: time of the telium terminal 
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL getTerminalTime(LPSYSTEMTIME lpSystemTime);

/** This enum lists return of update function. */
typedef enum
{
	/// Return OK 
    SPM_UPDATE_OK, 
	/// Return KO 
    SPM_UPDATE_KO
}eSPMUpdate;

/** Launch Telium update
@param result: status of the operation 
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL doUpdate(unsigned long * result);

/** Fetch Components information
@param pbPath: Path where to store the Information file 
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL getTerminalComponents(char *pbPath);

/** Open Printer
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL openPrinter(char* result);

/** Close Printer
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL closePrinter(char* result);

/** Get Printer Status
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL getPrinterStatus(char* result);

/** Store Logo
@param pbBitmapName: name of the logo (8 chars max, in upper case)
@param bmpType: type of the bitmap
@param pbBitmap: bitmap buffer
@param bitmapSize: size of the bitmap
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL storeLogo(char* pbBitmapName, unsigned long bmpType, char* pbBitmap, unsigned long bitmapSize, char* result);

/** Print Text
@param pbString: Buffer to be printed
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL printText(char *pbString, char* result);

/** Print Bitmap
@param pbBitmap: Bitmap to be printed 
@param bitmapSize: Bitmap size
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL printBitmap(char *pbBitmap, unsigned long bitmapSize, char *result);

/** Print Logo
@param pbName: name of the logo (8 chars max, in upper case) 
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL printLogo(char *pbName, char* result);

/** This enum lists all the available fonts that can be used
 * on terminal printer. */
typedef enum {
	/** ISO8859-1 font (latin-1) */
	ISO8859_1 = 1, 
	/** ISO8859-2 font (latin-2) */
	ISO8859_2 = 2, 
	/** ISO8859-3 font (latin-3) */
	ISO8859_3 = 3, 
	/** ISO8859-5 font (cyrillic) */
	ISO8859_5 = 5, 
	/** ISO8859-6 font (arab) */
	ISO8859_6 = 6, 
	/** ISO8859-7 font (greek) */
	ISO8859_7 = 7,
	/** ISO8859-15 font (latin-9) */
	ISO8859_15 = 15,
} eFonts;

/** Set Printer Font
@param font: Font identifier 
@see eFonts
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL setPrinterFont(eFonts font, char* result);

/** Get PPP server status
@param result: status of the operation 
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL serverStatus(char *result);

/** Open Barcode with default inactivity timeout (600s)
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL openBarcode(char* result);

/** Open Barcode with inactivity timeout
@param inactivityTimeout: barcode reader will be automatically closed after this timeout (0 means no timeout)
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL openBarcodeWithInactivityTo(int inactivityTimeout, char* result);

/** Close Barcode
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL closeBarcode(char* result);

/** Start Barcode scan.
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrStartScan(char* result);

/** Stop Barcode scan.
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrStopScan(char* result);

/** This enum lists read mode that can be used on barcode reader. */
typedef enum {
	ICBarCodeScanMode_SingleScan=0,               /**< Single-Scan Mode */
	ICBarCodeScanMode_MultiScan                 /**< Multi-Scan Mode */
} eICBarCode_ScanMode;

/** Configure Barcode reader mode.
@param modeType: 0=single scan and 1=multiple scan
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrSetReaderMode(int modeType, char* result);

/** This enum lists good scan beep that can be used on barcode reader. */
typedef enum {
	/// Disable beep
	ICBarCodeGoodScanBeep_Disable=0,             
	/// One beep
	ICBarCodeGoodScanBeep_OneBeep=1,         
	/// Two beep
	ICBarCodeGoodScanBeep_TwoBeep=2           
} eICBarCode_GoodScanBeep;

/** Configure Barcode good scan beep.
@param modeBeep: 0=disable scan, 1=one beep and 2=two beep
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrSetGoodScanBeep(int modeBeep, char* result);

/** Enable/disable Barcode trigger.
@param isEnable: 0=disable scan and 1=enable
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrEnableTrigger(int isEnable, char* result);

/** This enum lists imager mode that can be used on barcode reader. */
typedef enum
{
    ICBarCodeImagerMode_1D=0,                     /**< 1D */
	ICBarCodeImagerMode_1D2D=1,                   /**< 1D and 2D standard */
	ICBarCodeImagerMode_1D2D_bright=2,            /**< 1D and 2D bright environment */
	ICBarCodeImagerMode_1D2D_reflective=3         /**< 1D and 2D reflective surface */
} eICBarCode_ImagerMode;

/** Configure Barcode imager mode.
@param imagerMode: 0=1D, 1=1D 2D, 2=1D 2D bright and 3=1D 2D reflective.
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrSetImagerMode(int imagerMode, char* result);
  
/** Configure Barcode beep.
@param freq: frequency in Hz (1000 to 5110 Hz).
@param length: duration in milliseconds (0 to 2550 ms).
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrSetBeep(int freq, int length , char* result);

/** This enum lists lighting mode that can be used on barcode reader. */
typedef enum {
	illuminiationLEDPriority=0,       /**< Shorter exposure time  */
	aperturePriority=1                /**< Use aperture priority if you have a shiny barcode label */
}eICBarCode_LightingMode;

/** Configure Barcode lighting mode.
@param mode: 0= illumination priority, 1= aperture priority.
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrSetLightingMode(int mode, char* result);

/** Barcode soft reset.
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrSoftReset(char* result);

/** This enum lists all the available symbologies that can be used on barcode reader. */
typedef enum {
	ICBarCode_Unknown = -1,                     /**< Unknown Symbology */
	ICBarCode_AllSymbologies	= 0,            /**< All Symbologies. Can be used only with apiDisableSymbologies */
	
	ICBarCode_EAN13,                            /**< EAN13 Barcode Type */
	ICBarCode_EAN8,                             /**< EAN8 Barcode Type */
	ICBarCode_UPCA,                             /**< UPCA Barcode Type */
	ICBarCode_UPCE,                             /**< UPCE Barcode Type */
	
	ICBarCode_EAN13_2,                          /**< EAN13_2 Barcode Type */
	ICBarCode_EAN8_2,                           /**< EAN8_2 Barcode Type */
	ICBarCode_UPCA_2,                           /**< UPCA_2 Barcode Type */
	ICBarCode_UPCE_2,                           /**< UPCE_2 Barcode Type */
	
	ICBarCode_EAN13_5,                          /**< EAN13_5 Barcode Type */
	ICBarCode_EAN8_5,                           /**< EAN8_5 Barcode Type */
	ICBarCode_UPCA_5,                           /**< UPCA_5 Barcode Type */
	ICBarCode_UPCE_5,                           /**< UPCE_5 Barcode Type */
	
	ICBarCode_Code39,                           /**< Code39 Barcode Type */

	ICBarCode_Interleaved2of5 = 15,             /**< Interleaved2of5 Barcode Type */
	ICBarCode_Standard2of5,                     /**< Standard2of5 Barcode Type */
	ICBarCode_Matrix2of5,                       /**< Matrix2of5 Barcode Type */
	
	ICBarCode_CodaBar = 19,                     /**< CodeBar Barcode Type */
//	ICBarCode_AmesCode,                         /**< AmesCode Barcode Type. Not supported */
	ICBarCode_MSI = 21,                         /**< MSI Barcode Type */
	ICBarCode_Plessey,                          /**< Pleassey Barcode Type */
	
	ICBarCode_Code128,                          /**< Code128 Barcode Type */
//	ICBarCode_Code16K,                          /**< Code16k Barcode Type. Not supported */
	ICBarCode_93 = 25,                               /**< Code93 Barcode Type */
	ICBarCode_11,                               /**< Code11 Barcode Type */

	ICBarCode_Telepen,                          /**< Telepen Barcode Type */
//	ICBarCode_Code49,                           /**< Code49 Barcode Type. Not supported */
	ICBarCode_Code39_ItalianCPI = 29,           /**< Code39_ItalianCPI Barcode Type */
	
	ICBarCode_CodaBlockA,                       /**< CodeBlockA Barcode Type */
	ICBarCode_CodaBlockF,                       /**< CodaBlockF Barcode Type */
	
	ICBarCode_PDF417 = 33,                      /**< PDF417 Barcode Type */
	ICBarCode_GS1_128,							/**< GS1_128 Barcode Type, Replace EAN128 */
	ICBarCode_ISBT128,                          /**< ISBT128 Barcode Type */
	ICBarCode_MicroPDF,                         /**< MicroPDF Barcode Type */

    ICBarCode_GS1_DataBarOmni,                  /**< GS1_DataBarOmni Barcode Type */
    ICBarCode_GS1_DataBarLimited,               /**< GS1_DataBarLimited Barcode Type */
    ICBarCode_GS1_DataBarExpanded,              /**< GS1_DataBarExpanded Barcode Type */

	ICBarCode_DataMatrix,                       /**< DataMatrix Barcode Type */
    ICBarCode_QRCode,                           /**< QRCode Barcode Type */
	ICBarCode_Maxicode,                         /**< Maxicode Barcode Type */
    ICBarCode_UPCE1,                            /**< UPC-E1 Barcode Type */

    ICBarCode_Aztec = 0x4A,                     /**< Aztec Barcode Type */
	
	
	ICBarCode_MaxIndex                          /**< MaxIndex Barcode Type */
} eICBarCodeSymbologies;

/** Configure enable symbologies on Barcode.
@param symbologies: Array containing barcode symbologies to enable.
@param symbologyCount: Size of symbologies array.
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrEnableSymbologies(int *symbologies, int symbologyCount, char* result); // symbologies: int[] containing eBCRSymbologies

/** Configure disable symbologies on Barcode.
@param symbologies: Array containing barcode symbologies to disable.
@param symbologyCount: Size of symbologies array.
@param result: status of the operation
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrDisableSymbologies(int *symbologies, int symbologyCount, char* result); // symbologies: int[] containing eBCRSymbologies

/** Convert symbology number to human-readable string.
@param type: Symbology code.
@return A string corresponding to input symbology number. */
PCL_SERVICE_API char* bcrSymbologyToText(int type);

/** Convert UDSI code to symbology number.
@param code_udsi: UDSI code.
@return Symbology number corresponding to input UDSI code. */
PCL_SERVICE_API int bcrCodeUDSIToSymbology(char* code_udsi);

/** Return the firmware version of the barcode reader.
@param version: A buffer to store the firmware version.
@param versionSize: The input buffer size.
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrGetFirmwareVersion(char* version, unsigned long versionSize);

/** Set storage mode for parameters. When using non volatile mode, parameters are restored when the barcode reader is opened. 
@param set:    1 to set non volatile mode, 0 to set volatile mode
@param result: Result of the operation (0=OK, 1=KO)
@return        TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrSetNonVolatileMode(int set, char* result);
    
/** Set version number for parameters stored in non volatile memory
@param version:     number (two bytes)
@param versionSize: The size of input buffer.
@param result:      Result of the operation (0=OK, 1=KO)
@return			    TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrSetSettingsVersion(char* version, unsigned int versionSize, char* result);
    
/** Get version number for parameters stored in non volatile memory
@param version: Buffer to receive version number (two bytes)
@param versionSize: The size of input buffer.
@return         TRUE if success else return FALSE */
PCL_SERVICE_API BOOL bcrGetSettingsVersion(char* version, unsigned int versionSize);

/** This structure is use by function tmsReadParam to return list of ssl profiles. */
typedef struct
{
	/// number of profiles
	unsigned int   nb_ssl_profile;
	/// matrix of profiles
    unsigned char  ssl_profiles[20][12];
	/// the current profile
    unsigned char  current_ssl_profile[12];
}tms_ssl_parameter_t;

/** Read TMS parameters of terminal.
@param addr: Buffer to receive ip address or domain name (max size 258).
@param addrSize: The size of input buffer addr.
@param port: Buffer to receive port (max size 6).
@param portSize: The size of input buffer port.
@param identifier: Buffer to receive identifier (max size 11).
@param identifierSize: The size of input buffer identifier.
@param ssl_profiles: Structure to receive SSL information.
@param result: Status of the operation (0=OK, 1=KO)
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL tmsReadParam(char* addr, unsigned int addrSize, char* port, unsigned int portSize, char* identifier, unsigned int identifierSize, tms_ssl_parameter_t* ssl_profiles, char* result);

/** Configure TMS parameters of terminal.
@param	addr: TMS IP address or domain name. Empty string or NULL to keep the current parameter.
@param	port: TMS port. Empty string or NULL to keep the current parameter. 
@param	identifier: TMS Identifier. Empty string or NULL to keep the current parameter. 
@param	ssl_profile: The name of TMS SSL profile. If NULL to keep the current parameter and if empty string to disable SSL.
@param	result: Status of the operation (0=OK, 1=KO)
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL tmsWriteParam(char* addr, char* port, char* identifier, char* ssl_profile, char* result);

/** Set backlight lock
@param lock 0 to unlock backlight, 3 to lock backlight (unlocked by keyboard)
@param result Status of the operation (0=OK, 1=KO)
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL setBacklightLock(int lock, char* result);

#define BRIDGE_TOWARDS_TELIUM	0
#define BRIDGE_FROM_TELIUM		1

/** Add dynamic bridge. You can add up to 12 bridges.
@param port TCP port
@param direction 0 means Windows connects to Telium, 1 means Telium connects to Windows
@return 0 if success else negative error code:
 -1: No more dynamic bridges available
 -2: Bridge already exists. This can be considered as information and is not a real error.
 -3: Issue during thread creation.
 -4: Bridge initialization failed.
 -5: PCL service is not started.*/
PCL_SERVICE_API int addDynamicBridge(int port, int direction);

/** Add dynamic bridge on local interface. You can add up to 12 bridges.
@param port TCP port
@param direction 0 means Windows connects to Telium, 1 means Telium connects to Windows
@return 0 if success else negative error code:
 -1: No more dynamic bridges available
 -2: Bridge already exists. This can be considered as information and is not a real error.
 -3: Issue during thread creation.
 -4: Bridge initialization failed.
 -5: PCL service is not started.*/
PCL_SERVICE_API int addDynamicBridgeLocal(int port, int direction);

#ifdef USE_OPENSSL
/** Set path of client certificate.
@param path The path of client certificate (max size MAX_PATH).
@param pathSize The size of input path.
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL setClientCertificate(char * path, unsigned int pathSize);

/** Set path of client key.
@param path The path of client key (max size MAX_PATH).
@param pathSize The size of input path.
@param privateKey The private key of client key file (max size 32).
@param keySize The size of input key.
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL setClientKey(char * path, unsigned int pathSize, char* privateKey, unsigned int keySize);

/** Set path of Diffie-Hellman file.
@param path The path of Diffie-Hellman file (max size MAX_PATH).
@param pathSize The size of input path.
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL setDHFile(char * path, unsigned int pathSize);
#endif

/** Request to open the cash drawer connected to a printer
@param result   Status of the operation (0=OK, 1=KO)
@return TRUE if success else return FALSE */
PCL_SERVICE_API BOOL openCashDrawer(char* result);

/** This callback allows retrieving data scanned with Companion barcode reader.
@param data	buffer containing barcode reader data
@param len	buffer length */
typedef void (CALLBACK *BarcodeEventFunc)(char*, int);

/** Register barcode callback
@param pBarcodeEventFunc function call when */
PCL_SERVICE_API void registerBarcodeEventCallback(BarcodeEventFunc pBarcodeEventFunc);

/** This callback allows retrieving data scanned with Companion barcode reader.
@param barcode	buffer containing barcode reader data
@param barcodelen	buffer length
@param symbology	buffer containing symbologie of barcode
@param symbologylen	buffer length */
typedef void (CALLBACK *BarcodeEventExtFunc)(char*, int, char*, int);

/** Register barcode extention callback
@param pBarcodeEventExtFunc function */
PCL_SERVICE_API void registerBarcodeEventExtCallback(BarcodeEventExtFunc pBarcodeEventExtFunc);

/** This callback is called when the Telium application needs signature capture. It should return a 1-bit depth bitmap representing the signature. 
@param width		   signature maximum width
@param height		   signature maximum height
@param timeout		   signature timeout
@param pBufOut		   buffer to contain signature bitmap
@param pdwBufOutSize   on input, contains buffer size; on output, contains bitmap size */
typedef int (CALLBACK *SignCapFunc)(int, int, int, unsigned char *, unsigned long *);

/** Register callback function that will catch the signature capture event. This function is kept for compatibility. You should now use apiRegisterCallbacks instead.
@param pBarcodeEventExtFunc function */
PCL_SERVICE_API void registerSignCapCallback(SignCapFunc pSignCapFunc);

/** This callback is called when the Telium application wants to print text.   
@param text			Null-terminated string containing the text to be printed
@param font			The font identifier
@param justification	Indicates text alignment (0=centered, 1=right, 2=left)
@param xFactor		The factor by which the text should be scaled in the X direction. Possible values are 1, 2 or 4 (normal, double or quadruple text width).
@param yFactor		The factor by which the text should be scaled in the Y direction. Possible values are 1, 2 or 4 (normal, double or quadruple text width)
@param underline		Indicates whether text must be underlined (1) or not (0)
@param bold			Indicates whether text must be bold (1) or not (0)
@param charset		Indicates character set to be used 
@return 0 if successful */
typedef int (CALLBACK *PrintTextFunc)(char* szText, unsigned char font, unsigned char justification, unsigned char xFactor, unsigned char yFactor, unsigned char underline, unsigned char bold, unsigned char charset);

/** This callback is called when the Telium application wants to print image. 
@param width			Image width
@param height			Image height
@param image			Bitmap containing the image to be printed
@param size				Bitmap buffer size
@param justification	Indicates image alignment (0=centered, 1=right, 2=left) 
@return 0 if successful */
typedef int (CALLBACK *PrintImageFunc)(unsigned long width, unsigned long height, char* image, unsigned long size, unsigned char justification);

/** This callback is called when the Telium application wants to feed paper.  
@param dwLines	Number of lines
@return 0 if successful */
typedef int (CALLBACK *FeedPaperFunc)(DWORD dwLines);

/** This callback is called when the Telium application wants to cut paper, indicating the ticket is finished.  
@return 0 if successful */
typedef int (CALLBACK *CutPaperFunc)();

/** This callback is called when the Telium application wants to start printing a receipt.  
@param type Indicates the receipt type (0=merchant, 1=customer).
@return 0 if successful */
typedef int (CALLBACK *StartReceiptFunc)(unsigned char type);

/** This callback is called when the Telium application wants to end printing a receipt.  
@return 0 if successful */
typedef int (CALLBACK *EndReceiptFunc)();

/** This callback is called when the Telium application wants to add signature to the receipt.  
@return 0 if successful */
typedef int (CALLBACK *AddSignatureFunc)();

/** This callback is called when the barcode reader is automatically closed on inactivity timeout expiration.    
@return 0 if successful */
typedef int (CALLBACK *BarcodeEventCloseFunc)();

/** Structure content all functions callbacks */
typedef struct
{
    BarcodeEventFunc	pBarcodeEventFunc; 			/**< Callback barcode event */
    SignCapFunc			pSignCapFunc;				/**< Callback signature event */
	PrintTextFunc		pPrintTextFunc;				/**< Callback print text event */
	PrintImageFunc		pPrintImageFunc;			/**< Callback print image event */
	FeedPaperFunc		pFeedPaperFunc;				/**< Callback feed paper event */
	CutPaperFunc		pCutPaperFunc;				/**< Callback cut paper event */
	StartReceiptFunc	pStartReceiptFunc;			/**< Callback start receipt event */
	EndReceiptFunc		pEndReceiptFunc;			/**< Callback end receipt event */
	AddSignatureFunc	pAddSignatureFunc;			/**< Callback add signature event */
	BarcodeEventCloseFunc	pBarcodeEventCloseFunc; /**< Callback barcode close event */
	BarcodeEventExtFunc	pBarcodeEventExtFunc;		/**< Callback barcode event extended */
}callbacks_t;

/** Register callback functions to handle signature capture, barcode events and printing from Telium
@param sCallbacks structure with all functions callbacks */
PCL_SERVICE_API void registerCallbacks(callbacks_t sCallbacks);

 /** Enable log
     @param enable: If true, enable logs.*/
PCL_SERVICE_API void enableDebugLog(bool enable);

/** Get logs state.
    @return      true is logs are enabled. */
PCL_SERVICE_API bool isDebugLogEnabled();

/** Add Authorized USB Vid/Pid in PCLService
* @param  pVid USB Vid 
* @param  pPid USB Pid 
* @return TRUE in case of success, else FALSE
 @note by default authorized :
 (0x079B, 0x0028), (0x0B00, 0x0060), (0x0B00, 0x0061), (0x0B00, 0x0062), (0x0B00, 0x0064), (0x0B00, 0x0066), (0x0B00, 0x0057), 
 (0x0B00, 0x0052), (0x0B00, 0x0053), (0x0B00, 0x0054), (0x0B00, 0x0055), (0x0B00, 0x0063), (0x0B00, 0x0080), (0x0B00, 0x0081), (0x0B00, 0x0083)*/
PCL_SERVICE_API BOOL addAuthorizedVidPidSrv(unsigned short pVid, unsigned short pPid);

#ifdef __cplusplus
} // extern "C"
#endif // _cplusplus_

#endif