// NETWORK.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include "PclTestApp.h"
#include "NETWORK.h"
#include "afxdialogex.h"
#include <winsock2.h> 
#include "pdautil.h"

//Global variables
extern BOOL isKeyboardPresent;


BYTE trama[]=	"\x3c\x3f\x78\x6d\x6c\x20\x76\x65" \
"\x72\x73\x69\x6f\x6e\x3d\x22\x31\x2e\x30\x22\x20\x65\x6e\x63\x6f" \
"\x64\x69\x6e\x67\x3d\x22\x55\x54\x46\x2d\x38\x22\x3f\x3e\x0a\x3c" \
"\x72\x65\x71\x75\x65\x73\x74\x5f\x6d\x65\x73\x73\x61\x67\x65\x3e" \
"\x0a\x09\x3c\x68\x65\x61\x64\x65\x72\x20\x74\x79\x70\x65\x3d\x22" \
"\x73\x61\x6c\x65\x22\x20\x6d\x65\x73\x73\x61\x67\x65\x5f\x69\x64" \
"\x3d\x22\x31\x22\x20\x76\x65\x72\x73\x69\x6f\x6e\x3d\x22\x30\x31" \
"\x22\x20\x2f\x3e\x0a\x09\x3c\x64\x61\x74\x61\x3e\x0a\x09\x09\x3c" \
"\x66\x69\x65\x6c\x64\x20\x6e\x61\x6d\x65\x3d\x22\x41\x6d\x6f\x75" \
"\x6e\x74\x22\x3e\x30\x30\x30\x30\x30\x30\x30\x30\x30\x32\x30\x30" \
"\x3c\x2f\x66\x69\x65\x6c\x64\x3e\x0a\x09\x09\x3c\x66\x69\x65\x6c" \
"\x64\x20\x6e\x61\x6d\x65\x3d\x22\x43\x75\x72\x72\x65\x6e\x63\x79" \
"\x43\x6f\x64\x65\x22\x3e\x30\x39\x37\x38\x3c\x2f\x66\x69\x65\x6c" \
"\x64\x3e\x0a\x09\x09\x3c\x66\x69\x65\x6c\x64\x20\x6e\x61\x6d\x65" \
"\x3d\x22\x54\x69\x70\x22\x3e\x31\x30\x30\x3c\x2f\x66\x69\x65\x6c" \
"\x64\x3e\x0a\x09\x09\x3c\x66\x69\x65\x6c\x64\x20\x6e\x61\x6d\x65" \
"\x3d\x22\x41\x64\x64\x69\x74\x69\x6f\x6e\x61\x6c\x42\x75\x73\x69" \
"\x6e\x65\x73\x73\x44\x61\x74\x61\x22\x3e\x54\x65\x73\x74\x41\x64" \
"\x64\x69\x74\x42\x75\x73\x73\x69\x6e\x65\x73\x3c\x2f\x66\x69\x65" \
"\x6c\x64\x3e\x0a\x09\x3c\x2f\x64\x61\x74\x61\x3e\x0a\x3c\x2f\x72" \
"\x65\x71\x75\x65\x73\x74\x5f\x6d\x65\x73\x73\x61\x67\x65\x3e";


#define BUFFER_SIZE 256
// Bo�te de dialogue NETWORK

IMPLEMENT_DYNAMIC(NETWORK, CDialog)

NETWORK::NETWORK(CWnd* pParent /*=NULL*/)
	: CDialog(NETWORK::IDD, pParent)
{

}

NETWORK::~NETWORK()
{
}

void NETWORK::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NETWORKPORT, var_networkPort);
	DDX_Control(pDX, IDC_NETWORKPACKETSIZE, var_networkPacketSize);
	DDX_Control(pDX, IDC_NETWORKPACKETNUMBER, var_networkPacketNumber);
	DDX_Control(pDX, IDC_NETWORKDIRECTION, var_list_direction);
	DDX_Control(pDX, IDC_NETWORK_STATUS, m_networkStatus);
}


BEGIN_MESSAGE_MAP(NETWORK, CDialog)
	ON_BN_CLICKED(IDOK, &NETWORK::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTT_NETWORKTEST, &NETWORK::OnBnClickedButtNetworktest)
	ON_EN_KILLFOCUS(IDC_NETWORKPACKETSIZE, &NETWORK::OnEnKillfocusNetworkpacketsize)
	ON_EN_KILLFOCUS(IDC_NETWORKPACKETNUMBER, &NETWORK::OnEnKillfocusNetworkpacketnumber)
	ON_EN_KILLFOCUS(IDC_NETWORKPORT, &NETWORK::OnEnKillfocusNetworkport)
	ON_BN_CLICKED(IDC_BUTTON_BRIDGE, &NETWORK::OnBnClickedButtonBridge)
END_MESSAGE_MAP()


BOOL NETWORK::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: ajoutez ici une initialisation suppl�mentaire
	var_networkPort.LimitText(5);
	var_networkPacketNumber.LimitText(5);

	var_list_direction.AddString(_T("Windows to Telium"));
	var_list_direction.AddString(_T("Telium to Windows"));

	    if ( !isKeyboardPresent )
	{
		TCHAR  infoBuf[1024];
		infoBuf[0]='\0';
		if ( SHGetSpecialFolderPath(0,infoBuf,CSIDL_PROGRAM_FILES,FALSE) )
		{
			infoBuf[2]='\0';
			PathAppend(infoBuf, TEXT("\\Program Files\\Common Files\\microsoft shared\\ink\\tabtip.exe"));
			ShellExecute(NULL, NULL,infoBuf, NULL, NULL, SW_SHOWNORMAL);
		}
	}

	return TRUE;  // retourne TRUE, sauf si vous avez d�fini le focus sur un contr�le
}

// Gestionnaires de messages de NETWORK


void NETWORK::OnBnClickedOk()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	
	if ( !isKeyboardPresent )
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	CDialog::OnOK();
}


void NETWORK::OnBnClickedButtNetworktest()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contr�le
	 ///////////////////////

	char charBuff[BUFFER_SIZE];
	wchar_t buffer[BUFFER_SIZE];
	WCHAR textStatus[BUFFER_SIZE*4];	// 128
	size_t i;
	int port;
	int packetNumber;
	int packetSize;
	int n;
	int pkt_sent = 0, pkt_recvd = 0;
	int bytes_sent= 0, bytes_recvd = 0;
	int error = 0;
	const char* TeliumToWindows = "T";
	const char* WindowsToTelium = "W";
	DWORD tick;
	int ret;

	var_networkPort.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
    sscanf_s(charBuff, "%d", &port);
	var_networkPacketSize.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	sscanf_s(charBuff, "%d", &packetSize);
	var_networkPacketNumber.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
    sscanf_s(charBuff, "%d", &packetNumber);

	var_list_direction.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	charBuff[1] =0;

	//============================== SOCKET ====================================
	WSADATA wsa;
	BYTE bufferReceived[2048];
	BYTE bufferToSend [2048];
    if (WSAStartup(MAKEWORD(2, 2), &wsa))
	{
		return;
	}
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock == INVALID_SOCKET)
	{
		m_networkStatus.SetWindowTextW(L"socket failed");
		WSACleanup();
		return;
	}

	struct hostent *hostinfo = NULL;
	SOCKADDR_IN sin = { 0 }; /* initialise la structure avec des 0 */
	const char *hostname = "127.0.0.1";

	
	sin.sin_port = htons(port); /* on utilise htons pour le port */
	sin.sin_family = AF_INET;

	//SEND PACKETS FROM WINDOWS TO TELIUM
	if (!strcmp(charBuff,WindowsToTelium))
	{
		
		ret = addDynamicBridge(port, BRIDGE_TOWARDS_TELIUM);
		if (ret == 0 || ret == -2)
		{
			sin.sin_addr.s_addr = inet_addr(hostname);
			m_networkStatus.SetWindowTextW(L"Connection in progress...");
			if(connect(sock,(SOCKADDR *) &sin, sizeof(sin)) == SOCKET_ERROR)
			{
				error = WSAGetLastError();
				swprintf_s(textStatus, L"Connection failed with error %d", error);
				m_networkStatus.SetWindowTextW(textStatus);
				closesocket(sock);
				WSACleanup();
				return;
			}
			m_networkStatus.SetWindowTextW(L"Connected");


			int lentrama= strlen((char*)trama);

			for(int i =0; i <= packetSize; i++)	// packetSize
			{
				bufferToSend[i] = i ; //trama[i];
			}
	
			tick = GetTickCount();
			error = 0;
			while(packetNumber--)
			{
				int rx;
				if((n = send(sock, (const char*)bufferToSend, packetSize, 0)) < 0)
				{
					error = WSAGetLastError();
					swprintf_s(textStatus, L"send failed with error %d", error);
					m_networkStatus.SetWindowTextW(textStatus);
					break;
				}
				bytes_sent += n;
				pkt_sent++;
				swprintf_s(textStatus, L"Sent: %d / Recvd: %d", bytes_sent, bytes_recvd);
				m_networkStatus.SetWindowTextW(textStatus);

				rx = 0;
				do {
					if((n = recv(sock,  (char*)&bufferReceived[rx], sizeof(bufferReceived)-rx, 0)) < 0)
					{
						error = WSAGetLastError();
						swprintf_s(textStatus, L"recv failed with error %d", error);
						m_networkStatus.SetWindowTextW(textStatus);
					}
					else if (n == 0)
					{
						swprintf_s(textStatus, L"socket closed by peer");
						m_networkStatus.SetWindowTextW(textStatus);
						error = 1;
					}
					else
					{
						rx += n;
						bytes_recvd += n;
						swprintf_s(textStatus, L"Sent: %d / Recvd: %d", bytes_sent, bytes_recvd);
						m_networkStatus.SetWindowTextW(textStatus);
					}
				
				} while ((n > 0) && (rx < packetSize));
				if (error != 0)
				{
					break;
				}
				pkt_recvd++;
			}
			if (error == 0)
			{
				DWORD tickResult = GetTickCount() - tick;
				if(tickResult != 0)
				{
					swprintf_s(textStatus, L"Sent: %d / Recvd: %d\nElapsed time = %d ms\nBit rate = %lu bits/s", bytes_sent, bytes_recvd, tickResult, (DWORD)(((bytes_sent + bytes_recvd)*8000)/tickResult));
				}else{
					swprintf_s(textStatus, L"Sent: %d / Recvd: %d\nElapsed time = %d ms", bytes_sent, bytes_recvd, tickResult);
				}
				m_networkStatus.SetWindowTextW(textStatus);
			}
			shutdown(sock, SD_BOTH);
		}
		else
		{
			swprintf_s(textStatus, L"addDynamicBridge failed with error %d", ret);
			m_networkStatus.SetWindowTextW(textStatus);
		}
	}

	//SEND PACKETS FROM TELIUM TO WINDOWS
	else if(!strcmp(charBuff,TeliumToWindows))
	{

		int actual = 0;
		int max = (int)sock;
		fd_set rdfs;

		ret = addDynamicBridge(port, BRIDGE_FROM_TELIUM);
		if (ret == 0 || ret == -2)
		{
			sin.sin_addr.s_addr = inet_addr(hostname);

			if(bind (sock, (SOCKADDR *) &sin, sizeof sin) == SOCKET_ERROR)
			{
				error = WSAGetLastError();
				swprintf_s(textStatus, L"bind failed with error %d", error);
				m_networkStatus.SetWindowTextW(textStatus);
				closesocket(sock);
				WSACleanup();
				return;
			}

			if(listen(sock, 1) == SOCKET_ERROR)
			{
				error = WSAGetLastError();
				swprintf_s(textStatus, L"listen failed with error %d", error);
				m_networkStatus.SetWindowTextW(textStatus);
				closesocket(sock);
				WSACleanup();
				return;
			}
			SOCKADDR_IN csin = { 0 };

			SOCKET csock;

			int sinsize = sizeof csin;
			FD_ZERO(&rdfs);

			/* add the connection socket */
			FD_SET(sock, &rdfs);

			if(select(max + 1, &rdfs, NULL, NULL, NULL) == -1)
			{
				error = WSAGetLastError();
				swprintf_s(textStatus, L"select failed with error %d", error);
				m_networkStatus.SetWindowTextW(textStatus);
				closesocket(sock);
				WSACleanup();
				return;
			}

			csock = accept(sock, (SOCKADDR *)&csin, &sinsize);

			if(csock == INVALID_SOCKET)
			{
				error = WSAGetLastError();
				swprintf_s(textStatus, L"accept failed with error %d", error);
				m_networkStatus.SetWindowTextW(textStatus);
				closesocket(sock);
				WSACleanup();
				return;
			}

			char buffer[1500];
			int n = 0;

			while(1)
			{
				if((n = recv(csock, buffer, sizeof(buffer), 0)) < 0)
				{
					error = WSAGetLastError();
					swprintf_s(textStatus, L"recv failed with error %d", error);
					m_networkStatus.SetWindowTextW(textStatus);
					break;
				}

				if (n>0)
				{
					bytes_recvd += n;
					pkt_recvd++;
					swprintf_s(textStatus, L"Sent: %d / Recvd: %d", bytes_sent, bytes_recvd);
					m_networkStatus.SetWindowTextW(textStatus);
					if(n = send(csock, buffer, n, 0) < 0)
					{
						error = WSAGetLastError();
						swprintf_s(textStatus, L"send failed with error %d", error);
						m_networkStatus.SetWindowTextW(textStatus);
						break;
					}
					bytes_sent += n;
					pkt_sent++;

					WCHAR wbuffer[2048];
					size_t convertedChars = 0;
					swprintf_s(wbuffer, L"%s" ,buffer);
					mbstowcs_s(&convertedChars, wbuffer, buffer, (size_t) BUFFER_SIZE);
					swprintf_s(textStatus, L"Sent: %d / Recvd: %d \n%s", bytes_sent, bytes_recvd,wbuffer);
					m_networkStatus.SetWindowTextW(textStatus);
				}

				if (n == 0)
				{
					shutdown(csock, SD_BOTH);
					closesocket(csock);
					break;
				}
			}
		}
		else
		{
			swprintf_s(textStatus, L"addDynamicBridge failed with error %d", ret);
			m_networkStatus.SetWindowTextW(textStatus);
		}

	}
	if ( !isKeyboardPresent )
	{
		CWnd* hwndTip = FindWindow(TEXT("IPTip_Main_Window"),NULL);

		if (hwndTip)
			hwndTip->PostMessageW(WM_SYSCOMMAND,SC_CLOSE,NULL);
	}
	closesocket(sock);
	WSACleanup();
}


void NETWORK::OnEnKillfocusNetworkport()
{
	// TODO:  S'il s'agit d'un contr�le RICHEDIT, le contr�le ne
	// envoyez cette notification sauf si vous substituez CDialog::OnInitDialog()
	// fonction pour envoyer le message EM_SETEVENTMASK au contr�le
	// avec l'indicateur ENM_UPDATE ajout� au masque lParam gr�ce � l'op�rateur OR.

	// TODO:  Ajoutez ici le code de votre gestionnaire de notification de contr�le
	char charBuff[BUFFER_SIZE];
	wchar_t buffer[BUFFER_SIZE];
	size_t i;
	int port;

	var_networkPort.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
    sscanf_s(charBuff, "%d", &port);

	if((port < 1025) || (port > 65535))
	{
		wchar_t * msg = L"Enter a valid port number (between 1025 and 65535)";
 
		// Display Unicode string
		MessageBox(msg, L"Invalid Port",0);
	}
	var_networkPort.GetFocus();
	
}

void NETWORK::OnEnKillfocusNetworkpacketnumber()
{
	// TODO:  S'il s'agit d'un contr�le RICHEDIT, le contr�le ne
	// envoyez cette notification sauf si vous substituez CDialog::OnInitDialog()
	// fonction pour envoyer le message EM_SETEVENTMASK au contr�le
	// avec l'indicateur ENM_UPDATE ajout� au masque lParam gr�ce � l'op�rateur OR.

	// TODO:  Ajoutez ici le code de votre gestionnaire de notification de contr�le
	char charBuff[BUFFER_SIZE];
	wchar_t buffer[BUFFER_SIZE];
	size_t i;
	int packetNumber;

	var_networkPacketNumber.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
    sscanf_s(charBuff, "%d", &packetNumber);
}

void NETWORK::OnEnKillfocusNetworkpacketsize()
{
	// TODO:  S'il s'agit d'un contr�le RICHEDIT, le contr�le ne
	// envoyez cette notification sauf si vous substituez CDialog::OnInitDialog()
	// fonction pour envoyer le message EM_SETEVENTMASK au contr�le
	// avec l'indicateur ENM_UPDATE ajout� au masque lParam gr�ce � l'op�rateur OR.


	// TODO:  Ajoutez ici le code de votre gestionnaire de notification de contr�le
	char charBuff[BUFFER_SIZE];
	wchar_t buffer[BUFFER_SIZE];
	size_t i;
	int packetSize;

	var_networkPacketSize.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	sscanf_s(charBuff, "%d", &packetSize);

	if(packetSize > 1500)
	{

    wchar_t * msg = L"Enter a valid packet size (<= 1500)";
 
    // Display Unicode string
    MessageBox(msg, L"Invalid Packet size",0);

}
}

void NETWORK::OnBnClickedButtonBridge()
{

	char charBuff[BUFFER_SIZE];
	wchar_t buffer[BUFFER_SIZE];
	WCHAR textStatus[128];
	size_t i;
	int port;
	int n;
	int error = 0;
	const char* TeliumToWindows = "T";
	const char* WindowsToTelium = "W";
	
	int ret;


	var_networkPort.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
    sscanf_s(charBuff, "%d", &port);

	var_list_direction.GetWindowText(buffer, BUFFER_SIZE);
	wcstombs_s (&i, charBuff, (size_t)BUFFER_SIZE, buffer, (size_t)BUFFER_SIZE);
	charBuff[1] =0;

	if (!strcmp(charBuff,WindowsToTelium))
	{		
		ret = addDynamicBridge(port, BRIDGE_TOWARDS_TELIUM);
		if (ret == 0 || ret == -2)
		{
			m_networkStatus.SetWindowTextW(L"BRIDGE_TOWARDS_TELIUM OK");
		}else
		{
			swprintf_s(textStatus, L"addDynamicBridge failed with error %d", ret);
			m_networkStatus.SetWindowTextW(textStatus);
		}
	} else if(!strcmp(charBuff,TeliumToWindows))
	{
		ret = addDynamicBridge(port, BRIDGE_FROM_TELIUM);
		if (ret == 0 || ret == -2)
		{
			m_networkStatus.SetWindowTextW(L"BRIDGE_FROM_TELIUM OK");
		}else
		{
			swprintf_s(textStatus, L"addDynamicBridge failed with error %d", ret);
			m_networkStatus.SetWindowTextW(textStatus);
		}
	}


}
