// BARCODE_DIALOG.cpp�: fichier d'impl�mentation
//

#include "stdafx.h"
#include <stdio.h>
#include "PclTestApp.h"
#include "BARCODE_DIALOG.h"
#include "afxdialogex.h"
#include "pdautil.h"

#define BUFFER_BCR_SIZE 256
// Bo�te de dialogue BARCODE_DIALOG

extern BOOL isKeyboardPresent;
void* pBcrDlg = NULL;

IMPLEMENT_DYNAMIC(BARCODE_DIALOG, CDialogEx)

BARCODE_DIALOG::BARCODE_DIALOG(CWnd* pParent /*=NULL*/)
	: CDialogEx(BARCODE_DIALOG::IDD, pParent)
{
		pBcrDlg = this;
}

BARCODE_DIALOG::~BARCODE_DIALOG()
{
}

void BARCODE_DIALOG::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATUS_BCR, textStatus);
	DDX_Control(pDX, IDC_RICHEDIT_COMMENTS, m_comment);
	DDX_Control(pDX, IDC_LIST_SYMBOLOGY_BCR, m_listSymbology);
	DDX_Control(pDX, IDC_LIST_SYMBOLOGY_BCR, m_listSymbology);
	DDX_Control(pDX, IDC_COMBO_READ_MODE_BCR, m_combo_read_mode);
	DDX_Control(pDX, IDC_COMBO_ILLUMINATION_MODE_BCR, m_combo_illumination_mode);
	DDX_Control(pDX, IDC_COMBO_IMAGER_MODE, m_combo_imager_mode);
	DDX_Control(pDX, IDC_COMBO_GOOD_SCAN_BEEP, m_combo_good_scan_beep);
	DDX_Control(pDX, IDC_COMBO_LIGHTING_MODE, m_combo_lighting_mode);
	DDX_Control(pDX, IDC_COMBO_TRIGGER_BCR, m_combo_trigger);
	DDX_Control(pDX, IDC_INPUT_INACTIVITY_BRC, m_edit_timeout);
	DDX_Control(pDX, IDC_EDIT_LENGTH_BCR, m_edit_length);
	DDX_Control(pDX, IDC_EDIT_FREQ_BCR, m_edit_freq);
	DDX_Control(pDX, IDC_COMBO_EAN8_AS_EAN13, m_combo_ean8_as_ean13);
	DDX_Control(pDX, IDC_COMBO_UPCE_AS_UPCA, m_combo_upce_as_upca);
	DDX_Control(pDX, IDC_COMBO_UPCA_AS_EAN13, m_combo_upca_as_ean13);
	DDX_Control(pDX, IDC_BARCODE_VERSION, m_version_barcode);
	DDX_Control(pDX, IDC_CHECK_VOLATILE_PARAM, m_volatile_parameters);
}


BEGIN_MESSAGE_MAP(BARCODE_DIALOG, CDialogEx)
	ON_BN_CLICKED(IDC_OPEN_BCR, &BARCODE_DIALOG::OnBnClickedOpenBcr)
	ON_BN_CLICKED(IDC_CLOSE_BCR, &BARCODE_DIALOG::OnBnClickedCloseBcr)
	ON_BN_CLICKED(IDQUIT, &BARCODE_DIALOG::OnBnClickedQuit)
	ON_BN_CLICKED(IDC_READ_MODE_BCR, &BARCODE_DIALOG::OnBnClickedReadModeBcr)
	ON_BN_CLICKED(IDC_GOOD_BEEP, &BARCODE_DIALOG::OnBnClickedGoodBeep)
	ON_BN_CLICKED(IDC_TRIGGER_BCR, &BARCODE_DIALOG::OnBnClickedTriggerBcr)
	ON_BN_CLICKED(IDC_IMAGER_MODE_BCR, &BARCODE_DIALOG::OnBnClickedImagerModeBcr)
	ON_BN_CLICKED(IDC_LIGHTING_GOAL_BCR, &BARCODE_DIALOG::OnBnClickedLightingGoalBcr)
	ON_BN_CLICKED(IDC_LIGHTING_MODE_BCR, &BARCODE_DIALOG::OnBnClickedLightingModeBcr)
	ON_BN_CLICKED(IDC_ILLUMINATION_LEVEL_BCR, &BARCODE_DIALOG::OnBnClickedIlluminationLevelBcr)
	ON_BN_CLICKED(IDC_ILLUMINATION_MODE_BCR, &BARCODE_DIALOG::OnBnClickedIlluminationModeBcr)
	ON_BN_CLICKED(IDC_BUTTON_RESET_SOFT_BCR, &BARCODE_DIALOG::OnBnClickedButtonResetSoftBcr)
	ON_BN_CLICKED(IDC_GET_FIRMWARE_VERSION, &BARCODE_DIALOG::OnBnClickedGetFirmwareVersion)
	ON_BN_CLICKED(IDC_ENABLE_SYMBOLOGY_BCR, &BARCODE_DIALOG::OnBnClickedEnableSymbologyBcr)
	ON_BN_CLICKED(IDC_DISABLE_SYMBOLOGY_BCR, &BARCODE_DIALOG::OnBnClickedDisableSymbologyBcr)
	ON_BN_CLICKED(IDC_START_SCAN_BCR, &BARCODE_DIALOG::OnBnClickedStartScanBcr)
	ON_BN_CLICKED(IDC_STOP_SCAN_BCR, &BARCODE_DIALOG::OnBnClickedStopScanBcr)
	ON_BN_CLICKED(IDC_BEEP_LENGTH_FREQ_BCR, &BARCODE_DIALOG::OnBnClickedBeepLengthFreqBcr)
	ON_BN_CLICKED(IDC_CONVERT_SYMB_TO_STRING, &BARCODE_DIALOG::OnBnClickedConvertSymbToString)
	/*ON_BN_CLICKED(IDC_UPCA_AS_EAN13, &BARCODE_DIALOG::OnBnClickedUpcaAsEan13)
	ON_BN_CLICKED(IDC_UPCE_AS_UPCA, &BARCODE_DIALOG::OnBnClickedUpceAsUpca)
	ON_BN_CLICKED(IDC_EAN8_AS_EAN13, &BARCODE_DIALOG::OnBnClickedEan8AsEan13)*/
	ON_BN_CLICKED(IDC_APPLY_VERSION, &BARCODE_DIALOG::OnBnClickedApplyVersion)
	ON_BN_CLICKED(IDC_CHECK_VOLATILE_PARAM, &BARCODE_DIALOG::OnBnClickedCheckVolatileParam)
END_MESSAGE_MAP()

//list of barcode symbology
static const struct apptest_symbologies {
    eICBarCodeSymbologies id;
    const wchar_t *name;
} list_symbologies[] = {
    {ICBarCode_AllSymbologies,L"AllSymbologies"},  
	{ICBarCode_EAN13,L"EAN13"},                    
	{ICBarCode_EAN8,L"EAN8"},                     
	{ICBarCode_UPCA,L"UPCA"},                     
	{ICBarCode_UPCE,L"UPCE"},                     
	{ICBarCode_EAN13_2,L"EAN13_2"},                  
	{ICBarCode_EAN8_2,L"EAN8_2"},                   
	{ICBarCode_UPCA_2,L"UPCA_2"},                   
	{ICBarCode_UPCE_2,L"UPCE_2"},                   
	{ICBarCode_EAN13_5,L"EAN13_5"},                  
	{ICBarCode_EAN8_5,L"EAN8_5"},                   
	{ICBarCode_UPCA_5,L"UPCA_5"},                   
	{ICBarCode_UPCE_5,L"UPCE_5"},                   
	{ICBarCode_Code39,L"Code39"},                   
	{ICBarCode_Interleaved2of5,L"Interleaved2of5"},     
	{ICBarCode_Standard2of5,L"Standard2of5"},             
	{ICBarCode_Matrix2of5,L"Matrix2of5"},               
	{ICBarCode_CodaBar,L"CodaBar"},             
	//{ICBarCode_AmesCode,L"AmesCode"},                 
	{ICBarCode_MSI,L"MSI"},                      
	{ICBarCode_Plessey,L"Plessey"},                  
	{ICBarCode_Code128,L"Code128"},                  
	//{ICBarCode_Code16K,L"Code16K"},                  
	{ICBarCode_93,L"Code93"},                       
	{ICBarCode_11,L"Code11"},                       
	{ICBarCode_Telepen,L"Telepen"},                  
	//{ICBarCode_Code49,L"Code49"},                   
	{ICBarCode_Code39_ItalianCPI,L"Code39_ItalianCPI"},        
	{ICBarCode_CodaBlockA,L"CodaBlockA"},               
	{ICBarCode_CodaBlockF,L"CodaBlockF"},               
	{ICBarCode_PDF417,L"PDF417"},              
	{ICBarCode_GS1_128,L"GS1_128"},
	{ICBarCode_ISBT128,L"ISBT128"},                  
	{ICBarCode_MicroPDF,L"MicroPDF"},                 
	{ICBarCode_GS1_DataBarOmni,L"GS1_DataBarOmni"},          
	{ICBarCode_GS1_DataBarLimited,L"GS1_DataBarLimited"},       
	{ICBarCode_GS1_DataBarExpanded,L"GS1_DataBarExpanded"},      
	{ICBarCode_DataMatrix,L"DataMatrix"},               
	{ICBarCode_QRCode,L"QRCode"},                   
	{ICBarCode_Maxicode,L"Maxicode"},                 
	{ICBarCode_UPCE1,L"UPCE1"},                    
	{ICBarCode_Aztec,L"Aztec"}
};

// Gestionnaires de messages de OpenBcrDialog
BOOL BARCODE_DIALOG::OnInitDialog()
{
	CDialog::OnInitDialog();
	char version[2] = {0};
	wchar_t versionW[5] = {0};
	BOOL ret = FALSE;

	//init combobox read mode
	m_combo_read_mode.AddString(L"single scan");
	m_combo_read_mode.AddString(L"multiple scan");
	m_combo_read_mode.SetCurSel(0);

	//init combobox read mode
	m_combo_illumination_mode.AddString(L"aimer and illumination leds");
	m_combo_illumination_mode.AddString(L"aimer only");            
	m_combo_illumination_mode.AddString(L"illumination leds only");
	m_combo_illumination_mode.AddString(L"no illumination");
	m_combo_illumination_mode.SetCurSel(0);

	//init combobox imager mode
	m_combo_imager_mode.AddString(L"1D");
	m_combo_imager_mode.AddString(L"1D 2D");
	m_combo_imager_mode.AddString(L"1D 2D bright");
	m_combo_imager_mode.AddString(L"1D 2D reflective");
	m_combo_imager_mode.SetCurSel(0);

	//init combobox good beep scan
	m_combo_good_scan_beep.AddString(L"Disable Beep");
	m_combo_good_scan_beep.AddString(L"One Beep");
	m_combo_good_scan_beep.AddString(L"Two Beep");
	m_combo_good_scan_beep.SetCurSel(0);

	//init combobox lighting mode
	m_combo_lighting_mode.AddString(L"illumination led priority");
	m_combo_lighting_mode.AddString(L"aperture priority");
	m_combo_lighting_mode.SetCurSel(0);        

	//init combobox trigger
	m_combo_trigger.AddString(L"Disable");
	m_combo_trigger.AddString(L"Enable");
	m_combo_trigger.SetCurSel(0);

	//init combobox TransmitUPCABarcodesAsEAN13
	m_combo_upca_as_ean13.AddString(L"Disable");
	m_combo_upca_as_ean13.AddString(L"Enable");
	m_combo_upca_as_ean13.SetCurSel(0);

	//init combobox TransmitUPCEBarcodesAsUPCA
	m_combo_upce_as_upca.AddString(L"Disable");
	m_combo_upce_as_upca.AddString(L"Enable");
	m_combo_upce_as_upca.SetCurSel(0);

	//init combobox TransmitEAN8BarcodesAsEAN13
	m_combo_ean8_as_ean13.AddString(L"Disable");
	m_combo_ean8_as_ean13.AddString(L"Enable");
	m_combo_ean8_as_ean13.SetCurSel(0);


	// init list symbologie
	LVCOLUMN lvColumn;
	lvColumn.mask    = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 0;
	lvColumn.pszText = TEXT("Value");
	m_listSymbology.InsertColumn(0, &lvColumn);

	lvColumn.cx = 150;
	lvColumn.pszText = TEXT("Text");
	m_listSymbology.InsertColumn(1, &lvColumn);
	m_listSymbology.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	LVITEM lvi = {0};
	wchar_t symbologyIDText[BUFFER_BCR_SIZE];
	int nbTotal = sizeof(list_symbologies) / sizeof(list_symbologies[0]);
	for(int index = 0; index < nbTotal;++index)
	{
		lvi.mask = LVIF_TEXT;
		lvi.iItem = index;		
		swprintf_s(symbologyIDText, L"%d", list_symbologies[index].id);
		lvi.pszText = symbologyIDText;
		int idx = m_listSymbology.InsertItem(&lvi);
		m_listSymbology.SetItemText(idx, 1, list_symbologies[index].name);
	}
	
	m_edit_timeout.SetWindowTextW(L"600");

	if ( !isKeyboardPresent )
	{
		TCHAR  infoBuf[1024];
		infoBuf[0]='\0';
		if ( SHGetSpecialFolderPath(0,infoBuf,CSIDL_PROGRAM_FILES,FALSE) )
		{
			infoBuf[2]='\0';
			PathAppend(infoBuf, TEXT("\\Program Files\\Common Files\\microsoft shared\\ink\\tabtip.exe"));
			ShellExecute(NULL, NULL,infoBuf, NULL, NULL, SW_SHOWNORMAL);
		}
	}
	return TRUE;
}

// Gestionnaires de messages de BARCODE_DIALOG
void CALLBACK BARCODE_DIALOG::BarcodeEvent(char* data, int len)
{
	//Now show BarcodeEventExt data
	/*size_t size;
	BARCODE_DIALOG* myself = (BARCODE_DIALOG*)pBcrDlg;
	myself->textStatus.SetWindowText(_T("Received barcode event"));
	TCHAR* wdata = (TCHAR*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, (len+1)*sizeof(TCHAR));
	if (wdata)
	{
		mbstowcs_s(&size, wdata, len+1, data, len);
		myself->m_comment.SetWindowText(wdata);		
		HeapFree(GetProcessHeap(), 0, wdata);
	}
	else
	{
		myself->m_comment.SetWindowText(L"Memory allocation issue");
	}	*/
}

void CALLBACK BARCODE_DIALOG::BarcodeEventExt(char* data, int len, char* symb, int len_symb)
{
	size_t size;
	BARCODE_DIALOG* myself = (BARCODE_DIALOG*)pBcrDlg;
	char* str_symb = bcrSymbologyToText( bcrCodeUDSIToSymbology(symb));
	myself->textStatus.SetWindowText(_T("Received barcode Ext event"));
	TCHAR* wdata = (TCHAR*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, (strlen(str_symb)+len+21+4)*sizeof(TCHAR));
	if (wdata)
	{
		int pos =9;
		mbstowcs_s(&size, wdata, 10, "Barcode: ", 9);
		mbstowcs_s(&size, wdata+pos, len+1, data, len);
		pos += len;
		mbstowcs_s(&size, wdata+pos, 13, "\nSymbology: ", 12);
		pos += 12;
		mbstowcs_s(&size, wdata+pos, strlen(str_symb)+1, str_symb, strlen(str_symb));		
		myself->m_comment.SetWindowText(wdata);		
		HeapFree(GetProcessHeap(), 0, wdata);
	}
	else
	{
		myself->m_comment.SetWindowText(L"Memory allocation issue");
	}	
}

void CALLBACK BARCODE_DIALOG::BarcodeEventClose()
{
	BARCODE_DIALOG* myself = (BARCODE_DIALOG*)pBcrDlg;
	myself->m_comment.SetWindowText(_T("Barcode close event"));	
}

void BARCODE_DIALOG::OnBnClickedQuit()
{
	EndDialog(0);
}

void BARCODE_DIALOG::OnBnClickedOpenBcr()
{
	BOOL bReturn = FALSE;
	char result;
	UINT inactivityTo=0;
	wchar_t inputBuff[BUFFER_BCR_SIZE];
	char versionStr[50] = {0};
	unsigned char version[2] = {0};

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");
	
	m_edit_timeout.GetWindowText(inputBuff,BUFFER_BCR_SIZE);
	FromString(inputBuff, inactivityTo );

	bReturn = openBarcodeWithInactivityTo(inactivityTo*100 , &result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Open barcode KO"));
	}
	else
	{
		if (result == 0)
		{
			textStatus.SetWindowTextW(_T("Open barcode OK"));
			//get version
			bReturn = bcrGetSettingsVersion((char*)version,sizeof(version));
			if (bReturn == TRUE)
			{
				sprintf_s(versionStr, sizeof(versionStr), "%02X%02X\0", version[0],version[1]);
				m_version_barcode.SetWindowText(CA2W(versionStr));
			}
			else
				m_version_barcode.SetWindowText(L"0000");
		}
		else
			textStatus.SetWindowTextW(_T("Open barcode KO"));
	}


}

void BARCODE_DIALOG::OnBnClickedCloseBcr()
{
	BOOL bReturn;
	char result;

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");

	bReturn = closeBarcode(&result);
	
	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Close barcode KO"));
	}
	else
	{
		if (result == 0)
			textStatus.SetWindowTextW(_T("Close barcode OK"));
		else
			textStatus.SetWindowTextW(_T("Close barcode KO"));
	}
}

void BARCODE_DIALOG::OnBnClickedReadModeBcr()
{
	BOOL bReturn = FALSE;
	char result;
	UINT modeType = 0;
	char buffer[BUFFER_BCR_SIZE];

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");
	modeType = m_combo_read_mode.GetCurSel();

	bReturn = bcrSetReaderMode(modeType , &result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Configure Barcode Reader Mode KO"));
	}
	else
	{
		if (result == 0)
		{
			sprintf_s(buffer,"Configure Barcode Reader Mode OK: %d \r\n",modeType);
			textStatus.SetWindowText(CA2W(buffer));
		}		
		else
		{
			sprintf_s(buffer,"Configure Barcode Reader Mode KO: %d \r\n",modeType);
			textStatus.SetWindowText(CA2W(buffer));
		}
	}
}

void BARCODE_DIALOG::OnBnClickedGoodBeep()
{
	BOOL bReturn = FALSE;
	char result;
	UINT modeType = 0;
	char buffer[BUFFER_BCR_SIZE];

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");
	modeType = m_combo_good_scan_beep.GetCurSel();

	bReturn = bcrSetGoodScanBeep(modeType , &result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Good Scan Beep KO"));
	}
	else
	{
		if (result == 0)
		{
			sprintf_s(buffer,"Good Scan Beep OK: %d \r\n",modeType);
			textStatus.SetWindowText(CA2W(buffer));
		}		
		else
		{
			sprintf_s(buffer,"Good Scan Beep KO: %d \r\n",modeType);
			textStatus.SetWindowText(CA2W(buffer));
		}
	}
}

void BARCODE_DIALOG::OnBnClickedTriggerBcr()
{
	BOOL bReturn = FALSE;
	char result;
	UINT modeType = 0;
	char buffer[BUFFER_BCR_SIZE];

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");
	modeType = m_combo_trigger.GetCurSel();

	bReturn = bcrEnableTrigger(modeType , &result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Enable Trigger KO"));
	}
	else
	{
		if (result == 0)
		{
			sprintf_s(buffer,"Enable Trigger OK: %d \r\n",modeType);
			textStatus.SetWindowText(CA2W(buffer));
		}		
		else
		{
			sprintf_s(buffer,"Enable Trigger KO: %d \r\n",modeType);
			textStatus.SetWindowText(CA2W(buffer));
		}
	}
}

void BARCODE_DIALOG::OnBnClickedImagerModeBcr()
{
	BOOL bReturn = FALSE;
	char result;
	UINT modeType = 0;
	char buffer[BUFFER_BCR_SIZE];

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");
	modeType = m_combo_imager_mode.GetCurSel();

	bReturn = bcrSetImagerMode(modeType , &result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Imager mode KO"));
	}
	else
	{
		if (result == 0)
		{
			sprintf_s(buffer,"Imager mode OK: %d \r\n",modeType);
			textStatus.SetWindowText(CA2W(buffer));
		}		
		else
		{
			sprintf_s(buffer,"Imager mode KO: %d \r\n",modeType);
			textStatus.SetWindowText(CA2W(buffer));
		}
	}
}

void BARCODE_DIALOG::OnBnClickedLightingGoalBcr()
{
	//BOOL bReturn = FALSE;
	//char result;
	//UINT modeType = 0;
	//char buffer[BUFFER_BCR_SIZE];
	//wchar_t inputBuff[BUFFER_BCR_SIZE];

	//textStatus.SetWindowText(_T(""));
	//m_comment.SetWindowText(L"");
	////inputParamBRC.GetWindowText(inputBuff,BUFFER_BCR_SIZE);
	//FromString(inputBuff, modeType );

	//bReturn = apiLightingGoal(modeType , &result);

	//if (bReturn == FALSE)
	//{
	//	textStatus.SetWindowTextW(_T("Lighting goal KO"));
	//}
	//else
	//{
	//	if (result == 0)
	//	{
	//		sprintf_s(buffer,"Lighting goal OK: %d \r\n",modeType);
	//		textStatus.SetWindowText(CA2W(buffer));
	//	}		
	//	else
	//	{
	//		sprintf_s(buffer,"Lighting goal KO: %d \r\n",modeType);
	//		textStatus.SetWindowText(CA2W(buffer));
	//	}
	//}
}

void BARCODE_DIALOG::OnBnClickedLightingModeBcr()
{
	BOOL bReturn = FALSE;
	char result;
	UINT modeType = 0;
	char buffer[BUFFER_BCR_SIZE];

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");
	modeType = m_combo_lighting_mode.GetCurSel();

	bReturn = bcrSetLightingMode(modeType , &result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Lighting mode KO"));
	}
	else
	{
		if (result == 0)
		{
			sprintf_s(buffer,"Lighting mode OK: %d \r\n",modeType);
			textStatus.SetWindowText(CA2W(buffer));
		}		
		else
		{
			sprintf_s(buffer,"Lighting mode KO: %d \r\n",modeType);
			textStatus.SetWindowText(CA2W(buffer));
		}
	}
}

void BARCODE_DIALOG::OnBnClickedIlluminationLevelBcr()
{
	//BOOL bReturn = FALSE;
	//char result;
	//UINT modeType = 0;
	//char buffer[BUFFER_BCR_SIZE];
	//wchar_t inputBuff[BUFFER_BCR_SIZE];

	//textStatus.SetWindowText(_T(""));
	//m_comment.SetWindowText(L"");
	////inputParamBRC.GetWindowText(inputBuff,BUFFER_BCR_SIZE);
	//FromString(inputBuff, modeType );

	//bReturn = apiIlluminationLevel( modeType , &result);

	//if (bReturn == FALSE)
	//{
	//	textStatus.SetWindowTextW(_T("Illumination Level KO"));
	//}
	//else
	//{
	//	if (result == 0)
	//	{
	//		sprintf_s(buffer,"Illumination Level OK: %d \r\n",modeType);
	//		textStatus.SetWindowText(CA2W(buffer));
	//	}		
	//	else
	//	{
	//		sprintf_s(buffer,"Illumination Level KO: %d \r\n",modeType);
	//		textStatus.SetWindowText(CA2W(buffer));
	//	}
	//}
}

void BARCODE_DIALOG::OnBnClickedIlluminationModeBcr()
{
	//BOOL bReturn = FALSE;
	//char result;
	//UINT modeType = 0;
	//char buffer[BUFFER_BCR_SIZE];

	//textStatus.SetWindowText(_T(""));
	//m_comment.SetWindowText(L"");

	//modeType = m_combo_illumination_mode.GetCurSel();

	//bReturn = apiIlluminationMode( modeType , &result);

	//if (bReturn == FALSE)
	//{
	//	textStatus.SetWindowTextW(_T("Illumination Mode KO"));
	//}
	//else
	//{
	//	if (result == 0)
	//	{
	//		sprintf_s(buffer,"Illumination Mode OK: %d \r\n",modeType);
	//		textStatus.SetWindowText(CA2W(buffer));
	//	}		
	//	else
	//	{
	//		sprintf_s(buffer,"Illumination Mode KO: %d \r\n",modeType);
	//		textStatus.SetWindowText(CA2W(buffer));
	//	}
	//}
}

void BARCODE_DIALOG::OnBnClickedButtonResetSoftBcr()
{
	BOOL bReturn = FALSE;
	char result;
	char buffer[BUFFER_BCR_SIZE];

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");

	bReturn = bcrSoftReset(&result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Reset KO"));
	}
	else
	{
		if (result == 0)
		{
			sprintf_s(buffer,"Reset OK \r\n");
			textStatus.SetWindowText(CA2W(buffer));
		}		
		else
		{
			sprintf_s(buffer,"Reset KO \r\n");
			textStatus.SetWindowText(CA2W(buffer));
		}
	}
}

void BARCODE_DIALOG::OnBnClickedGetFirmwareVersion()
{
	BOOL bReturn = FALSE;
	char buffer[BUFFER_BCR_SIZE];
	wchar_t wdata[BUFFER_BCR_SIZE];

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");

	bReturn = bcrGetFirmwareVersion(buffer, BUFFER_BCR_SIZE);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Get firmware BCR version KO"));
	}
	else
	{
		size_t size;
		textStatus.SetWindowText(L"Get firmware BCR version OK \r\n");
		mbstowcs_s(&size, wdata, strlen(buffer)+1, buffer, strlen(buffer));
		m_comment.SetWindowText(wdata);
	}	
}

void BARCODE_DIALOG::OnBnClickedEnableSymbologyBcr()
{
	BOOL bReturn = FALSE;
	char result;
	char buffer[BUFFER_BCR_SIZE];
	int *listSymbol;

	int countSelected = m_listSymbology.GetSelectedCount();
	listSymbol = new int[countSelected];
	POSITION pos = m_listSymbology.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int i = 0;
		while (pos)
		{
			int nItem = m_listSymbology.GetNextSelectedItem(pos);
			// this Specific treatment for "AllSymbologies" is include in PCLServie library now.
			//if (nItem == 0)
			//{
			//	delete listSymbol;
			//	countSelected = (sizeof(list_symbologies) / sizeof(list_symbologies[0]))-1;
			//	listSymbol = new int[countSelected];
			//	for (i=0; i<countSelected; i++)
			//	{
			//		listSymbol[i] = list_symbologies[i+1].id;
			//	}
			//	break;
			//}
			listSymbol[i] = list_symbologies[nItem].id;
			++i;
		}

		textStatus.SetWindowText(_T(""));
		m_comment.SetWindowText(L"");

		bReturn = bcrEnableSymbologies(listSymbol,countSelected,&result);

		if (bReturn == FALSE)
		{
			sprintf_s(buffer,"Enable Symbologies KO %d\r\n",countSelected);
			textStatus.SetWindowText(CA2W(buffer));
		}
		else
		{
			if (result == 0)
			{
				sprintf_s(buffer,"Enable Symbologies OK %d\r\n",countSelected);
				textStatus.SetWindowText(CA2W(buffer));
			}		
			else
			{
				sprintf_s(buffer,"Enable Symbologies KO %d\r\n",countSelected);
				textStatus.SetWindowText(CA2W(buffer));
			}
		}
	}
	else
	{
		MessageBox(L"Select one or more symbology item");
	}
}

void BARCODE_DIALOG::OnBnClickedDisableSymbologyBcr()
{
	BOOL bReturn = FALSE;
	char result;
	char buffer[BUFFER_BCR_SIZE];
	int *listSymbol;

	int countSelected = m_listSymbology.GetSelectedCount();
	listSymbol = new int[countSelected];
	POSITION pos = m_listSymbology.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int i = 0;
		while (pos)
		{
			int nItem = m_listSymbology.GetNextSelectedItem(pos);
			listSymbol[i] = list_symbologies[nItem].id;
			++i;
		}

		textStatus.SetWindowText(_T(""));
		m_comment.SetWindowText(L"");

		bReturn = bcrDisableSymbologies(listSymbol,countSelected,&result);

		if (bReturn == FALSE)
		{
			sprintf_s(buffer,"Disable Symbologies KO %d\r\n",countSelected);
			textStatus.SetWindowText(CA2W(buffer));
		}
		else
		{
			if (result == 0)
			{
				sprintf_s(buffer,"Disable Symbologies OK %d\r\n",countSelected);
				textStatus.SetWindowText(CA2W(buffer));
			}		
			else
			{
				sprintf_s(buffer,"Disable Symbologies KO %d\r\n",countSelected);
				textStatus.SetWindowText(CA2W(buffer));
			}
		}
	}
	else
	{
		MessageBox(L"Select one or more symbology item");
	}
}

void BARCODE_DIALOG::OnBnClickedConvertSymbToString()
{
	wchar_t wdata[BUFFER_BCR_SIZE];
	m_comment.SetWindowText(L"");
	textStatus.SetWindowTextW(_T(""));

	POSITION pos = m_listSymbology.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int nItem = m_listSymbology.GetNextSelectedItem(pos);
		char* textSymb = bcrSymbologyToText(list_symbologies[nItem].id);
		size_t size;		
		mbstowcs_s(&size, wdata, strlen(textSymb)+1, textSymb, strlen(textSymb));
		m_comment.SetWindowText(wdata);
		textStatus.SetWindowTextW(_T("Symbology to text OK"));
	}
	else
	{
		textStatus.SetWindowTextW(_T("Symbology to text KO"));
		m_comment.SetWindowText(L"Select one symbology item");
	}
}

void BARCODE_DIALOG::OnBnClickedStartScanBcr()
{
	BOOL bReturn = FALSE;
	char result;

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");	

	bReturn = bcrStartScan(&result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Start Scan KO"));
	}
	else
	{
		if (result == 0)
			textStatus.SetWindowTextW(_T("Start Scan OK"));
		else
			textStatus.SetWindowTextW(_T("Start Scan KO"));
	}
}

void BARCODE_DIALOG::OnBnClickedStopScanBcr()
{
	BOOL bReturn = FALSE;
	char result;

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");	

	bReturn = bcrStopScan(&result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Start Scan KO"));
	}
	else
	{
		if (result == 0)
			textStatus.SetWindowTextW(_T("Start Scan OK"));
		else
			textStatus.SetWindowTextW(_T("Start Scan KO"));
	}
}

void BARCODE_DIALOG::OnBnClickedBeepLengthFreqBcr()
{
	BOOL bReturn = FALSE;
	char result;
	UINT freq = 0;
	UINT length = 0;
	char buffer[BUFFER_BCR_SIZE];
	wchar_t inputBuff[BUFFER_BCR_SIZE];

	textStatus.SetWindowText(_T(""));
	m_comment.SetWindowText(L"");
	m_edit_length.GetWindowText(inputBuff,BUFFER_BCR_SIZE);
	FromString(inputBuff, length );

	m_edit_freq.GetWindowText(inputBuff,BUFFER_BCR_SIZE);
	FromString(inputBuff, freq );

	bReturn = bcrSetBeep(freq, length , &result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Beep Length KO"));
	}
	else
	{
		if (result == 0)
		{
			sprintf_s(buffer,"Beep Length OK: %d, %d \r\n",freq, length );
			textStatus.SetWindowText(CA2W(buffer));
		}		
		else
		{
			sprintf_s(buffer,"Beep Length KO: %d, %d \r\n",freq, length );
			textStatus.SetWindowText(CA2W(buffer));
		}
	}
}

void BARCODE_DIALOG::OnBnClickedCheckVolatileParam()
{
	BOOL bReturn = FALSE;
	char result;
	char buffer[BUFFER_BCR_SIZE];


	bReturn = bcrSetNonVolatileMode(m_volatile_parameters.GetCheck(),&result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Set VolatileMode KO"));
	}
	else
	{
		if (result == 0)
		{
			sprintf_s(buffer,"VolatileMode OK: %d \r\n",result);
			textStatus.SetWindowText(CA2W(buffer));
		}		
		else
		{
			sprintf_s(buffer,"VolatileMode KO: %d \r\n",result);
			textStatus.SetWindowText(CA2W(buffer));
		}
	}
}

static char DecToByte(const char a,const char b)
{
	char ans='0';
	if (a >= '0' && a <= '9')
		ans = (a - '0') * 16;

	if (b >= '0' && b <= '9')
		ans += (b - '0');

	return ans;
}

void BARCODE_DIALOG::OnBnClickedApplyVersion()
{
	BOOL bReturn = FALSE;
	char result;
	char buffer[BUFFER_BCR_SIZE] = {0};
	wchar_t inputBuffW[5] = {0};
	char inputBuff[6] = {0};
	char version[2] = {0};
	size_t i;

	m_version_barcode.GetWindowText(inputBuffW, sizeof(inputBuffW));

	wcstombs_s(&i, inputBuff, sizeof(inputBuff), inputBuffW, sizeof(inputBuffW));
	version[0] = DecToByte(inputBuff[0],inputBuff[1]);
	version[1] = DecToByte(inputBuff[2],inputBuff[3]);

	bReturn = bcrSetSettingsVersion(version, sizeof(version),&result);

	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Set Version KO"));
	}
	else
	{
		if (result == 0)
		{
			sprintf_s(buffer,"Set Version OK: %d \r\n",result);
			textStatus.SetWindowText(CA2W(buffer));
		}		
		else
		{
			sprintf_s(buffer,"Set Version KO: %d \r\n",result);
			textStatus.SetWindowText(CA2W(buffer));
		}
	}
}

//void BARCODE_DIALOG::OnBnClickedUpcaAsEan13()
//{
//	BOOL bReturn = FALSE;
//	char result;
//	UINT modeType = 0;
//	char buffer[BUFFER_BCR_SIZE];
//
//	textStatus.SetWindowText(_T(""));
//	m_comment.SetWindowText(L"");
//	modeType = m_combo_upca_as_ean13.GetCurSel();
//
//	bReturn = apiEnableTransmitUPCABarcodesAsEAN13(modeType , &result);
//
//	if (bReturn == FALSE)
//	{
//		textStatus.SetWindowTextW(_T("TransmitUPCABarcodesAsEAN13 KO"));
//	}
//	else
//	{
//		if (result == 0)
//		{
//			sprintf_s(buffer,"TransmitUPCABarcodesAsEAN13 OK: %c \r\n",result);
//			textStatus.SetWindowText(CA2W(buffer));
//		}		
//		else
//		{
//			sprintf_s(buffer,"TransmitUPCABarcodesAsEAN13 KO: %c \r\n",result);
//			textStatus.SetWindowText(CA2W(buffer));
//		}
//	}
//}
//
//void BARCODE_DIALOG::OnBnClickedUpceAsUpca()
//{
//		BOOL bReturn = FALSE;
//	char result;
//	UINT modeType = 0;
//	char buffer[BUFFER_BCR_SIZE];
//
//	textStatus.SetWindowText(_T(""));
//	m_comment.SetWindowText(L"");
//	modeType = m_combo_upce_as_upca.GetCurSel();
//
//	bReturn = apiEnableTransmitUPCEBarcodesAsUPCA(modeType , &result);
//
//	if (bReturn == FALSE)
//	{
//		textStatus.SetWindowTextW(_T("TransmitUPCEBarcodesAsUPCA KO"));
//	}
//	else
//	{
//		if (result == 0)
//		{
//			sprintf_s(buffer,"TransmitUPCEBarcodesAsUPCA OK: %c \r\n",result);
//			textStatus.SetWindowText(CA2W(buffer));
//		}		
//		else
//		{
//			sprintf_s(buffer,"TransmitUPCEBarcodesAsUPCA KO: %c \r\n",result);
//			textStatus.SetWindowText(CA2W(buffer));
//		}
//	}
//}
//
//void BARCODE_DIALOG::OnBnClickedEan8AsEan13()
//{
//	BOOL bReturn = FALSE;
//	char result;
//	UINT modeType = 0;
//	char buffer[BUFFER_BCR_SIZE];
//
//	textStatus.SetWindowText(_T(""));
//	m_comment.SetWindowText(L"");
//	modeType = m_combo_ean8_as_ean13.GetCurSel();
//
//	bReturn = apiEnableTransmitEAN8BarcodesAsEAN13(modeType , &result);
//
//	if (bReturn == FALSE)
//	{
//		textStatus.SetWindowTextW(_T("TransmitEAN8BarcodesAsEAN13 KO"));
//	}
//	else
//	{
//		if (result == 0)
//		{
//			sprintf_s(buffer,"TransmitEAN8BarcodesAsEAN13 OK: %c \r\n",result);
//			textStatus.SetWindowText(CA2W(buffer));
//		}		
//		else
//		{
//			sprintf_s(buffer,"TransmitEAN8BarcodesAsEAN13 KO: %c \r\n",result);
//			textStatus.SetWindowText(CA2W(buffer));
//		}
//	}
//}


