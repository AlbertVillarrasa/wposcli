﻿// UNIT_TEST.cpp : fichier d'implémentation
//

#include "stdafx.h"
#include "PclTestApp.h"
#include "afxdialogex.h"
#include "pdautil.h"
#ifdef ENABLE_EPAS
#include "EpasR.h"
#endif
#include "UNIT_TEST.h"
#include "TRANSACTION.h"
#include "INPUT_SIMUL.h"
#include "SHORTCUT.h"
#include "LOGONAME.h"
#include "NETWORK.h"
#include "BARCODE_DIALOG.h"
#include "TMS_DIALOG.h"
#include "BacklightDialog.h"
#include "PrinterFontDialog.h"
#include "InputMessage.h"

#include <windows.h>
#include <string.h>
#include <iostream>
#include <vector>

#define EXT_BUFFER_SIZE 8000
#define BUFFER_SIZE 256
using namespace std;

extern BOOL breturn_Transaction;
extern ITransactionOut* iTransOut;
extern transactionOut_t iTransOutStruct;
extern char charShorcut[BUFFER_SIZE]; 
extern char globLogoName[BUFFER_SIZE];
extern char gSendMsg[1024];
extern unsigned char  globBufferOut[8000];
extern unsigned long globBufferOutSize;

//Global variable
BOOL isTransactionEx;
extern BOOL bReturnShortCutApi;
extern int g_lock;
extern eFonts g_font;


BOOL isKeyboardPresent;
HWND g_hWndStaticConnState;
void* pUnitTestDlg = NULL;
HANDLE g_hThreadEvent;

// UNIT_TEST dialog box

IMPLEMENT_DYNAMIC(UNIT_TEST, CDialog)

UNIT_TEST::UNIT_TEST(CWnd* pParent /*=NULL*/)
	: CDialog(UNIT_TEST::IDD, pParent)
{
	pUnitTestDlg = this;
	AfxInitRichEdit2();
}

UNIT_TEST::~UNIT_TEST()
{
}

void UNIT_TEST::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDITSTATUS, textStatus);
	DDX_Control(pDX, IDC_STATIC_CONNSTATE, m_staticConnState);
	DDX_Control(pDX, IDC_RICHEDIT_COMMENTS, textComments);
	DDX_Control(pDX, IDC_CHECK_LOGENABLED, checkLogEnabled);
}


BEGIN_MESSAGE_MAP(UNIT_TEST, CDialog)
	ON_BN_CLICKED(IDC_BUTT_GOBACK, &UNIT_TEST::OnBnClickedButtGoback)
	ON_BN_CLICKED(IDC_BUTT_START, &UNIT_TEST::OnBnClickedButtStart)
	ON_BN_CLICKED(IDC_BUTT_STOP, &UNIT_TEST::OnBnClickedButtStop)
	ON_BN_CLICKED(IDC_BUTT_COMPINFO, &UNIT_TEST::OnBnClickedButtCompinfo)
	ON_BN_CLICKED(IDC_BUTT_SETTIME, &UNIT_TEST::OnBnClickedButtSettime)
	ON_BN_CLICKED(IDC_BUTT_GETTIME, &UNIT_TEST::OnBnClickedButtGettime)
	ON_BN_CLICKED(IDC_BUTT_GETINFOS, &UNIT_TEST::OnBnClickedButtGetinfos)
	ON_BN_CLICKED(IDC_BUTT_PRINTTEXT, &UNIT_TEST::OnBnClickedButtPrinttext)
	ON_BN_CLICKED(IDC_BUTT_UPDATE, &UNIT_TEST::OnBnClickedButtUpdate)
	ON_BN_CLICKED(IDC_BUTT_PRINTBITMAP, &UNIT_TEST::OnBnClickedButtPrintbitmap)
	ON_BN_CLICKED(IDC_BUTT_RESET, &UNIT_TEST::OnBnClickedButtReset)
	ON_BN_CLICKED(IDC_BUTT_SENDMSG, &UNIT_TEST::OnBnClickedButtSendmsg)
	ON_BN_CLICKED(IDC_BUTT_RECEIVEMSG, &UNIT_TEST::OnBnClickedButtReceivemsg)
	ON_BN_CLICKED(IDC_BUTT_FLUSHMSG, &UNIT_TEST::OnBnClickedButtFlushmsg)
	ON_BN_CLICKED(IDC_BUTT_SHORTCUT, &UNIT_TEST::OnBnClickedButtShortcut)
	ON_BN_CLICKED(IDC_BUTT_INPUTSIMUL, &UNIT_TEST::OnBnClickedButtInputsimul)
	ON_BN_CLICKED(IDC_BUTT_DOTRANSACTION, &UNIT_TEST::OnBnClickedButtDotransaction)
	ON_BN_CLICKED(IDC_BUTT_DOTRANSACTIONEX, &UNIT_TEST::OnBnClickedButtDotransactionex)
	ON_BN_CLICKED(IDC_BUTT_STORELOGO, &UNIT_TEST::OnBnClickedButtStorelogo)
	ON_BN_CLICKED(IDC_BUTT_PRINTLOGO, &UNIT_TEST::OnBnClickedButtPrintlogo)
	ON_BN_CLICKED(IDC_BUTT_PRINTERSTATUS, &UNIT_TEST::OnBnClickedButtPrinterstatus)
	ON_BN_CLICKED(IDC_BUTT_NETWORK, &UNIT_TEST::OnBnClickedButtNetwork)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTT_GETFULLSN, &UNIT_TEST::OnBnClickedButtGetfullsn)
	ON_BN_CLICKED(IDC_CHECK_LOGENABLED, &UNIT_TEST::OnBnClickedCheckLogenabled)
	ON_BN_CLICKED(IDC_BUTT_GETSPMCIVERSION, &UNIT_TEST::OnClickedButtGetSpmciVersion)
	ON_BN_CLICKED(IDC_BUTT_BCR, &UNIT_TEST::OnBnClickedButtBcr)
	ON_BN_CLICKED(IDC_TMS_PARAM, &UNIT_TEST::OnBnClickedInitTmsParam)
#ifdef ENABLE_EPAS
	ON_BN_CLICKED(IDC_BUTT_EPASLOGIN, &UNIT_TEST::OnBnClickedButtEpaslogin)
	ON_BN_CLICKED(IDC_BUTT_EPASPAYMENT, &UNIT_TEST::OnBnClickedButtEpaspayment)
	ON_BN_CLICKED(IDC_BUTT_EPASLOGOUT, &UNIT_TEST::OnBnClickedButtEpaslogout)
	ON_BN_CLICKED(IDC_BUTT_EPASDISCONNECT, &UNIT_TEST::OnBnClickedButtEpasdisconnect)
	ON_BN_CLICKED(IDC_BUTT_EPASCONNECT, &UNIT_TEST::OnBnClickedButtEpasconnect)
#endif
	ON_BN_CLICKED(IDC_BUTT_BACKLIGHT, &UNIT_TEST::OnBnClickedButtBacklight)
	ON_BN_CLICKED(IDC_BUTT_PRINTERFONT, &UNIT_TEST::OnBnClickedButtPrinterfont)
	ON_BN_CLICKED(IDC_BUTT_GETBATLEVEL, &UNIT_TEST::OnBnClickedButtGetbatlevel)
	ON_MESSAGE(WM_POWERBROADCAST, OnPowerMsgRcvd)
	ON_BN_CLICKED(IDC_OPEN_CASH_DRAWER_BTN, &UNIT_TEST::OnBnClickedOpenCashDrawerBtn)
END_MESSAGE_MAP()


// Manage hibernate PC windows
LRESULT UNIT_TEST::OnPowerMsgRcvd(WPARAM wParam, LPARAM lParam)
{
    switch (wParam) {
        case PBT_APMPOWERSTATUSCHANGE:
            break;
        case PBT_APMRESUMEAUTOMATIC:
			startPclService();
            break;
        case PBT_APMRESUMESUSPEND:
			startPclService();
            break;
        case PBT_APMSUSPEND:
			stopPclService();
            break;
    }

    return 0;
}

void UNIT_TEST::OnBnClickedButtGoback()
{
	EndDialog(0);
}


void UNIT_TEST::OnBnClickedButtStart()
{
	textStatus.SetWindowText(_T("Starting PCL service..."));
	textComments.SetWindowText(_T(""));
	if(startPclService())
	{
		textStatus.SetWindowText(_T("PCL Service started"));
	}
	else
	{
		char buffer[256];
		textStatus.SetWindowText(_T("PCL Service not started"));
		sprintf_s(buffer,"Error=%d",GetLastError());
		textComments.SetWindowTextW(CA2W(buffer));
	}
}


void UNIT_TEST::OnBnClickedButtStop()
{
	textStatus.SetWindowText(_T("Stopping PCL service..."));
	textComments.SetWindowText(_T(""));
	if(stopPclService())
	{
		textStatus.SetWindowText(_T("PCL Service stopped"));
	}
	else
	{
		char buffer[256];
		textStatus.SetWindowText(_T("PCL Service not stopped"));
		sprintf_s(buffer,"Error=%d",GetLastError());
		textComments.SetWindowTextW(CA2W(buffer));
	}
}


void UNIT_TEST::OnBnClickedButtCompinfo()
{
	BOOL bReturn;
    FILE * pFile;
    long lSize;

	#define BUFFER_SIZE 256
	char fileCompInfos[BUFFER_SIZE];
	TCHAR  infoBuf[1024];
	char *compInfosBuff;
	size_t i;

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	if (SHGetFolderPath(0, CSIDL_MYDOCUMENTS, NULL, SHGFP_TYPE_CURRENT, infoBuf) == S_OK)
	{
		if (PathAppend(infoBuf, TEXT("Running.lst")) == TRUE)
		{
			wcstombs_s (&i, fileCompInfos, (size_t)BUFFER_SIZE, infoBuf, (size_t)BUFFER_SIZE);
		}
		else
		{
			textStatus.SetWindowText(_T("Comp Info KO (Unable to open running.lst)"));
			return;
		}
	}
	else
	{
		textStatus.SetWindowText(_T("Comp Info KO (Unable to open running.lst)"));
		return;
	}

	bReturn = getTerminalComponents(fileCompInfos);

	if (bReturn == TRUE)
	{
		if (!fopen_s(&pFile, fileCompInfos, "r") && pFile)
		{
			// obtain file size:
			fseek (pFile , 0 , SEEK_END);
			lSize = ftell (pFile);
			fseek (pFile , 0 , SEEK_SET);

			// allocate memory to contain the whole file:
			compInfosBuff = (char*) malloc (sizeof(char)*(lSize+1));
			if (compInfosBuff)
			{
				// copy the file into the buffer:
				size_t length = fread (compInfosBuff,1,lSize,pFile);
				compInfosBuff[length] = 0;
			
				/* the whole file is now loaded in the memory buffer. */
				MessageBox(CA2W(compInfosBuff),0,0);
				textStatus.SetWindowText(_T("Comp Info OK"));
				free(compInfosBuff);
			}
			else
			{
				textStatus.SetWindowText(_T("Comp Info KO (memory allocation issue)"));
			}
			// terminate
			fclose(pFile);
		}
		else
		{
			textStatus.SetWindowText(_T("Comp Info KO (Unable to open running.lst)"));
		}
	}
	else
	{
		textStatus.SetWindowText(_T("Comp Info KO"));
	}

}


void UNIT_TEST::OnBnClickedButtSettime()
{
	BOOL bReturn;
	unsigned long result;

	textStatus.SetWindowTextW(_T(""));
	textComments.SetWindowTextW(_T(""));

	// Set Telium Time
	bReturn = setTerminalTime(&result);
	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Set Time KO"));
	}
	else
	{
		if(result == 0)
		{
			textStatus.SetWindowTextW(_T("Set Time OK"));
		}
		else
		{
			textStatus.SetWindowTextW(_T("Set Time KO"));
		}
	}
}


void UNIT_TEST::OnBnClickedButtGettime()
{
	// Get Telium Time
    BOOL bReturn;
	char buffer[512];
	_SYSTEMTIME TeliumDate;

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
    bReturn = getTerminalTime(&TeliumDate);
	if (bReturn == FALSE)
	{
		textStatus.SetWindowText(_T("Get Time KO"));
	}
	else
	{

		textStatus.SetWindowTextW(_T("Get Time OK"));
		sprintf_s(buffer,"Year: %d\r\nMonth: %d\r\nDay Of Week: %d\r\nDay: %d\r\nHour: %d\r\nMinute: %d\r\nSecond: %d\r\nMillisecond: %d\r\n",
			    TeliumDate.wYear,TeliumDate.wMonth,TeliumDate.wDayOfWeek,TeliumDate.wDay,TeliumDate.wHour,TeliumDate.wMinute,TeliumDate.wSecond,TeliumDate.wMilliseconds);
		USES_CONVERSION;
		textComments.SetWindowTextW(CA2W(buffer));
	}
}


void UNIT_TEST::OnBnClickedButtGetinfos()
{
	// Get Telium terminal information
	BOOL bReturn;
	spmInfo_t iwlInfo;
	char buffer[256];

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	bReturn = getTerminalInfo(&iwlInfo);
	if (bReturn == FALSE)
	{
		textStatus.SetWindowText(_T("Get Info KO"));
	}
	else
	{

		textStatus.SetWindowText(_T("Get Info OK"));
		sprintf_s(buffer,"Serial Number: %x \r\nProduct Number: %x\r\n",iwlInfo.dwSerialNumber,iwlInfo.dwProductNumber);
		textComments.SetWindowText(CA2W(buffer));
	}
}
DWORD WINAPI DoUpdateThread(void* lpParam)
{
	BOOL bReturn;
	unsigned long result;
	UNIT_TEST *p = (UNIT_TEST*)lpParam;
	bReturn = doUpdate(&result);

	if ((bReturn == FALSE) || (result == SPM_UPDATE_KO))
	{
		p->textStatus.SetWindowText(_T("Do Update KO"));
	}
	else
	{
		p->textStatus.SetWindowText(_T("Do Update OK"));
	}
	p->textComments.SetWindowText(_T(""));

	return 0;
}

void UNIT_TEST::OnBnClickedButtUpdate()
{
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T("DoUpdate IN PROGRESS"));
	m_hUpdateThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)DoUpdateThread, this, 0, NULL);	
}

void UNIT_TEST::OnBnClickedButtPrinttext()
{
	BOOL bReturn;
	char pbString[256];
	char result;
	TCHAR szStatus[256];

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	sprintf_s(pbString,"Print realized from Windows\nAccentuated characters:\nàâäéèêëîïôöùûüç\xa4");
	bReturn = openPrinter(&result);
	if(bReturn && result == 0)
	{
		bReturn = printText(pbString, &result);
		if (bReturn == TRUE)
		{
			if (result == 0)
			{
				textStatus.SetWindowTextW(_T("Print Text OK"));
			}
			else
			{
				textStatus.SetWindowTextW(_T("Print Text KO"));
				bReturn = getPrinterStatus(&result);
				if (bReturn == TRUE)
				{
					memset(szStatus, 0, sizeof(szStatus));
					swprintf_s(szStatus, L"Reason: ");
					if (result & 0x10)
					{
						wcscat_s(szStatus, L"PRINTER ERROR; ");
					}
					if (result & 0x20)
					{
						wcscat_s(szStatus, L"PAPER OUT; ");
					}
					if (result & 0x40)
					{
						wcscat_s(szStatus, L"PRINTER NOT CONNECTED; ");
					}
					if (result & 0x80)
					{
						wcscat_s(szStatus, L"PRINTER BATTERY LOW; ");
					}
					if (result & 0x01)
					{
						wcscat_s(szStatus, L"OTHER ERROR; ");
					}
					textComments.SetWindowText(szStatus);
				}
				else
				{
					textComments.SetWindowTextW(_T("Unable to get printer status"));
				}
			}
		}
		else
		{
			textStatus.SetWindowTextW(_T("Print Text KO"));
			textComments.SetWindowText(_T(""));
		}
		bReturn = closePrinter(&result);
	}
	else
	{
		textStatus.SetWindowTextW(_T("Print Text KO"));
		textComments.SetWindowText(_T("Unable to open printer"));
	}
	
}

void UNIT_TEST::OnBnClickedButtPrintbitmap()
{
	BOOL bReturn;
    FILE * pFile;
    unsigned long lSize;
    char * buffer;
	char result;
	OPENFILENAME ofn;
	TCHAR szStatus[256];
	wchar_t fileName[MAX_PATH]=L"";

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = NULL;
	ofn.lpstrFilter = L"Image Files (*.bmp)\0*.*\0";
	ofn.lpstrFile = fileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt;
	
	if ( GetOpenFileName(&ofn) )
	{
		//Read of the Bitmap to print
		if (!_wfopen_s(&pFile, fileName, L"r") && pFile)
		{
			// obtain file size:
			fseek (pFile , 0 , SEEK_END);
			lSize = ftell (pFile);
			fseek (pFile , 0 , SEEK_SET);

			// allocate memory to contain the whole file:
			buffer = (char*) malloc (sizeof(char)*lSize);
			if (buffer)
			{
				// copy the file into the buffer:
				fread (buffer,1,lSize,pFile);
				
				bReturn = openPrinter(&result);
				if(bReturn && result == 0)
				{
					bReturn = printBitmap(buffer,lSize,&result);	
					if (bReturn == TRUE)
					{
						if (result == 0)
						{
							textStatus.SetWindowTextW(_T("Print Bitmap OK"));
						}
						else
						{
							textStatus.SetWindowTextW(_T("Print Bitmap KO"));
							bReturn = getPrinterStatus(&result);
							if (bReturn == TRUE)
							{
								memset(szStatus, 0, sizeof(szStatus));
								swprintf_s(szStatus, L"Reason: ");
								if (result & 0x10)
								{
									wcscat_s(szStatus, L"PRINTER ERROR; ");
								}
								if (result & 0x20)
								{
									wcscat_s(szStatus, L"PAPER OUT; ");
								}
								if (result & 0x40)
								{
									wcscat_s(szStatus, L"PRINTER NOT CONNECTED; ");
								}
								if (result & 0x80)
								{
									wcscat_s(szStatus, L"PRINTER BATTERY LOW; ");
								}
								if (result & 0x01)
								{
									wcscat_s(szStatus, L"OTHER ERROR; ");
								}
								textComments.SetWindowText(szStatus);
							}
							else
							{
								textComments.SetWindowTextW(_T("Unable to get printer status"));
							}
						}
					}
					else
					{
						textStatus.SetWindowTextW(_T("Print Bitmap KO"));
						textComments.SetWindowText(_T(""));
					}
					bReturn = closePrinter(&result); 
				}
				else
				{
					textStatus.SetWindowTextW(_T("Print Bitmap KO (unable to open printer)"));
				}
				free(buffer);
			}
			else
			{
				textStatus.SetWindowTextW(_T("Print Bitmap KO (memory allocation issue)"));
			}
			// terminate
			fclose(pFile);
		}
		else
		{
			textStatus.SetWindowTextW(_T("Print Bitmap KO (unable to open bitmap file)"));
		}
	}
}

void UNIT_TEST::OnBnClickedButtReset()
{
	BOOL bReturn;
	int nResetInfo=0;

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	bReturn = resetTerminal(nResetInfo);
	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Reset Companion not sent"));
	}
	else
	{
		textStatus.SetWindowTextW(_T("Reset Companion sent"));
	}
}


void UNIT_TEST::OnBnClickedButtSendmsg()
{
	// Send message to the terminal
	InputMessage DlgSendMsg;
	BOOL bReturn;
	unsigned long bytesSent;

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));


	DlgSendMsg.DoModal();

	bReturn = sendMessage(gSendMsg, (unsigned long)strlen(gSendMsg), &bytesSent);
	if (bReturn == FALSE)
	{
		textStatus.SetWindowText(_T("Send Msg KO"));
	}
	else
	{
		textStatus.SetWindowText(_T("Send Msg OK"));
	}
}


void UNIT_TEST::OnBnClickedButtReceivemsg()
{
	// Receive a message from the terminal 
	char buffer[1024];
	char message[256];
	BOOL bReturn;
	unsigned long bytesReceived;
	
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	bReturn = receiveMessage(buffer, sizeof(buffer), &bytesReceived);
	if (bReturn == FALSE)
	{
		textStatus.SetWindowText(_T("Receive Msg KO: method failed"));
	}
	else
	{
		if (bytesReceived != 0)
		{
			textStatus.SetWindowText(_T("Receive Msg OK"));
			sprintf_s(message,"Bytes received: %d\r\nMessage: %s\r\n",bytesReceived,buffer);
			textComments.SetWindowText(CA2W(message));
		}
		else
		{
			textStatus.SetWindowText(_T("Receive Msg KO: msg rcv NULL"));
		}	
	}
}


void UNIT_TEST::OnBnClickedButtFlushmsg()
{
	/** Flush Messages received in Mailbox */
	BOOL bReturn;
	bReturn = flushMessages();

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Flush Msg KO"));
	}
	else
	{
		textStatus.SetWindowTextW(_T("Flush Msg OK"));
	}
}


void UNIT_TEST::OnBnClickedButtShortcut()
{
	SHORTCUT DlgShortCut;

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	DlgShortCut.DoModal();
	
	if (bReturnShortCutApi == FALSE)
	{
		textStatus.SetWindowTextW(_T("Send Shorcut KO"));
	}
	else
	{
		textStatus.SetWindowTextW(_T("Send Shorcut OK"));
	}
}


void UNIT_TEST::OnBnClickedButtInputsimul()
{
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	INPUT_SIMUL Dlg_INPUT_SIMUL;
	Dlg_INPUT_SIMUL.DoModal();
}


void UNIT_TEST::OnBnClickedButtDotransaction()
{
	isTransactionEx = FALSE;
	TRANSACTION Dlg_TRANSACTION;
	Dlg_TRANSACTION.DoModal();
	char buffer[1024];
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	if (breturn_Transaction == FALSE)
	{
		textStatus.SetWindowTextW(_T("Transaction FAILED"));
	}
	else
	{
		if(iTransOut == NULL)
		{
			if (!strcmp(iTransOutStruct.c3Error, "0000"))
			{
				textStatus.SetWindowTextW(_T("Transaction OK"));
				sprintf_s(buffer,"Amount: %s \r\nC3 Error: %s", iTransOutStruct.amount, iTransOutStruct.c3Error);
				textComments.SetWindowText(CA2W(buffer));
			}
			else
			{
				textStatus.SetWindowTextW(_T("Transaction KO"));
			}
		}
		else
		{

			char* C3Err = iTransOut->getC3Error();
			char* Amount = iTransOut->getAmount();

			if (!strcmp(C3Err, "0000"))
			{
				textStatus.SetWindowTextW(_T("Transaction OK"));
				sprintf_s(buffer,"Amount: %s \r\nC3 Error: %s", Amount, C3Err);
				textComments.SetWindowText(CA2W(buffer));
			}
			else
			{
				textStatus.SetWindowTextW(_T("Transaction KO"));
			}
		}
	}

	if (iTransOut)
	{
		delete iTransOut;
		iTransOut = NULL;
	}
	
}


void UNIT_TEST::OnBnClickedButtDotransactionex()
{
	ZeroMemory(globBufferOut, globBufferOutSize);
	isTransactionEx = TRUE;
	TRANSACTION Dlg_TRANSACTION;
	Dlg_TRANSACTION.DoModal();
	char buffer[250000]; //globBufferOutSize * 3 + Text
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	if (breturn_Transaction == FALSE)
	{
		textStatus.SetWindowTextW(_T("TransactionEx FAILED"));
	}
	else
	{
		if(iTransOut == NULL)
		{
			if (!strcmp(iTransOutStruct.c3Error, "0000"))
			{
				textStatus.SetWindowTextW(_T("TransactionEx OK"));
				int nbWrite = sprintf_s(buffer,"Amount: %s \r\nC3 Error: %s\nExtended Data (size = %d):\n", iTransOutStruct.amount, iTransOutStruct.c3Error, globBufferOutSize);
				for(unsigned long i = 0; i < globBufferOutSize; i++)
				{
					sprintf_s(&buffer[nbWrite + 3 * i], sizeof(buffer) - (nbWrite + 3 *i),"%02x ", globBufferOut[i]);
				}
				textComments.SetWindowText(CA2W(buffer));
			}
			else
			{
				textStatus.SetWindowTextW(_T("TransactionEx KO"));
			}
		}
		else
		{
			char* C3Err = iTransOut->getC3Error();
			char* Amount = iTransOut->getAmount();

			if (!strcmp(C3Err, "0000"))
			{
				textStatus.SetWindowTextW(_T("TransactionEx OK"));
				int nbWrite = sprintf_s(buffer,"Amount: %s \r\nC3 Error: %s\nExtended Data (size = %d):\n", Amount, C3Err, globBufferOutSize);
				for(unsigned long i = 0; i < globBufferOutSize; i++)
				{
					sprintf_s(&buffer[nbWrite + 3 * i], sizeof(buffer) - (nbWrite + 3 *i),"%02x ", globBufferOut[i]);
				}
				textComments.SetWindowText(CA2W(buffer));
			}
			else
			{
				textStatus.SetWindowTextW(_T("TransactionEx KO"));
			}
		}
	}

	if (iTransOut)
	{
		delete iTransOut;
		iTransOut = NULL;
	}
}


void UNIT_TEST::OnBnClickedButtStorelogo()
{
	LOGONAME DlgLogoName;
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	BOOL bReturn;
    FILE * pFile;
    unsigned long lSize;
    char * buffer;
	char result;
	OPENFILENAME ofn;
	wchar_t fileName[MAX_PATH]=L"";
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = NULL;
	ofn.lpstrFilter = L"Image Files (*.bmp)\0*.*\0";
	ofn.lpstrFile = fileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt;
	
	if ( GetOpenFileName(&ofn) )
	{
		//Read of the Bitmap to print
		if (!_wfopen_s(&pFile, fileName,L"r") && pFile)
		{
			// obtain file size:
			fseek (pFile , 0 , SEEK_END);
			lSize = ftell (pFile);
			fseek (pFile , 0 , SEEK_SET);

			// allocate memory to contain the whole file:
			buffer = (char*) malloc (sizeof(char)*lSize);
			if (buffer)
			{
				// copy the file into the buffer:
				fread (buffer,1,lSize,pFile);

				DlgLogoName.DoModal();

				bReturn = storeLogo(globLogoName, 0, buffer, sizeof(char)*lSize, &result);
				if (bReturn == TRUE && result == 0)
				{
					textStatus.SetWindowTextW(_T("Store LOGO OK"));
				}
				else
				{
					textStatus.SetWindowTextW(_T("Store LOGO KO"));
				}
				free(buffer);
			}
			else
			{
				textStatus.SetWindowTextW(_T("Store LOGO KO (memory allocation issue)"));
			}
			// terminate
			fclose(pFile);
		}
		else
		{
			textStatus.SetWindowTextW(_T("Store LOGO KO (unable to open bitmap file)"));
		}
	}
}


void UNIT_TEST::OnBnClickedButtPrintlogo()
{
	LOGONAME DlgLogoName;
	char result;
	TCHAR szStatus[256];
	BOOL bReturn = FALSE;
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	DlgLogoName.DoModal();
	bReturn = openPrinter(&result);
	if(bReturn && result == 0)
	{
		bReturn = printLogo(globLogoName, &result);
		if (bReturn == TRUE)
		{
			if (result == 0)
			{
				textStatus.SetWindowTextW(_T("Print Logo OK"));
			}
			else
			{
				textStatus.SetWindowTextW(_T("Print Logo KO"));
				bReturn = getPrinterStatus(&result);
				if (bReturn == TRUE)
				{
					memset(szStatus, 0, sizeof(szStatus));
					swprintf_s(szStatus, L"Reason: ");
					if (result & 0x10)
					{
						wcscat_s(szStatus, L"PRINTER ERROR; ");
					}
					if (result & 0x20)
					{
						wcscat_s(szStatus, L"PAPER OUT; ");
					}
					if (result & 0x40)
					{
						wcscat_s(szStatus, L"PRINTER NOT CONNECTED; ");
					}
					if (result & 0x80)
					{
						wcscat_s(szStatus, L"PRINTER BATTERY LOW; ");
					}
					if (result & 0x01)
					{
						wcscat_s(szStatus, L"OTHER ERROR; ");
					}
					textComments.SetWindowText(szStatus);
				}
				else
				{
					textComments.SetWindowTextW(_T("Unable to get printer status"));
				}
			}
		}
		else
		{
			textStatus.SetWindowTextW(_T("Print Logo KO"));
			textComments.SetWindowText(_T(""));
		}
		closePrinter(&result); 
	}
	else
	{
		textStatus.SetWindowTextW(_T("Print Logo KO (unable to open printer)"));
	}
}


void UNIT_TEST::OnBnClickedButtPrinterstatus()
{
	char result;
	TCHAR szStatus[256];
	BOOL bReturn = FALSE;
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	bReturn = openPrinter(&result);
	if(bReturn && result == 0)
	{
		bReturn = getPrinterStatus(&result);
		if (bReturn == TRUE)
		{
			textStatus.SetWindowTextW(_T("Printer Status OK"));
			if (result == 0)
			{
				textComments.SetWindowText(L"NO ERROR");
			}
			else
			{
				memset(szStatus, 0, sizeof(szStatus));
				swprintf_s(szStatus, L"Reason: ");
				if (result & 0x10)
				{
					wcscat_s(szStatus, L"PRINTER ERROR; ");
				}
				if (result & 0x20)
				{
					wcscat_s(szStatus, L"PAPER OUT; ");
				}
				if (result & 0x40)
				{
					wcscat_s(szStatus, L"PRINTER NOT CONNECTED; ");
				}
				if (result & 0x80)
				{
					wcscat_s(szStatus, L"PRINTER BATTERY LOW; ");
				}
				if (result & 0x01)
				{
					wcscat_s(szStatus, L"OTHER ERROR; ");
				}
				textComments.SetWindowText(szStatus);
			}
		}
		else
		{
			textStatus.SetWindowTextW(_T("Printer Status KO"));
			textComments.SetWindowText(_T(""));
		}
		closePrinter(&result); 
	}
	else
	{
		textStatus.SetWindowTextW(_T("Printer Status KO (unable to open printer)"));
	}
}

void UNIT_TEST::OnBnClickedButtPrinterfont()
{
	PrinterFontDialog DlgFont;
	int ret;
	char result;
	unsigned char pbText[256];
	wchar_t wch[256];
	TCHAR szStatus[256];
	BOOL bReturn = FALSE;
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	ret = DlgFont.DoModal();
	if (ret == IDOK)
	{
		memset(wch, 0, sizeof(wch));
		memset(pbText, 0, sizeof(pbText));
		switch(g_font)
		{
		case ISO8859_1:
			wcscpy_s(wch, _countof(wch), L"English: Welcome\n");
			WideCharToMultiByte(28591, 0, wch, -1, (char*)pbText, sizeof(pbText), NULL, NULL);
			break;
		case ISO8859_2:
			// Czech (ISO8859-2)
			//strcpy_s(pbText, _countof(pbText), "Czech: Nechť již hříšné saxofony ďáblů rozzvučí síň úděsnými tóny waltzu, tanga a quickstepu.\nPolish: Pożądany\n");
			wcscpy_s(wch, _countof(wch), L"Czech: Nechť již hříšné saxofony ďáblů rozzvučí síň úděsnými tóny waltzu, tanga a quickstepu.\nPolish: Pożądany\n");
			WideCharToMultiByte(28592, 0, wch, -1, (char*)pbText, sizeof(pbText), NULL, NULL);
			break;
		case ISO8859_3:
			// Turkish (ISO8859-3)
			wcscpy_s(wch, _countof(wch), L"Turkish: Günaydin\n");
			WideCharToMultiByte(28593, 0, wch, -1, (char*)pbText, sizeof(pbText), NULL, NULL);
			break;
		case ISO8859_5:
			// Russian (ISO8859-5)
			wcscpy_s(wch, _countof(wch), L"Russian: ДОБРО ПОЖАЛОВАТЬ\n");
			WideCharToMultiByte(28595, 0, wch, -1, (char*)pbText, sizeof(pbText), NULL, NULL);
			break;
		case ISO8859_6:
			// Arabic (ISO8859-6)
			//wcscpy_s(wch, _countof(wch), L"Arabic: ﺍﻟﺴﻼﻡ ﻋﻠﻴﻜﻢ\n");
			//WideCharToMultiByte(28596, 0, wch, -1, pbText, sizeof(pbText), NULL, NULL);
			pbText[0] = 0xC7;
			pbText[1] = 0xE4;
			pbText[2] = 0xD3;
			pbText[3] = 0xE4;
			pbText[4] = 0xC7;
			pbText[5] = 0xE5;
			pbText[6] = 0x20;
			pbText[7] = 0xD9;
			pbText[8] = 0xE4;
			pbText[9] = 0xEA;
			pbText[10] = 0xE3;
			pbText[11] = 0xE5;
			pbText[12] = '\n';
			break;
		case ISO8859_7:
			// Greek (ISO8859-7)
			wcscpy_s(wch, _countof(wch), L"Greek: καλημέρα\n");
			WideCharToMultiByte(28597, 0, wch, -1, (char*)pbText, sizeof(pbText), NULL, NULL);
			break;
		case ISO8859_15:
			// French (ISO8859-15)
			wcscpy_s(wch, _countof(wch), L"French: Bienvenue €\n");
			WideCharToMultiByte(28605, 0, wch, -1, (char*)pbText, sizeof(pbText), NULL, NULL);
			break;
		}
		
		
		bReturn = openPrinter(&result);
		if(bReturn && result == 0)
		{
			if (g_font == ISO8859_6)
			{
				setPrinterFont(ISO8859_1, &result);
				printText("Arabic:\n", &result);
			}
			bReturn = setPrinterFont(g_font, &result);
			if (bReturn && result == 0)
			{
				bReturn = printText((char*)pbText, &result);
				if (bReturn == TRUE)
				{
					if (result == 0)
					{
						textStatus.SetWindowTextW(_T("Print Text with font OK"));
					}
					else
					{
						textStatus.SetWindowTextW(_T("Print Text with font KO"));
						bReturn = getPrinterStatus(&result);
						if (bReturn == TRUE)
						{
							memset(szStatus, 0, sizeof(szStatus));
							swprintf_s(szStatus, L"Reason: ");
							if (result & 0x10)
							{
								wcscat_s(szStatus, L"PRINTER ERROR; ");
							}
							if (result & 0x20)
							{
								wcscat_s(szStatus, L"PAPER OUT; ");
							}
							if (result & 0x40)
							{
								wcscat_s(szStatus, L"PRINTER NOT CONNECTED; ");
							}
							if (result & 0x80)
							{
								wcscat_s(szStatus, L"PRINTER BATTERY LOW; ");
							}
							if (result & 0x01)
							{
								wcscat_s(szStatus, L"OTHER ERROR; ");
							}
							textComments.SetWindowText(szStatus);
						}
						else
						{
							textComments.SetWindowTextW(_T("Unable to get printer status"));
						}
					}
				}
				else
				{
					textStatus.SetWindowTextW(_T("Print Text with font KO"));
					textComments.SetWindowText(_T(""));
				}
			}
			else
			{
				textStatus.SetWindowTextW(_T("Unable to set font"));
				textComments.SetWindowText(_T(""));
			}
			closePrinter(&result); 
		}
		else
		{
			textStatus.SetWindowTextW(_T("Print Logo KO (unable to open printer)"));
		}
	}
}


void UNIT_TEST::OnBnClickedButtNetwork()
{
	NETWORK Dlg_NETWORK;

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	Dlg_NETWORK.DoModal();
}

DWORD WINAPI GetStateThread(LPVOID lpParam)
{
	char result;
	DWORD dwRet;
	UNIT_TEST* pUnitTest = (UNIT_TEST*)lpParam;
	for (;;)
	{
		if (serverStatus(&result))
		{
			if (result == 1)
				::SendMessage(g_hWndStaticConnState, WM_SETTEXT, NULL, (LPARAM)L"Companion is connected");
			else
				::SendMessage(g_hWndStaticConnState, WM_SETTEXT, NULL, (LPARAM)L"Companion is disconnected");
		}
		else
		{
			::SendMessage(g_hWndStaticConnState, WM_SETTEXT, NULL, (LPARAM)L"Companion is disconnected");
		}
		::UpdateWindow(g_hWndStaticConnState);
		
		dwRet = WaitForSingleObject(g_hThreadEvent, 100);
		if (dwRet == WAIT_OBJECT_0)
		{
			break;
		}
	}

	return 0;
}

int UNIT_TEST::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	PRAWINPUTDEVICELIST pRawInputDeviceList,pcur;
	UINT	NbDevices = 0;
	UINT	NbKeyb = 0;


	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	isKeyboardPresent = FALSE;

	GetRawInputDeviceList(NULL,&NbDevices,sizeof(RAWINPUTDEVICELIST));
	if ( NbDevices )
	{
		pRawInputDeviceList = (PRAWINPUTDEVICELIST)malloc(NbDevices*sizeof(RAWINPUTDEVICELIST));
		pcur = pRawInputDeviceList;
		if ( pRawInputDeviceList )
		{
			UINT i;
			GetRawInputDeviceList(pcur,&NbDevices,sizeof(RAWINPUTDEVICELIST));
			NbKeyb = 0;
			for(i=0;i<NbDevices;i++)
			{
				if ( pcur->dwType == RIM_TYPEKEYBOARD )
					NbKeyb++;
				pcur++;
			}
			free(pRawInputDeviceList);
		}

		if ( NbKeyb > 1 )
			isKeyboardPresent = TRUE;
	}

	return 0;
}

// Callbacks implementation

/*void CALLBACK UNIT_TEST::BarcodeEvent(char* data, int len)
{
	size_t size;
	UNIT_TEST* myself = (UNIT_TEST*)pUnitTestDlg;
	myself->textStatus.SetWindowText(_T("Received barcode event"));
	TCHAR* wdata = (TCHAR*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, (len+1)*sizeof(TCHAR));
	if (wdata)
	{
		mbstowcs_s(&size, wdata, len+1, data, len);
		myself->textComments.SetWindowText(wdata);
		HeapFree(GetProcessHeap(), 0, wdata);
	}
	else
	{
		myself->textComments.SetWindowText(L"Memory allocation issue");
	}
}

void CALLBACK UNIT_TEST::BarcodeEventClose()
{
	UNIT_TEST* myself = (UNIT_TEST*)pUnitTestDlg;
	myself->textStatus.SetWindowText(_T("Barcode close event"));
	myself->textComments.SetWindowText(L"");
}*/

std::wstring ConvertFromUtf8ToUtf16(const std::string& str)
{
    std::wstring convertedString;
    int requiredSize = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, 0, 0);
    if(requiredSize > 0)
    {
        std::vector<wchar_t> buffer(requiredSize);
        MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, &buffer[0], requiredSize);
        convertedString.assign(buffer.begin(), buffer.end() - 1);
    }
 
    return convertedString;
}

void CALLBACK UNIT_TEST::PrintText(char* szText, unsigned char font, unsigned char justification, unsigned char xFactor, unsigned char yFactor, unsigned char underline, unsigned char bold, unsigned char charset)
{
	size_t len = strlen(szText);
	UNIT_TEST* myself = (UNIT_TEST*)pUnitTestDlg;
	
	std::wstring text = ConvertFromUtf8ToUtf16(std::string(szText));
	myself->textStatus.SetWindowText(_T("PrintText"));
	int nBegin = myself->textComments.GetTextLength();
	PARAFORMAT pf;
	memset(&pf, 0, sizeof(PARAFORMAT));
	pf.cbSize = sizeof(PARAFORMAT);
	pf.dwMask = PFM_ALIGNMENT;
	pf.wAlignment = PFA_LEFT;
	switch (justification)
	{
	case 0:
		pf.wAlignment = PFA_CENTER;
		break;
	case 1:
		pf.wAlignment = PFA_RIGHT;
		break;
	case 2:
		pf.wAlignment = PFA_LEFT;
		break;
	}
	myself->textComments.SetParaFormat(pf);
	CHARFORMAT2 cf;
	cf.cbSize = sizeof(CHARFORMAT2);
	memset(&cf, 0, sizeof(CHARFORMAT2));
	DWORD dwMask = myself->textComments.GetDefaultCharFormat(cf);
	LONG yHeight = 0;
	if ((dwMask & CFM_SIZE) == CFM_SIZE)
		yHeight = cf.yHeight;
	memset(&cf, 0, sizeof(CHARFORMAT2));
	cf.cbSize = sizeof(CHARFORMAT2);
	cf.dwMask = CFM_BOLD | CFM_UNDERLINE | CFM_CHARSET | CFM_SIZE;
	cf.dwEffects = 0;
	if (bold)
		cf.dwEffects |= CFE_BOLD;
	if (underline)
		cf.dwEffects |= CFE_UNDERLINE;
	if (yHeight != 0)
	{
		cf.yHeight = yHeight * yFactor;
	}
	cf.bCharSet = charset;
	myself->textComments.SetSelectionCharFormat(cf);
	myself->textComments.SetSel(nBegin, nBegin);		// Select last character
	myself->textComments.ReplaceSel(text.c_str());		// Append, move cursor to end of text
	myself->textComments.SetSel(-1,0);					// Remove Black selection bars
	int nEnd = myself->textComments.GetTextLength();	// Get New Length
	myself->textComments.SetSel(nEnd,nEnd);				// Cursor to End of new text
}

void CALLBACK UNIT_TEST::PrintImage(unsigned long width, unsigned long height, char* image, unsigned long size, unsigned char justification)
{
	UNIT_TEST* myself = (UNIT_TEST*)pUnitTestDlg;
	myself->textStatus.SetWindowText(_T("PrintImage"));
}

void CALLBACK UNIT_TEST::FeedPaper(DWORD dwLines)
{
	UNIT_TEST* myself = (UNIT_TEST*)pUnitTestDlg;
	myself->textStatus.SetWindowText(_T("FeedPaper"));
}
void CALLBACK UNIT_TEST::CutPaper()
{
	UNIT_TEST* myself = (UNIT_TEST*)pUnitTestDlg;
	myself->textStatus.SetWindowText(_T("CutPaper"));
}
int CALLBACK UNIT_TEST::StartReceipt(unsigned char type)
{
	UNIT_TEST* myself = (UNIT_TEST*)pUnitTestDlg;
	myself->textStatus.SetWindowText(_T("StartReceipt"));
	return 0;
}
int CALLBACK UNIT_TEST::EndReceipt()
{
	UNIT_TEST* myself = (UNIT_TEST*)pUnitTestDlg;
	myself->textStatus.SetWindowText(_T("EndReceipt"));
	return 0;
}
void CALLBACK UNIT_TEST::AddSignature()
{
	UNIT_TEST* myself = (UNIT_TEST*)pUnitTestDlg;
	myself->textStatus.SetWindowText(_T("AddSignature"));
}

BOOL UNIT_TEST::OnInitDialog()
{
	CDialog::OnInitDialog();
	GetDlgItem(IDC_STATIC_CONNSTATE, &g_hWndStaticConnState);
#ifdef ENABLE_EPAS
	GetDlgItem(IDC_BUTT_EPASCONNECT)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BUTT_EPASDISCONNECT)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BUTT_EPASLOGIN)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BUTT_EPASLOGOUT)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BUTT_EPASPAYMENT)->ShowWindow(SW_SHOW);
#endif
	g_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_hThread=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)GetStateThread,this,0,NULL);
	CFont font;
	font.CreatePointFont(160, L"Arial");
	m_staticConnState.SetFont(&font);
	if (isDebugLogEnabled())
		checkLogEnabled.SetCheck(1);
	else
		checkLogEnabled.SetCheck(0);
	
	callbacks_t callbacks = {
		(BarcodeEventFunc)&BARCODE_DIALOG::BarcodeEvent,
		NULL,
		(PrintTextFunc)&UNIT_TEST::PrintText,
		(PrintImageFunc)&UNIT_TEST::PrintImage,
		(FeedPaperFunc)&UNIT_TEST::FeedPaper,
		(CutPaperFunc)&UNIT_TEST::CutPaper,
		(StartReceiptFunc)&UNIT_TEST::StartReceipt,
		(EndReceiptFunc)&UNIT_TEST::EndReceipt,
		(AddSignatureFunc)&UNIT_TEST::AddSignature,
		(BarcodeEventCloseFunc)&BARCODE_DIALOG::BarcodeEventClose,
		//(BarcodeEventExtFunc)&BARCODE_DIALOG::BarcodeEventExt
	};
	registerCallbacks(callbacks);

	registerBarcodeEventExtCallback((BarcodeEventExtFunc)&BARCODE_DIALOG::BarcodeEventExt);
	
	return TRUE;
}




void UNIT_TEST::OnDestroy()
{
	CDialog::OnDestroy();

	SetEvent(g_hThreadEvent);
	WaitForSingleObject(m_hThread, INFINITE);
	CloseHandle(m_hThread);
	CloseHandle(g_hThreadEvent);
}



void UNIT_TEST::OnBnClickedButtGetfullsn()
{
	BOOL bReturn;
	char fullSN[26];
	char buffer[256];

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	bReturn = getFullSerialNumber(fullSN, sizeof(fullSN));
	
	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Get full SN KO"));
	}
	else
	{
		textStatus.SetWindowText(_T("Get full SN OK"));
		sprintf_s(buffer,"Full Serial Number: %s \r\n",fullSN);
		textComments.SetWindowText(CA2W(buffer));
	}
}


void UNIT_TEST::OnBnClickedCheckLogenabled()
{
	if (checkLogEnabled.GetCheck())
	{
		enableDebugLog(true);
	}
	else
	{
		enableDebugLog(false);
	}
}


void UNIT_TEST::OnClickedButtGetSpmciVersion()
{
	BOOL bReturn;
	char spmciVersion[5];
	char buffer[256];

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	bReturn = getSPMCIVersion(spmciVersion, sizeof(spmciVersion));
	
	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Get SPMCI version KO"));
	}
	else
	{
		textStatus.SetWindowText(_T("Get SPMCI version OK"));
		sprintf_s(buffer,"SPMCI Version: %s \r\n",spmciVersion);
		textComments.SetWindowText(CA2W(buffer));
	}
}

void UNIT_TEST::OnBnClickedButtBcr()
{
	BARCODE_DIALOG DlgBCR;

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	DlgBCR.DoModal();
}


void UNIT_TEST::OnBnClickedInitTmsParam()
{
	TMS_DIALOG DlgBCR;

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	DlgBCR.DoModal();
}

void UNIT_TEST::OnBnClickedButtBacklight()
{
	BacklightDialog DlgBacklight;
	int ret;
	char result;
	BOOL bRet;

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	ret = DlgBacklight.DoModal();

	if (ret == IDOK)
	{
		bRet = setBacklightLock(g_lock, &result);
		if (bRet == FALSE)
		{
			textStatus.SetWindowTextW(_T("Backlight control KO"));
		}
		else
		{
			if (result == 0)
			{
				textStatus.SetWindowTextW(_T("Backlight control OK"));
			}
			else
			{
				textStatus.SetWindowTextW(_T("Backlight control KO"));
			}
		}
	}
	
	
}

void UNIT_TEST::OnBnClickedButtGetbatlevel()
{
	BOOL bReturn;
	int batLevel;
	char buffer[32];

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	bReturn = getBatteryLevel(&batLevel);
	
	if (bReturn == FALSE)
	{
		textStatus.SetWindowTextW(_T("Get battery level KO"));
	}
	else
	{
		textStatus.SetWindowText(_T("Get battery level OK"));
		sprintf_s(buffer,"Battery level: %d \r\n",batLevel);
		textComments.SetWindowText(CA2W(buffer));
	}
}

void UNIT_TEST::OnBnClickedOpenCashDrawerBtn()
{
	BOOL bReturn;
	char result;
	char buffer[32];

	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	bReturn = openPrinter(&result);
	if(bReturn && result == 0)
	{
		bReturn = openCashDrawer(&result);
		if (bReturn == FALSE)
		{
			textStatus.SetWindowTextW(_T("Open Cash Drawer KO"));
			textComments.SetWindowText(_T(""));
		}
		else
		{
			textStatus.SetWindowText(_T("Open Cash Drawer OK"));
			sprintf_s(buffer,"Open Cash Drawer: %c \r\n",result);
			textComments.SetWindowText(CA2W(buffer));
		}
	}
	else
	{
		textStatus.SetWindowTextW(_T("Open Printer KO"));
		textComments.SetWindowText(_T(""));
	}
	closePrinter(&result); 
}

#ifdef ENABLE_EPAS
void UNIT_TEST::OnBnClickedButtEpaslogin()
{
	DWORD dwError;
	TCHAR szError[64];
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	
	if (apiEpasLogin(5, &dwError))
	{
		textStatus.SetWindowTextW(_T("EpasLogin OK"));
	}
	else
	{
		memset(szError, 0, sizeof(szError));
		wsprintf(szError, L"EpasLogin KO. Error %d", dwError);
		textStatus.SetWindowTextW(szError);
	}
}


void UNIT_TEST::OnBnClickedButtEpaspayment()
{
	DWORD dwError;
	TCHAR szError[64];
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	if (apiEpasPaymentRequest((double)11, 60, &dwError))
	{
		textStatus.SetWindowTextW(_T("EpasPayment OK"));
	}
	else
	{
		memset(szError, 0, sizeof(szError));
		wsprintf(szError, L"EpasPayment KO. Error %d", dwError);
		textStatus.SetWindowTextW(szError);
	}
}


void UNIT_TEST::OnBnClickedButtEpaslogout()
{
	DWORD dwError;
	TCHAR szError[64];
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	if (apiEpasLogout(5, &dwError))
	{
		textStatus.SetWindowTextW(_T("EpasLogout OK"));
	}
	else
	{
		memset(szError, 0, sizeof(szError));
		wsprintf(szError, L"EpasLogout KO. Error %d", dwError);
		textStatus.SetWindowTextW(szError);
	}
}


void UNIT_TEST::OnBnClickedButtEpasdisconnect()
{
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));
	if (apiEpasDisconnect())
	{
		textStatus.SetWindowTextW(_T("EpasDisconnect OK"));
	}
	else
	{
		textStatus.SetWindowTextW(_T("EpasDisconnect KO"));
	}
}


void UNIT_TEST::OnBnClickedButtEpasconnect()
{
	DWORD dwError;
	TCHAR szError[64];
	textStatus.SetWindowText(_T(""));
	textComments.SetWindowText(_T(""));

	if (apiEpasConfigure("1.1.1.2", 11000, "SaleTermA", "POITerm1", "Ingenico", "EPAS Retailer", "0.01", ""))
	{
		if (apiEpasConnect())
		{
			textStatus.SetWindowTextW(_T("EpasConnect OK"));
		}
		else
		{
			textStatus.SetWindowTextW(_T("EpasConnect KO"));
		}
	}
	else
	{
		textStatus.SetWindowTextW(_T("EpasConnect KO"));
		textComments.SetWindowTextW(_T("Configure failed"));
	}
}
#endif // ENABLE_EPAS






