// TestThread.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <windows.h>
#include <string.h>

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#include <wchar.h>

using namespace std;



HANDLE m_hThread,m_hThread_1;

DWORD WINAPI GetStateThread(LPVOID lpParam)
{
	int increment = 0;

	while(increment<50){
		increment++;
		printf("Thread:1 %u\n",increment);
		std::this_thread::sleep_for (std::chrono::milliseconds(200));
	}

	return 0;
}

DWORD WINAPI GetStateThread_2(LPVOID lpParam)
{
	int increment = 0;

	while(increment<80){
		increment++;
		printf("Thread:2 %u\n",increment);
		std::this_thread::sleep_for (std::chrono::milliseconds(100));
	}

	return 0;
}




/*

int _tmain(int argc, _TCHAR* argv[])
{


	thread t1((LPTHREAD_START_ROUTINE)GetStateThread,(LPVOID)0);
	thread t2((LPTHREAD_START_ROUTINE)GetStateThread_2,(LPVOID)0);

	t1.join();
	t2.join();

	while(1)
		{
			if(t1.joinable()==false && t2.joinable()==false)	
			{ 
			printf("Adios\n");
			return 0;
			}

	}
	return -1;

}

*/