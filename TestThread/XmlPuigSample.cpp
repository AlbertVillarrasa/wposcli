// TestThread.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include "pugixml.hpp"
#include <iostream>
#include <fstream>
#include <cstring>
#include <windows.h>
#include <string.h>

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

using namespace std;
using namespace std::chrono;
struct xml_string_writer: pugi::xml_writer
{
    std::string result;

    virtual void write(const void* data, size_t size)
    {
        result.append(static_cast<const char*>(data), size);
    }
};

struct xml_memory_writer: pugi::xml_writer
{
    char* buffer;
    size_t capacity;

    size_t result;

    xml_memory_writer(): buffer(0), capacity(0), result(0)
    {
    }

    xml_memory_writer(char* buffer, size_t capacity): buffer(buffer), capacity(capacity), result(0)
    {
    }

    size_t written_size() const
    {
        return result < capacity ? result : capacity;
    }

    virtual void write(const void* data, size_t size)
    {
        if (result < capacity)
        {
            size_t chunk = (capacity - result < size) ? capacity - result : size;

            memcpy(buffer + result, data, chunk);
        }

        result += size;
    }
};

BOOL response_Message(char* outbuffer,char* amount, char* currentcode, char *tip, char* AdditBussines);

char* saleResponse=
	"<response_message>\
		<header type='sale' message_id='1' version='01'/>\
			<data>\
				<field name='AcceptanceCode'>91</field>\
				<field name='TransactionData'> <![CDATA[DATOS A DISPLAY EN FORMATO JSON]]>  </field>\
				<field name='CashoutData'>1</field>\
			</data>\
	</response_message>";

char* endMessage=
	"<request_message> \
		<header type='End' message_id='1' version='01'/> \
			<data>	\
				<field name='ResponseCode'>99</field> \
			</data> \
	</request_message>";

char* errorMessage=
	"<response_message> \
		<header type='error' message_id='1' version='01'/> \
			<data> \
			<field name='ErrorCode'>04</field> \
			</data> \
		</response_message>";


BOOL Send_Sale_Message(char* buffer, char* amount, char* currentcode, char *tip, char* AdditBussines)
{
    const char source[] = "<request_message> </request_message>";
    size_t size = sizeof(source);

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_buffer(source, size);

	//<header type='sale' message_id='1' version='01'/>  

	pugi::xml_node header = doc.child("request_message").append_child("header");
	header.append_attribute("type") = "Sale";
	header.append_attribute("message_id") = "1";
	header.append_attribute("version") = "01";

    // add description node with text child
    pugi::xml_node data = doc.child("request_message");
    // add param node before the description

	pugi::xml_node field= data.append_child("data");

	pugi::xml_node param = field.append_child("field");
    param.append_attribute("name") = "Amount";
	param.text()=amount;

	param = field.append_child("field");
    param.append_attribute("name") = "CurrencyCode";
	param.text()=currentcode;

	param = field.append_child("field");
    param.append_attribute("name") = "Tip";
	param.text()=tip;
    
	param = field.append_child("field");
	param.append_attribute("name") = "AdditionalBusinessData";
	param.text()=AdditBussines;

	doc.save_file("SendSale.xml");

    xml_memory_writer counter;
    doc.print(counter);

    // allocate necessary size (+1 for null termination)
    //char* buffer = new char[counter.result + 1];

    // second pass: actual printing
    xml_memory_writer writer(buffer, counter.result);
    doc.print(writer);

    // null terminate
    buffer[writer.written_size()] = 0;
	//outBuffer= &node_to_string(doc);

	return true;

}


BOOL Sale_Message_Decoder(char* AcceptanceCode, char* TransactionData, char* CashoutData,char* buffer){

	pugi::xml_document docresp;
	pugi::xml_parse_result result = docresp.load_string(buffer);
	//pugi::xml_parse_result result = docresp.load_file("RenponseMessage.xml");
	pugi::xml_node header = docresp.child("response_message").child("header");
	pugi::xml_node datas = docresp.child("response_message").child("data");

/*
    for (pugi::xml_node field = datas.child("field"); field; field = field.next_sibling("field"))
    {
		std::cout << field.attribute("name").value()<< " -->"<< field.child_value() <<std::endl;
    }

*/
	pugi::xml_node da= datas.first_child();
	//std::cout << da.attribute("name").value()<< " -->"<< da.child_value() <<std::endl;
	memcpy(AcceptanceCode,da.child_value(),16);
    pugi::xml_node de= da.next_sibling("field");
	//std::cout << de.attribute("name").value()<< " -->"<< de.child_value() <<std::endl;
	memcpy(TransactionData,de.child_value(),512);
    pugi::xml_node di= de.next_sibling("field");
	//std::cout << di.attribute("name").value()<< " -->"<< di.child_value() <<std::endl;
	memcpy(CashoutData,di.child_value(),16);


	return 0;
}

BOOL End_Message_Decoder(char* endMessage,char* buffer){

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_string(buffer);
	//pugi::xml_parse_result result = docresp.load_file("RenponseMessage.xml");
	pugi::xml_node datas = doc.child("request_message").child("data");
	pugi::xml_node em= datas.first_child();
	memcpy(endMessage,em.child_value(),16);

	return 0;
}

BOOL Error_Message_Decoder(char* error,char* buffer){

	pugi::xml_document docresp;
	pugi::xml_parse_result result = docresp.load_string(buffer);
	//pugi::xml_parse_result result = docresp.load_file("RenponseMessage.xml");
	pugi::xml_node header = docresp.child("response_message").child("header");
	if(header!=NULL) {
		pugi::xml_node datas = docresp.child("response_message").child("data");

		pugi::xml_node da= datas.first_child();
		//std::cout << da.attribute("name").value()<< " -->"<< da.child_value() <<std::endl;
		memcpy(error,da.child_value(),16);
		return true;
	}

	return false;
}


BOOL CheckHeaderType(char* inputXml, char* messagetype, char* type)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_string(inputXml);

	pugi::xml_attribute header = doc.child(type).child("header").attribute("type");
	if(header!=NULL) {
		memcpy(messagetype,header.value(),16);
		return true;
	}

	return false;

}


int _tmain(int argc, _TCHAR* argv[])
{
/*	
	<?xml version="1.0"?>
	<request_message>
		<header type="Sale" message_id="1" version="01">
			<data>
				<field name="Amount">00000000012</field>
				<field name="CurrencyCode">0978</field>
				<field name="CurrencyCode">0978</field>
				<field name="Tip">100</field>
				<field name="AdditionalBusinessData"></field>
			</data>
		</header>
	</request_message> 
*/
	 system_clock::time_point tp = system_clock::now();
	system_clock::duration dtn = tp.time_since_epoch();

	char  outBuffer[1024];
	Send_Sale_Message(outBuffer,"0000000012","0978","100","TestAdditBussines");
	cout << outBuffer;

	exit(0);

	char  messagetype[16];
	char  message[512];

	strcpy_s(message,saleResponse);
	//strcpy_s(message,endMessage);
	//strcpy_s(message,errorMessage);


	if(CheckHeaderType(message,messagetype,"response_message"))
	{

		if(strcmp("sale",messagetype)==0)
		{
			char  AcceptanceCode[16];
			char  TransactionData[512];
			char  CashoutData[1024];

			Sale_Message_Decoder(AcceptanceCode,TransactionData,CashoutData,message);

			cout <<"AcceptanceCode: "<< AcceptanceCode<< std::endl;
			cout <<"TransactionData: "<< TransactionData<< std::endl;
			cout <<"CashoutData: "<< CashoutData<< std::endl;	
		}

		if(strcmp("error",messagetype)==0)
		{
			char  errorCode[16];
			Error_Message_Decoder(errorCode,message);
			cout <<"ErrorCode: "<< errorCode<< std::endl;
		}

	}

	if(CheckHeaderType(message,messagetype,"request_message"))
	{
		if(strcmp("End",messagetype)==0)
		{
			char  endCode[16];
			End_Message_Decoder(endCode ,message);
			printf("%s",endCode);
		}
	}
	exit(0);

}

