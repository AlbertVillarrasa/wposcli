#ifndef INTERFAZWPOSAPI_H
#define INTERFAZWPOSAPI_H

//Codigos de error


typedef unsigned int APIRET;
typedef unsigned int UINT;

#ifndef EXPORT_DLL
#define EXPORT_DLL __declspec(dllexport)
#endif


EXPORT_DLL APIRET OpenWpos(char* source,int buffersize, UINT retrys );

EXPORT_DLL APIRET SaleWpos(char* outBuffer, char* importe , int timeout );

EXPORT_DLL APIRET Importe(char *source);

EXPORT_DLL APIRET SendMensage(char *source);

EXPORT_DLL APIRET CloseWpos();

EXPORT_DLL APIRET WposSocket(int port,char *sendbuf , char *recvbuf);



#endif // #ifndef INTERFAZWPOSAPI_H