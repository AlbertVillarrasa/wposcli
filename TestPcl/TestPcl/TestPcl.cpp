// TestPcl.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include "windows.h"
#include "pdautil.h"
#include "WposApi.h"
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include <iostream>


#define BUFFERSIZE 100
#define MAX_CHAR	100

int _tmain(int argc, _TCHAR* argv[])
{

	char argu[256];
	sprintf_s(argu, "%ws", argv[1] );
	char * buffer = (char*) malloc(BUFFERSIZE);

	char apiname[]="WPOS:";

	int r = strcpy_s(buffer, sizeof(apiname), apiname);
	r= strcat_s(buffer, 16," 1");

	CloseWpos();

	int result=OpenWpos(buffer,(int)BUFFERSIZE,(UINT)50);

	if(result>=1){
		printf("\nNumero %u\nStatus : %s ",OpenWpos(buffer,(int)BUFFERSIZE,(UINT)50));
		printf("%s\n",buffer);

		char  outBuffer[1024];
		SaleWpos(outBuffer,"00000120010",200);
		printf("\nXml:\n %s ",outBuffer);
	}
	CloseWpos();

	exit(0);

	char *sendbuf = "Request from WposScoket";
	char recvbuf[1024];
	memset(recvbuf,0x00,1024);

	WposSocket(4666,sendbuf,recvbuf);
	printf("\t%s\n",recvbuf);

	exit(0);



	while(1){
		printf("Pulse la tecla S para detener la App\n");
		int key=getchar();
		if(key=='S'){
			CloseWpos();
			exit(1);
		}
		printf("\nTecla Erronea. Vuelva a pulsar...\n");
	}
	CloseWpos();
	exit(0);

	if(startPclService())
	{
		printf("El servicio esta funcionando\n");
		char result;
		//std::this_thread::sleep_for (std::chrono::seconds(1));

		// Get Telium terminal information
		BOOL bReturn = FALSE;
		spmInfo_t iwlInfo;
		char buffer[256];
		int time=0;

		while(bReturn==FALSE && time==0){
			
			if (serverStatus(&result))	{
				printf("Server Status ?\n");
				//std::this_thread::sleep_for (std::chrono::seconds(1));
				if (result == 1)
					time=1;
				else
					time=0;

			}
		}

		printf("Server Status OK\n");

		while(1)
		{
			bReturn=getTerminalInfo(&iwlInfo);
			if (bReturn == FALSE)
			{
				printf("Get Info KO\n");
				if(stopPclService())
					printf("PCL Service stopped");
				else
					printf("PCL Service not stopped");

				return -1;
			}
			else
				{
					printf("Get Info OK\n");
					sprintf_s(buffer,"Serial Number: %x \r\nProduct Number: %x\r\n",iwlInfo.dwSerialNumber,iwlInfo.dwProductNumber);
					printf("%s",buffer);
			}
		}

	}


	return -1;
}

