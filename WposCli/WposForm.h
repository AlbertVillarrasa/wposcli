#pragma once

#include "WposApi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <chrono>         // std::chrono::seconds


namespace WposCli {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace std::chrono;
	using namespace	System::Xml;
	
	#define BUFFERSIZE_DATA 128*1024
	#define BUFFERSIZE 4*1024
	#define MAX_CHAR	100
	#define SALETIME	20

	#define TRIESCONNECT	10

	#define HOST	"127.0.0.1"
	#define PORT	4666
	#define TIMEOUT	5*60*1000;

	/// <summary>
	/// Resumen de WposForm
	/// </summary>

	public ref class WposForm : public System::Windows::Forms::Form
	{
	public:
	public: unsigned int saletimer;
	public:	unsigned int initImporte;
	public:	unsigned int ticks;
	public: char*  cImport;
	public: FileStream ^ fs;
	private: System::Windows::Forms::TabControl^  tabControl1;
	public: 
	private: System::Windows::Forms::TabPage^  tabPage1;

	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::TextBox^  XmlViewer;
	private: System::Windows::Forms::WebBrowser^  WebViewer;
	private: System::Windows::Forms::TextBox^  FinalizacioMessage;
	private: System::Windows::Forms::Label^  CodeLabel;
	private: System::Windows::Forms::Button^  RefundBoton;
	private: System::Windows::Forms::TextBox^  NumerOperacion;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  InputCode;

	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::CheckBox^  autoIncImport;
	private: System::Windows::Forms::Button^  PclStatusKey;
	private: System::Windows::Forms::Button^  GetDeviceConection;
	private: System::Windows::Forms::CheckBox^  EndCodeEnabled;






	public: 

	public: 
	public:	StreamWriter ^  sw;
		WposForm(void)
		{
			InitializeComponent();
			initImporte=200;
			ticks=0;
			saletimer=0;

			fs = File::Create("salelog.txt");
			sw = gcnew StreamWriter(fs);
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}

		protected:
		/// <summary>
		/// Limpiar los recursos que se est�n utilizando.
		/// </summary>
		~WposForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  BotonStartService;
	protected: 

	private: System::Windows::Forms::Button^  BotonStopService;
	private: System::Windows::Forms::Button^  SaleBoton;

	private: System::Windows::Forms::TextBox^  TextImport;


	private: System::Windows::Forms::TextBox^  Pantalla;


	private: System::Windows::Forms::TextBox^  TextMessaje;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::Button^  LoopSale;
	private: System::ComponentModel::IContainer^  components;


	protected: 


	protected: 

	private:
		/// <summary>
		/// Variable del dise�ador requerida.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido del m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->BotonStartService = (gcnew System::Windows::Forms::Button());
			this->Pantalla = (gcnew System::Windows::Forms::TextBox());
			this->BotonStopService = (gcnew System::Windows::Forms::Button());
			this->SaleBoton = (gcnew System::Windows::Forms::Button());
			this->TextImport = (gcnew System::Windows::Forms::TextBox());
			this->TextMessaje = (gcnew System::Windows::Forms::TextBox());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->LoopSale = (gcnew System::Windows::Forms::Button());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->XmlViewer = (gcnew System::Windows::Forms::TextBox());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->WebViewer = (gcnew System::Windows::Forms::WebBrowser());
			this->FinalizacioMessage = (gcnew System::Windows::Forms::TextBox());
			this->CodeLabel = (gcnew System::Windows::Forms::Label());
			this->RefundBoton = (gcnew System::Windows::Forms::Button());
			this->NumerOperacion = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->InputCode = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->autoIncImport = (gcnew System::Windows::Forms::CheckBox());
			this->PclStatusKey = (gcnew System::Windows::Forms::Button());
			this->GetDeviceConection = (gcnew System::Windows::Forms::Button());
			this->EndCodeEnabled = (gcnew System::Windows::Forms::CheckBox());
			this->tabControl1->SuspendLayout();
			this->tabPage2->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->SuspendLayout();
			// 
			// BotonStartService
			// 
			this->BotonStartService->Enabled = false;
			this->BotonStartService->Location = System::Drawing::Point(24, 60);
			this->BotonStartService->Name = L"BotonStartService";
			this->BotonStartService->Size = System::Drawing::Size(110, 38);
			this->BotonStartService->TabIndex = 0;
			this->BotonStartService->Text = L"Start Service";
			this->BotonStartService->UseVisualStyleBackColor = true;
			this->BotonStartService->Click += gcnew System::EventHandler(this, &WposForm::Boton_StartService_Click);
			// 
			// Pantalla
			// 
			this->Pantalla->Location = System::Drawing::Point(540, 18);
			this->Pantalla->Name = L"Pantalla";
			this->Pantalla->Size = System::Drawing::Size(309, 20);
			this->Pantalla->TabIndex = 0;
			// 
			// BotonStopService
			// 
			this->BotonStopService->Enabled = false;
			this->BotonStopService->Location = System::Drawing::Point(140, 59);
			this->BotonStopService->Name = L"BotonStopService";
			this->BotonStopService->Size = System::Drawing::Size(106, 38);
			this->BotonStopService->TabIndex = 2;
			this->BotonStopService->Text = L"StopService";
			this->BotonStopService->UseVisualStyleBackColor = true;
			this->BotonStopService->Click += gcnew System::EventHandler(this, &WposForm::BotonStopService_Click);
			// 
			// SaleBoton
			// 
			this->SaleBoton->Location = System::Drawing::Point(28, 111);
			this->SaleBoton->Name = L"SaleBoton";
			this->SaleBoton->Size = System::Drawing::Size(110, 26);
			this->SaleBoton->TabIndex = 3;
			this->SaleBoton->Text = L"Sale";
			this->SaleBoton->UseVisualStyleBackColor = true;
			this->SaleBoton->Click += gcnew System::EventHandler(this, &WposForm::SendBoton_Click);
			// 
			// TextImport
			// 
			this->TextImport->Location = System::Drawing::Point(139, 117);
			this->TextImport->Name = L"TextImport";
			this->TextImport->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->TextImport->Size = System::Drawing::Size(107, 20);
			this->TextImport->TabIndex = 4;
			this->TextImport->Text = L"00000000020";
			this->TextImport->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->TextImport->Leave += gcnew System::EventHandler(this, &WposForm::TextImport_Leave);
			// 
			// TextMessaje
			// 
			this->TextMessaje->Location = System::Drawing::Point(24, 583);
			this->TextMessaje->Name = L"TextMessaje";
			this->TextMessaje->Size = System::Drawing::Size(829, 20);
			this->TextMessaje->TabIndex = 9;
			// 
			// timer1
			// 
			this->timer1->Interval = 1000;
			this->timer1->Tick += gcnew System::EventHandler(this, &WposForm::timer1_Tick);
			// 
			// LoopSale
			// 
			this->LoopSale->Location = System::Drawing::Point(677, 95);
			this->LoopSale->Name = L"LoopSale";
			this->LoopSale->Size = System::Drawing::Size(137, 38);
			this->LoopSale->TabIndex = 10;
			this->LoopSale->Text = L"Loop";
			this->LoopSale->UseVisualStyleBackColor = true;
			this->LoopSale->Click += gcnew System::EventHandler(this, &WposForm::LoopSale_Click);
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Location = System::Drawing::Point(24, 149);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(829, 428);
			this->tabControl1->TabIndex = 11;
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->XmlViewer);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(821, 402);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Xml";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// XmlViewer
			// 
			this->XmlViewer->AcceptsReturn = true;
			this->XmlViewer->AcceptsTab = true;
			this->XmlViewer->Location = System::Drawing::Point(6, 6);
			this->XmlViewer->Multiline = true;
			this->XmlViewer->Name = L"XmlViewer";
			this->XmlViewer->ScrollBars = System::Windows::Forms::ScrollBars::Both;
			this->XmlViewer->Size = System::Drawing::Size(809, 388);
			this->XmlViewer->TabIndex = 0;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->WebViewer);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(821, 402);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Html";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// WebViewer
			// 
			this->WebViewer->Location = System::Drawing::Point(3, 3);
			this->WebViewer->MinimumSize = System::Drawing::Size(20, 20);
			this->WebViewer->Name = L"WebViewer";
			this->WebViewer->Size = System::Drawing::Size(812, 394);
			this->WebViewer->TabIndex = 0;
			// 
			// FinalizacioMessage
			// 
			this->FinalizacioMessage->Location = System::Drawing::Point(540, 111);
			this->FinalizacioMessage->Name = L"FinalizacioMessage";
			this->FinalizacioMessage->ReadOnly = true;
			this->FinalizacioMessage->Size = System::Drawing::Size(120, 20);
			this->FinalizacioMessage->TabIndex = 12;
			this->FinalizacioMessage->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// CodeLabel
			// 
			this->CodeLabel->AutoSize = true;
			this->CodeLabel->Location = System::Drawing::Point(537, 95);
			this->CodeLabel->Name = L"CodeLabel";
			this->CodeLabel->Size = System::Drawing::Size(113, 13);
			this->CodeLabel->TabIndex = 13;
			this->CodeLabel->Text = L"Codigo de Finalizacion";
			// 
			// RefundBoton
			// 
			this->RefundBoton->Location = System::Drawing::Point(252, 111);
			this->RefundBoton->Name = L"RefundBoton";
			this->RefundBoton->Size = System::Drawing::Size(110, 26);
			this->RefundBoton->TabIndex = 14;
			this->RefundBoton->Text = L"Refund";
			this->RefundBoton->UseVisualStyleBackColor = true;
			this->RefundBoton->Click += gcnew System::EventHandler(this, &WposForm::RefundBoton_Click);
			// 
			// NumerOperacion
			// 
			this->NumerOperacion->Location = System::Drawing::Point(365, 115);
			this->NumerOperacion->Name = L"NumerOperacion";
			this->NumerOperacion->Size = System::Drawing::Size(117, 20);
			this->NumerOperacion->TabIndex = 15;
			this->NumerOperacion->Text = L"000000000000";
			this->NumerOperacion->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->NumerOperacion->Leave += gcnew System::EventHandler(this, &WposForm::NumerOperacion_Leave);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(144, 101);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(42, 13);
			this->label1->TabIndex = 16;
			this->label1->Text = L"Importe";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(368, 99);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(78, 13);
			this->label2->TabIndex = 17;
			this->label2->Text = L"NumOperacion";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(537, 2);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(25, 13);
			this->label3->TabIndex = 18;
			this->label3->Text = L"Info";
			// 
			// InputCode
			// 
			this->InputCode->Location = System::Drawing::Point(597, 67);
			this->InputCode->Name = L"InputCode";
			this->InputCode->Size = System::Drawing::Size(47, 20);
			this->InputCode->TabIndex = 19;
			this->InputCode->Text = L"00";
			this->InputCode->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->InputCode->Leave += gcnew System::EventHandler(this, &WposForm::Code_Leave);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(537, 67);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(54, 13);
			this->label4->TabIndex = 20;
			this->label4->Text = L"End Code";
			// 
			// autoIncImport
			// 
			this->autoIncImport->AutoSize = true;
			this->autoIncImport->Location = System::Drawing::Point(677, 71);
			this->autoIncImport->Name = L"autoIncImport";
			this->autoIncImport->Size = System::Drawing::Size(98, 17);
			this->autoIncImport->TabIndex = 21;
			this->autoIncImport->Text = L"Auto Increment";
			this->autoIncImport->UseVisualStyleBackColor = true;
			// 
			// PclStatusKey
			// 
			this->PclStatusKey->Enabled = false;
			this->PclStatusKey->Location = System::Drawing::Point(140, 13);
			this->PclStatusKey->Name = L"PclStatusKey";
			this->PclStatusKey->Size = System::Drawing::Size(107, 40);
			this->PclStatusKey->TabIndex = 22;
			this->PclStatusKey->Text = L"Get Pcl Status";
			this->PclStatusKey->UseVisualStyleBackColor = true;
			this->PclStatusKey->Click += gcnew System::EventHandler(this, &WposForm::PclStatusKey_Click);
			// 
			// GetDeviceConection
			// 
			this->GetDeviceConection->Location = System::Drawing::Point(24, 12);
			this->GetDeviceConection->Name = L"GetDeviceConection";
			this->GetDeviceConection->Size = System::Drawing::Size(110, 41);
			this->GetDeviceConection->TabIndex = 23;
			this->GetDeviceConection->Text = L"Get Device Selected";
			this->GetDeviceConection->UseVisualStyleBackColor = true;
			this->GetDeviceConection->Click += gcnew System::EventHandler(this, &WposForm::GetDeviceConection_Click);
			// 
			// EndCodeEnabled
			// 
			this->EndCodeEnabled->AutoSize = true;
			this->EndCodeEnabled->Checked = true;
			this->EndCodeEnabled->CheckState = System::Windows::Forms::CheckState::Checked;
			this->EndCodeEnabled->Location = System::Drawing::Point(540, 47);
			this->EndCodeEnabled->Name = L"EndCodeEnabled";
			this->EndCodeEnabled->Size = System::Drawing::Size(101, 17);
			this->EndCodeEnabled->TabIndex = 24;
			this->EndCodeEnabled->Text = L"Send End Code";
			this->EndCodeEnabled->UseVisualStyleBackColor = true;
			// 
			// WposForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(877, 615);
			this->Controls->Add(this->EndCodeEnabled);
			this->Controls->Add(this->GetDeviceConection);
			this->Controls->Add(this->PclStatusKey);
			this->Controls->Add(this->autoIncImport);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->InputCode);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->NumerOperacion);
			this->Controls->Add(this->RefundBoton);
			this->Controls->Add(this->CodeLabel);
			this->Controls->Add(this->FinalizacioMessage);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->LoopSale);
			this->Controls->Add(this->TextMessaje);
			this->Controls->Add(this->Pantalla);
			this->Controls->Add(this->TextImport);
			this->Controls->Add(this->SaleBoton);
			this->Controls->Add(this->BotonStopService);
			this->Controls->Add(this->BotonStartService);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Location = System::Drawing::Point(42, 93);
			this->MaximizeBox = false;
			this->Name = L"WposForm";
			this->Text = L"WPos";
			this->Load += gcnew System::EventHandler(this, &WposForm::WposForm_Load);
			this->tabControl1->ResumeLayout(false);
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			this->tabPage1->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Boton_StartService_Click(System::Object^  sender, System::EventArgs^  e) {

			this->Pantalla->Text=L"Arrancando el Servicio. Por favor espere...";

			Application::DoEvents();

			char buffer[BUFFERSIZE*sizeof(char)];
			char apiname[]="WPOS";
			char result=0;

			String^ name = gcnew String(buffer);
			int timeout =TIMEOUT;
			if(int n=OpenWpos(HOST,PORT,buffer,(int)BUFFERSIZE,timeout,(UINT)10)==1)
			{
				String^ info = gcnew String(buffer);
				this->Pantalla->Text=L""+info;
				this->SaleBoton->Enabled=true;
				this->PclStatusKey->Enabled=true;
				this->BotonStopService->Enabled=true;
				ticks=0;
				 sw->Close();
				 fs = File::Create("salelog.txt");
				 sw = gcnew StreamWriter(fs);


			}else
				{
					String^ info = gcnew String(buffer);
					this->Pantalla->Text=L""+info;
					this->BotonStartService->Enabled=false;
					this->PclStatusKey->Enabled=true;
				}  

		 }
private: System::Void BotonStopService_Click(System::Object^  sender, System::EventArgs^  e) {
			 
		StopPclService();
		this->timer1->Enabled= false;
		this->XmlViewer->Text=L"";
		this->TextMessaje->Text=L"";
		this->SaleBoton->Enabled=false;
		this->BotonStartService->Enabled=true;
		this->RefundBoton->Enabled=false;
		this->Pantalla->Text=L"Puerto Wpos Cerrado";
		sw->Close();

 }
private: System::Void LoopSale_Click(System::Object^  sender, System::EventArgs^  e) {
			this->timer1->Enabled=true;
			saletimer=2;
		 }

private: System::Void StartByCalls_Click(System::Object^  sender, System::EventArgs^  e) {

			Application::DoEvents();

			char buffer[BUFFERSIZE_DATA*sizeof(char)];
			char info[BUFFERSIZE_DATA*sizeof(char)];
			char apiname[]="WPOS";
			char result=0;

			if(OpenPclService(buffer,1)==1)
			{
				do{
					ServerStatus(&result);
					 Application::DoEvents();
				}while (result!=1);

				UINT timeout=TIMEOUT;
				WposSocket(HOST,PORT,timeout,info);

				this->Pantalla->Text=L"Comunicacion Establecida ";
				this->SaleBoton->Enabled=true;
			}
			else{
				this->Pantalla->Text=L"Fallo al establecer la comunicacion ";
				this->BotonStartService->Enabled=false;
					ticks=0;
			}
		}

private: System::Void WposForm_Load(System::Object^  sender, System::EventArgs^  e) {

			 char  cImport[BUFFERSIZE*sizeof(char)];
			 this->Pantalla->Text=L"Pulse Start Service para abrir la comunicacion...";
			 this->XmlViewer->Text=L"";
			 this->TextMessaje->Text=L"";
			 this->SaleBoton->Enabled=false;	// false
			 this->RefundBoton->Enabled=false;
			 this->PclStatusKey->Enabled=false;
			 sprintf_s(cImport, "%012d", initImporte,64);
			 String^ importe = gcnew String(cImport);
			 this->TextImport->Text= importe;

		 }
private: System::Void SendBoton_Click(System::Object^  sender, System::EventArgs^  e) {

			 char  outBuffer[BUFFERSIZE_DATA*sizeof(char)];
			 char  info[BUFFERSIZE_DATA*sizeof(char)];
			 char  cImport[BUFFERSIZE*sizeof(char)];
			 char  *InputFinal;
			 InputFinal = (char*) malloc(BUFFERSIZE*sizeof(char));
			 memset(outBuffer,'\x00',BUFFERSIZE_DATA*sizeof(char));
			 memset(cImport,'\x00',BUFFERSIZE*sizeof(char));
			 memset(InputFinal,'\x00',BUFFERSIZE*sizeof(char));
			 unsigned long len =(unsigned long)strlen(outBuffer);

			 this->SaleBoton->Enabled=false;
			 this->RefundBoton->Enabled=false;
			 this->TextMessaje->Text=L"";
			 this->XmlViewer->Text=L"";
			 this->FinalizacioMessage->Text=L"";
			 Application::DoEvents();
 
			 InputFinal =  (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(this->InputCode->Text);

			 sprintf_s(cImport, "%012d", initImporte);

			 int n =SaleWpos(outBuffer,info,cImport,"0978","100","{'CashRegisterData': {'ReferenceNumber' : '12345678ABCD'}}");
			 String^ inf = gcnew String(info);
			 this->TextMessaje->Text=L""+inf;

			 if(n!=-1){
	 			 String^ ot = gcnew String(outBuffer);
				 this->XmlViewer->Text=ot;

				char  cHora[BUFFERSIZE*sizeof(char)];
				System::DateTime^ now = System::DateTime::Now;
				sprintf_s(cHora, "%02d:%02d:%02d ", now->Hour,now->Minute,now->Second,64);
				String^ hora = gcnew System::String(cHora);
				sw->AutoFlush;
				sw->WriteLine(hora+ "--> "+inf);

				if(ot->Length>1){
					Xml::XmlDataDocument^ data = gcnew System::Xml::XmlDataDocument();
					data->LoadXml(ot);
					XmlNodeList^ fields= data->GetElementsByTagName("field");
					if(fields->Count>0){

							char responseCode[16*sizeof(char)];
							String^ acept = fields->Item(0)->InnerText;

							if(this->EndCodeEnabled->Checked==true){
								FinalWpos(outBuffer,info,InputFinal,responseCode);
								String^c= gcnew System::String(responseCode);
								this->FinalizacioMessage->Text=L""+c;
	
								if(String::Compare(acept,gcnew String("90"))==0 || String::Compare(acept,gcnew String("91"))==0 || String::Compare(acept,gcnew String("92"))==0) {
									this->WebViewer->DocumentText  = fields->Item(1)->InnerText;
								}
							}else{
								String^c= gcnew System::String("EndCode Not Sended");
								this->FinalizacioMessage->Text=L""+c;
							}

						}
					}
				}
				else{
				 //CloseWpos();
				this->TextMessaje->Text=L""+inf;
				this->Pantalla->Text=L"Se ha producido un error. Cierre el PCL...";
				this->SaleBoton->Enabled=false;
			 }

			 this->SaleBoton->Enabled=true;
			 this->RefundBoton->Enabled=true;

			 if(this->autoIncImport->Checked)
				initImporte++;

			 sprintf_s(cImport, "%012d", initImporte,64);
			 String^ importe = gcnew String(cImport);
			 this->TextImport->Text= importe;

		 }

private: System::Void RefundBoton_Click(System::Object^  sender, System::EventArgs^  e) {

			 char  outBuffer[BUFFERSIZE_DATA*sizeof(char)];
			 char  info[BUFFERSIZE_DATA*sizeof(char)];
			 char  cImport[BUFFERSIZE*sizeof(char)];
			 char  cDevolucion[BUFFERSIZE*sizeof(char)];
			 char  *InputFinal;
			 InputFinal = (char*) malloc(BUFFERSIZE*sizeof(char));
			 memset(outBuffer,'\x00',BUFFERSIZE_DATA*sizeof(char));
			 memset(cImport,'\x00',BUFFERSIZE*sizeof(char));
			 memset(InputFinal,'\x00',BUFFERSIZE*sizeof(char));
			 unsigned long len =(unsigned long)strlen(outBuffer);

			 this->SaleBoton->Enabled=false;
			 this->TextMessaje->Text=L"";
			 this->XmlViewer->Text=L"";
			 this->RefundBoton->Enabled=false;
			 this->FinalizacioMessage->Text=L"";
			 Application::DoEvents();

			 InputFinal =  (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(this->InputCode->Text);

			 sprintf_s(cImport, "%012d", initImporte);
			 sprintf_s(cDevolucion, "%012s", this->NumerOperacion->Text);
			 int n =RefundWpos(outBuffer,info,cImport,"0978",cDevolucion,"TestAdditBussines");
			 String^ inf = gcnew String(info);
			 this->TextMessaje->Text=L""+inf;

			 if(n!=-1){
	 			 String^ ot = gcnew String(outBuffer);
				 this->XmlViewer->Text=ot;

				char  cHora[BUFFERSIZE*sizeof(char)];
				System::DateTime^ now = System::DateTime::Now;
				sprintf_s(cHora, "%02d:%02d:%02d ", now->Hour,now->Minute,now->Second,64);
				String^ hora = gcnew System::String(cHora);
				sw->AutoFlush;
				sw->WriteLine(hora+ "--> "+inf);

				if(ot->Length>1){
					Xml::XmlDataDocument^ data = gcnew System::Xml::XmlDataDocument();
					data->LoadXml(ot);
					XmlNodeList^ fields= data->GetElementsByTagName("field");
					if(fields->Count>1){
							char responseCode[16*sizeof(char)];
							String^ acept = fields->Item(0)->InnerText;

							if(String::Compare(acept,gcnew String("90"))==0 || String::Compare(acept,gcnew String("91"))==0 || String::Compare(acept,gcnew String("92"))==0) {
							}else
							{
							}
							FinalWpos(outBuffer,info,InputFinal,responseCode);
							String^c= gcnew System::String(responseCode);
							this->FinalizacioMessage->Text=L""+c;
							this->WebViewer->DocumentText  = fields->Item(1)->InnerText;
						}else{
							String^ acept = fields->Item(0)->InnerText;
							char responseCode[16*sizeof(char)];
							FinalWpos(outBuffer,info,InputFinal,responseCode);
							String^c= gcnew System::String(responseCode);
							this->FinalizacioMessage->Text=L""+c;
						}
					}
				}
				else{
				 //CloseWpos();
				char responseCode[16*sizeof(char)];
				FinalWpos(outBuffer,info,InputFinal,responseCode);
				String^c= gcnew System::String(responseCode);
				this->FinalizacioMessage->Text=L""+c;
				this->TextMessaje->Text=L""+inf;
				this->Pantalla->Text=L"Se ha producido un error. Cierre el PCL...";
				this->SaleBoton->Enabled=false;
			 }

			 this->SaleBoton->Enabled=true;
			 this->RefundBoton->Enabled=true;

			 if(this->autoIncImport->Checked)
				initImporte++;

			 sprintf_s(cImport, "%012d", initImporte,64);
			 String^ importe = gcnew String(cImport);
			 this->TextImport->Text= importe;

		 }
private: System::Void TextImport_Leave(System::Object^  sender, System::EventArgs^  e) {
			char  cImport[BUFFERSIZE*sizeof(char)];
			initImporte =int::Parse(this->TextImport->Text);
			sprintf_s(cImport, "%012d", initImporte,64);
			String^ importe = gcnew String(cImport);
			this->TextImport->Text= importe;

		 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 if(ticks==saletimer){
				 if(this->SaleBoton->Enabled==true){
					this->SendBoton_Click(sender,e);
					saletimer=ticks+SALETIME;
				 }
			 }
			 ticks++;
		 }
private: System::Void NumerOperacion_Leave(System::Object^  sender, System::EventArgs^  e) {
			char  cOpera[BUFFERSIZE*sizeof(char)];
			String^ numopera = gcnew String(this->NumerOperacion->Text);
			sprintf_s(cOpera, "%012s", numopera,64);
			String^ op = gcnew String(cOpera);
			this->NumerOperacion->Text= op;
		 }
private: System::Void Code_Leave(System::Object^  sender, System::EventArgs^  e) {
			char  inputCode[BUFFERSIZE*sizeof(char)];
			String^ icode = gcnew String(this->InputCode->Text);
			sprintf_s(inputCode, "%02s", icode,64);
			String^ op = gcnew String(inputCode);
			this->InputCode->Text= op;
		 }
private: System::Void PclStatusKey_Click(System::Object^  sender, System::EventArgs^  e) {
			 char result=0;
			 ServerStatus(&result);
			 if(result==1){
				this->Pantalla->Text=L"PCL Conectado...";
			 }
			 else{
				this->Pantalla->Text=L"PCL Fuera de servcio.!";
				this->RefundBoton->Enabled=false;
				this->SaleBoton->Enabled=false;
			 }
		 }

private: System::Void GetDeviceConection_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			 wchar_t commName[256*sizeof(wchar_t)];
			 wchar_t devName[256*sizeof(wchar_t)];
			 if (UsbDeviceCompanions(commName,devName)==1){
				//#sw->Close();
				 this->timer1->Enabled= false;
				 this->XmlViewer->Text=L"";
				 this->TextMessaje->Text=L"";
				 this->BotonStartService->Enabled=true;
				 this->BotonStopService->Enabled=true;
				 this->PclStatusKey->Enabled=true;
				 String^c= gcnew System::String(commName);
				 String^d= gcnew System::String(devName);
				 this->Pantalla->Text=L"Selected Device: "+c+ " en " +d;
				 }else{
					 this->timer1->Enabled= false;
					 this->XmlViewer->Text=L"";
					 this->TextMessaje->Text=L"";
					 this->SaleBoton->Enabled=false;
					 this->BotonStartService->Enabled=false;
					 this->BotonStopService->Enabled=false;
					 this->PclStatusKey->Enabled=false;
					 this->RefundBoton->Enabled=false;
			 		this->Pantalla->Text=L"No se ha encontrado el dispositivo seleccionado...";
			 }
	 }

};
}